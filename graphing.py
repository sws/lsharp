import os
import sys
import pandas as pd
import matplotlib.pyplot as plt
import argparse
import seaborn as sns
import collections


def compute_points(data):
    dict_by_alg = collections.defaultdict(dict)
    for (key, val) in data.items():
        model_name = key[0]
        alg = key[1]
        # print(model_name + " " + alg + " " + str(val))
        model_name = model_name[:-4]
        dict_by_alg[alg][model_name] = val
    return dict_by_alg


def rep_und(original_str):
    original_str.replace("_", "-")


def float_format(string: str):
    print("Called float format with {}" % string)
    "{:.2E}".format(string)


def no_format(string: str):
    print(f"Called no format with {string}")
    "{}".format(string)


def formatter_get():
    format_dict = {
        "learn-inputs": float_format,
        "learn-resets": float_format,
        "test-inputs": float_format,
        "test-resets": float_format,
        "Total": float_format,
    }
    format_dict = {
        "$|n| \\times |k|$": no_format,
        "Name": no_format,
        "Learner": no_format,
        "EQ": no_format,
    }
    return format_dict


def processDF():
    df = pd.read_csv(sys.argv[1], sep=",", error_bad_lines=False)
    df.columns = df.columns.str.replace("_", "-")
    df = df.sort_values(by=["num-states", "num-inputs", "name", "learning-algorithm"], ignore_index=True)
    # print(df.sort_values(by=["name"])["name"].to_string())
    # print(df["name"].to_string())
    df["Total Testing"] = df["test-inputs"] + df["test-resets"]
    df["Total Learning"] = df["learn-inputs"] + df["learn-resets"]
    df["Total"] = df["Total Learning"] + df["Total Testing"]
    # what_to_plot = "learn-resets"
    # what_to_plot = "Total Learning"
    # what_to_plot = "rounds"
    what_to_plot = "Total"
    metric_dict = compute_points(
        df.groupby(["name", "learning-algorithm"], sort=False)[what_to_plot]
        .mean()
        .to_dict()
    )
    metric_std_dict = compute_points(
        df.groupby(["name", "learning-algorithm"], sort=False)[what_to_plot]
        .std()
        .to_dict()
    )
    # print(metric_dict)
    df.rename(
        columns={"name": "Name", "learning-algorithm": "Learner", "rounds": "EQ"},
        inplace=True,
    )
    df.rename(
        columns={
            "learn-inputs": "Learning Inputs",
            "learn-resets": "Learning Resets",
            "test-inputs": "Testing Inputs",
            "test-resets" : "Testing Resets"
        },
        inplace=True,
    )
    model_state_dict = {k.replace(".dot",""):v for (k,v) in   df.groupby(["Name"])["num-states"].mean().to_dict().items()}
    model_input_dict = {k.replace(".dot",""):v for (k,v) in   df.groupby(["Name"])["num-inputs"].mean().to_dict().items()}

    df["$|n| \\times |k|$"] = df["num-states"] * df["num-inputs"]
    cols = df.columns.tolist()
    cols = cols[-1:] + cols[:-1]
    df = df[cols]
    df = df.drop(
        [
            "num-states",
            "num-inputs",
            "ads-score",
            "learned",
            "Total Learning",
            "Total Testing",
        ],
        axis="columns",
    )
    grouped_df = df.groupby(["Name", "Learner"]).mean()
    # print(df.groupby(["Name", "Learner"]).describe().to_string())
    df_tex = grouped_df.to_latex(index=True, escape=False)
    # Colors for the markers!
    colors = ["blue", "k", "green", "red", "m", "y"]  # k is black
    # Colors for the error bars!
    ecolors = ["lightblue", "lightgray", "lightgreen", "pink", "lightgray", "lightgray"]
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    c_idx = 0
    markers = ["D", "p", "s", "o", ">", "<"]
    legend = []
    alg_rename_dict = {"lsharp_Ads_Ads": "L#-ADS", "lsharp_Nothing_SepSeq": "L#"}
    for alg in metric_dict:
        # alg_data = sorted(metric_dict[alg].items())
        # error_data = sorted(metric_std_dict[alg].items())
        alg_data = metric_dict[alg].items()
        error_data = metric_std_dict[alg].items()
        error_points = [i[1] for i in sorted(error_data, key= lambda k: (model_state_dict[k[0]], model_input_dict[k[0]], k[0]))]
        data_points = [i[1] for i in sorted(alg_data, key= lambda k: (model_state_dict[k[0]], model_input_dict[k[0]], k[0]))]
        # print(alg)
        legend.append(alg_rename_dict[alg] if alg in alg_rename_dict else alg)
        # model_list = [i[0] for i in alg_data]
        model_list = range(len(alg_data))
        # data_points = [i[1] for i in alg_data]
        # error_points = [i[1] for i in error_data]
        # print(model_list)
        ax.errorbar(
            model_list,
            data_points,
            yerr=error_points,
            fmt=colors[c_idx][0] + markers[c_idx],
            capsize=5,
            ecolor=ecolors[c_idx],
        )
        c_idx += 1
    plt.xlabel("Models (ascending order of size)")
    if what_to_plot != "rounds":
        plt.yscale("log")
    ax.set_xticks([])
    #     ax.set_yticks(num_models)
    plt.ylabel("Total input symbols and resets (log scale)")
    # if what_to_plot != "rounds":
    #     ax.set_ylim(bottom=100)
    plt.legend(legend, loc="best", markerscale=1.2, fontsize="large")
    #    plt.show()
    fig.savefig(what_to_plot + ".pdf", bbox_inches="tight")

    # fig = plt.figure()
    # ax = fig.add_subplot(1,1,1)
    # # df.groupby(["Name"]).boxplot("Total", by="Name",subplots=False,  grid=False)
    # for alg in metric_dict.keys():
    #     df[df["Learner"] == alg].boxplot("Total", ax=ax, by="Name",  grid=False)
    # # for alg in metric_dict:
    #     # df.groupby(["Name"]).boxplot("Total", by="Name",subplots=False,  grid=False)
    #     #ax.boxplot( df.groupby(["Name", "Learner"])["Total"])
    # plt.show()
    return df, df_tex


if __name__ in ["__main__"]:
    print("Welcome to the results plotter!")

    if len(sys.argv) < 2:
        assert False, "Please provide a path to the results CSV file!"

    resultsDF, df_tex = processDF()
    # with open("/home/bharat/papers/lsharp/table_data.tex", "w") as w:
    #     w.write(df_tex)
