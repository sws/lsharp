## L<sup>#</sup> Learning Library


### Note
[LearnLib](https://learnlib.de) is **not** a part of our tool,
it is included as a sub-module purely in order to make it easier
to run benchmarks against it.

## Requirements

This tool is meant to be run **only on 64-bit** systems:
running it on 32-bit systems is **undefined behaviour**.

## Building

Clone the repository; you will also need the rust toolchain installed.
If you wish to use [FSMLib's](https://github.com/Soucha/FSMlib) equivalence oracles (SPY and SPYH), you'll also need to download and build that.

You can build our learning library using ```cargo build``` to build the dev version of the same. 
Add the ```--release``` flag for the optimised release build, although this takes much longer to compile.


<!-- 
After cloning, you must build hybrid-ads (in the hybrid-ads directory), see [here](https://gitlab.science.ru.nl/moerman/hybrid-ads) for build instructions.
Next, you can build learnlib (in the learnlib directory) using the command ```maven compile assembly:single``` to get the jar file.
Finally, 
-->

## Running Experiments

In order to get help text, you can run ```cargo run -- -h```.
Please compile the program in release mode to get best (reasonable) performance, 
debug mode is quite slow.
In order to use FSMLib's [^1] equivalence oracles, you'll need to compile and provide the path to them yourself! [^2].

To learn the "example.dot" model, you can run the following command from the base directory.
Note: all arguments are mandatory. We do no use any default arguments.
```bash
cargo run -r --bin lsharp -- -e hads-int --rule2 ads --rule3 ads -k 3 -l 1 -m example.dot \
--eq-mode infinite --out results.csv -x 0 -v 0 
```

Results will be written to a file called "results.csv" in the same directory.

### Equivalence oracles 

The following equivalence oracles are valid:
1. Hybrid-ADS `hads-int`
2. W-method `w`
3. Wp-method `wp`
4. HSI `hsi`
5. IADS `iads`
6. FSMLib SPY and SPYH `soucha-spy` (`soucha-spyh`) 

When running equivalence oracles in infinite mode (`eq-mode` is set to `infinite`), the expected random length (argument `l`) must be greater than 0.

### Models
The models from AutomataWiki can be found in the `experiment_models` directory,
 while ASML models can be found in the `asml_rers_models` directory.
 ESM models are in the `esm_models` directory.
 Note, as yet, we can only learn the AutomataWiki models easily, the other two are difficult to learn, 
 due to equivalence checking.

You can provide logs (with the argument `traces`), and if those are provided, you can also use the `logged-hads` equivalence oracle.
Currently, we only have logs for the ASML models.

[^1]: Called Soucha in the code.
[^2]: The path can be provided using variable `soucha_config` in file `learning.rs`.