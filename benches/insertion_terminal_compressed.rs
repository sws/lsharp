use std::collections::HashMap;
use std::hash::BuildHasherDefault;
use std::path::Path;

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use itertools::Itertools;
use lsharp_ru::definitions::mealy::{InputSymbol, OutputSymbol};
use lsharp_ru::definitions::FiniteStateMachine;
use lsharp_ru::learner::obs_tree::{CompressedObservationTree, ObservationTree};
use lsharp_ru::{definitions::mealy::Mealy, util::parsers::machine::read_mealy_from_file};
use rand::rngs::StdRng;
use rand_core::SeedableRng;
use rand_distr::{Distribution, Uniform, WeightedAliasIndex};

fn load_basic_fsm(
    name: &str,
) -> (
    Mealy,
    HashMap<String, InputSymbol>,
    HashMap<String, OutputSymbol>,
) {
    let file_name = Path::new(name);
    read_mealy_from_file(file_name.to_str().expect("Safe"))
}

fn insertion() {
    // let fsm = load_basic_fsm("hyp_esm/hypothesis_534.dot");
    let (fsm, _, _) = load_basic_fsm("benches/sep_seq_models/esm-manual-controller.dot");

    let mut otree: CompressedObservationTree<BuildHasherDefault<rustc_hash::FxHasher>> =
        CompressedObservationTree::new(fsm.input_alphabet().len());
    let seed = 50;
    let mut rng = StdRng::seed_from_u64(seed);
    let num_traces = 25_000;
    let lens: Vec<usize> = (0usize..).into_iter().take(4000).collect_vec();
    let weights: Vec<_> = [4usize, 3, 2, 1]
        .into_iter()
        .flat_map(|n| std::iter::repeat(n).take(1000))
        .collect();
    let len_range = WeightedAliasIndex::new(weights)
        .unwrap()
        .map(|idx| lens[idx]);
    // let len_range = Uniform::new_inclusive(1, 4000);

    let mut inputs_rng = StdRng::seed_from_u64(22);
    let input_range = Uniform::new(0, fsm.input_alphabet().len() as u16).map(InputSymbol::from);
    let trace_iterator = len_range
        .sample_iter(&mut rng)
        .take(num_traces)
        .map(|length| {
            let iword = (&input_range)
                .sample_iter(&mut inputs_rng)
                .take(length)
                .collect_vec();
            let o_word = fsm.trace(&iword).1;
            (iword, o_word.to_vec())
        });
    for (i, o) in trace_iterator {
        otree.insert_observation(None, &i, &o);
    }
    // let seq = shortest_separating_sequence(&hyp, &fsm, None, None);
}

fn criterion_benchmark(c: &mut Criterion) {
    let mut grp = c.benchmark_group("OTREE");
    grp.sample_size(100);
    grp.bench_function("Insertion", |b| {
        b.iter(black_box(insertion));
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
