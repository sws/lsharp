use super::{
    super::{AdaptiveDistinguishingSequence, AdsStatus},
    utils::{compute_score, partition_on_output},
};
use crate::{
    definitions::mealy::{InputSymbol, OutputSymbol, State},
    learner::obs_tree::ObservationTree,
    util::toolbox,
};
use fnv::FnvHashMap;
use itertools::Itertools;
use rayon::iter::{IntoParallelIterator, ParallelIterator};
use std::{collections::HashMap, hash::BuildHasherDefault, sync::Arc};

#[derive(Debug, Default)]
struct Node {
    input: Option<InputSymbol>,
    children: FnvHashMap<OutputSymbol, Arc<Node>>,
    score: f64,
}

#[derive(Debug, Default)]
pub struct Ads {
    tree: Arc<Node>,
    curr_node: Arc<Node>,
    initial_node: Arc<Node>,
}

impl Node {
    #[must_use]
    fn leaf_node() -> Self {
        Self {
            input: None,
            children: FnvHashMap::with_capacity_and_hasher(0, BuildHasherDefault::default()),
            score: 0.0,
            // block_size: 1,
        }
    }
    #[must_use]
    fn get_score(&self) -> f64 {
        self.score
    }
    #[must_use]
    fn get_input(&self) -> Option<InputSymbol> {
        self.input
    }
    #[must_use]
    fn get_child_node(&self, last_output: OutputSymbol) -> Option<Arc<Node>> {
        self.children.get(&last_output).map(Arc::clone)
    }
}

impl Ads {
    #[must_use]
    pub fn new<Tree: ObservationTree<InputSymbol, OutputSymbol> + Send + Sync>(
        o_tree: &Tree,
        current_block: &[State],
    ) -> Self
    where
        <Tree as ObservationTree<InputSymbol, OutputSymbol>>::S: Default,
    {
        let mut ret = Self::default();
        let initial_node = Self::construct_ads(o_tree, current_block);
        ret.tree = Arc::clone(&initial_node);
        ret.initial_node = Arc::clone(&initial_node);
        ret.curr_node = initial_node;
        ret
    }

    #[must_use]
    pub fn get_score(&self) -> f64 {
        self.initial_node.get_score()
    }

    #[must_use]
    fn construct_ads<Tree: ObservationTree<InputSymbol, OutputSymbol> + Send + Sync>(
        o_tree: &Tree,
        current_block: &[State],
    ) -> Arc<Node>
    where
        <Tree as ObservationTree<InputSymbol, OutputSymbol>>::S: Default,
    {
        let block_size = current_block.len();
        if block_size == 1 {
            return Arc::new(Node::leaf_node());
        }

        let mut max_score = 0.0_f64;
        let mut winning_node = Some(Node::leaf_node());
        let input_size = o_tree.input_size();
        let input_alphabet = toolbox::inputs_iterator(input_size).collect_vec();
        let subtree_info = input_alphabet
            .into_par_iter()
            .map(|i| {
                let mut i_score = 0.0;
                let mut i_children = FnvHashMap::default();
                let mut undef = false;
                // Get the outputs and the parititions for input i.
                let o_partitions = partition_on_output(o_tree, current_block, i);
                if o_partitions.is_empty() {
                    // num_undef += 1;
                    undef = true;
                    return (undef, i_score, Node::leaf_node());
                }
                // Compute the sub-trees for the parititions.
                let u_i = o_partitions.values().map(std::vec::Vec::len).sum::<usize>();
                let data = o_partitions
                    .into_par_iter()
                    .map(|(o, o_part)| {
                        let i_subtree = Self::construct_ads(o_tree, &o_part);
                        let child_score = i_subtree.get_score();
                        let u_i_o = o_part.len();
                        let child_to_return = (o, i_subtree);
                        let i_score_to_return =
                            compute_score(u_i as f64, u_i_o as f64, u_i as f64, 0.99 * child_score);
                        (i_score_to_return, child_to_return)
                    })
                    .collect::<Vec<_>>();
                for (score, child) in data {
                    i_score += score;
                    i_children.insert(child.0, child.1);
                }
                if i_score <= max_score {
                    return (undef, i_score, Node::leaf_node());
                }
                let i_tree_node = Node {
                    score: i_score,
                    input: Some(i),
                    children: i_children,
                    // block_size,
                };
                (undef, i_score, i_tree_node)
            })
            .collect::<Vec<(bool, f64, Node)>>();
        if subtree_info.iter().all(|x| x.0) {
            let leaf = Arc::new(Node {
                // block_size,
                input: None,
                score: 0.0,
                children: HashMap::with_capacity_and_hasher(0, Default::default()),
            });
            return leaf;
        }
        for (_, score, node) in subtree_info {
            if score > max_score {
                max_score = score;
                winning_node = Some(node);
            }
        }
        Arc::new(winning_node.expect("Safe"))
    }
}

impl AdaptiveDistinguishingSequence for Ads {
    fn next_input(&mut self, prev_output: Option<OutputSymbol>) -> Result<InputSymbol, AdsStatus> {
        self.curr_node = Arc::clone(&{
            prev_output.map_or_else(
                || Ok(Arc::clone(&self.curr_node)),
                |output| {
                    self.curr_node
                        .get_child_node(output)
                        .ok_or(AdsStatus::Unexpected)
                },
            )
        }?);
        self.curr_node.get_input().ok_or(AdsStatus::Done)
    }

    fn reset_to_root(&mut self) {
        self.curr_node = Arc::clone(&self.initial_node);
    }

    fn get_print_tree(&self) -> Box<[u8]> {
        todo!()
    }

    fn identification_power(&self) -> f32 {
        self.get_score() as f32
    }
}
