use crate::definitions::mealy::{InputSymbol, Mealy, State};
use crate::definitions::FiniteStateMachine;
use crate::util::data_structs::arena_tree::ArenaTree;
use fnv::FnvHashMap;
use std::collections::VecDeque;

#[derive(PartialEq, Eq, Hash, Clone, Default, Debug)]
pub struct SplittingNode {
    // The block of states in this node.
    states: Vec<State>,
    // Index of the child nodes.
    children: Vec<usize>,
    // Separating word for the block.
    separator: Option<Box<[InputSymbol]>>,
    // Distance between root node and current node.
    depth: usize,
}

impl SplittingNode {
    #[must_use]
    fn new(
        states: Vec<State>,
        children: Vec<usize>,
        separator: Option<Box<[InputSymbol]>>,
        depth: usize,
    ) -> Self {
        Self {
            states,
            children,
            separator,
            depth,
        }
    }

    #[must_use]
    pub fn get_separator(&self) -> Option<Box<[InputSymbol]>> {
        self.separator.clone()
    }
    fn add_separator(&mut self, sep: Box<[InputSymbol]>) {
        self.separator = Some(sep);
    }
    fn add_child(&mut self, child: usize) {
        self.children.push(child);
    }

    #[must_use]
    #[allow(dead_code)]
    pub fn num_children(&self) -> usize {
        self.children.len()
    }
}

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
/// The type of split induced by the input symbol.
enum SplitType {
    /// The specified input generates different outputs in the block.
    Output,
    /// The specified input leads to a node index ``usize`` which splits the block.
    Destination(usize),
}
/// A split is considered valid iff
/// 1. The outputs partition the block, or
/// 2. The destination states have been split already, i.e.,
/// they are in a different partition.
///
/// Function returns Some(SplitType) if the specified input is a "valid input."
#[must_use]
fn is_input_valid(
    hyp: &Mealy,
    tree: &ArenaTree<SplittingNode, ()>,
    curr_block: &[State],
    input_symbol: InputSymbol,
) -> Option<SplitType> {
    let mut output_map_split = FnvHashMap::default();
    let mut dest_block = vec![];
    for s in curr_block {
        let (d, o) = hyp.step_from(*s, input_symbol);
        output_map_split.entry(o).or_insert_with(Vec::new).push(*s);
        dest_block.push(d);
    }
    if output_map_split.len() > 1 {
        return Some(SplitType::Output);
    }
    // Input does not split on the basis of the output, so
    // we will now check if it does so on the basis of the
    // destination states.
    for n in 0..tree.size() {
        let curr_node = &tree.arena[n];
        // If the current node is a leaf, or does not split on anything,
        // we are not interested.
        if curr_node.val.children.len() <= 1 {
            continue;
        }
        let node_block = &curr_node.val.states;
        let contains_all_dest_states = dest_block.iter().all(|x| node_block.contains(x));
        if contains_all_dest_states {
            let doesnt_refine = curr_node.val.children.iter().any(|child_idx| {
                let child_node = &tree.arena[*child_idx].val;
                dest_block.iter().all(|x| child_node.states.contains(x))
            });
            if doesnt_refine {
                continue;
            }
            // if dest_block.len() < curr_block.len() {
            //     continue;
            // }
            return Some(SplitType::Destination(n));
        }
    }
    None
}

/// For the given states, return the node index of the Lowest Common Ancestor.
#[must_use]
pub fn find_lca(tree: &ArenaTree<SplittingNode, ()>, block: &[State]) -> usize {
    let mut lca = 0;
    loop {
        let curr_node = &tree.arena[lca].val;
        let mut changed = false;
        for child_idx in &curr_node.children {
            let block_is_subset = block
                .iter()
                .all(|s| tree.arena[*child_idx].val.states.contains(s));
            if block_is_subset {
                lca = *child_idx;
                changed = true;
                break;
            }
        }
        if !changed {
            break;
        }
    }
    lca
}

#[must_use]
pub fn new(hyp: &Mealy) -> ArenaTree<SplittingNode, ()> {
    let mut tree = ArenaTree::default();
    let root_node = {
        SplittingNode {
            states: hyp.states().into_iter().collect(),
            children: Vec::new(),
            separator: None,
            depth: 0,
        }
    };
    let root_idx = tree.node(root_node);

    // We want to split on output until we cannot anymore.
    let mut work_list = VecDeque::new();
    work_list.push_back(root_idx);
    let mut no_progress_cnt = 0;
    while !work_list.is_empty() {
        let curr_node_idx = work_list
            .pop_front()
            .expect("Splitting tree worklist inconsistent! Should never happen!");
        let mut have_split = false;
        let mut curr_block = vec![];
        curr_block.extend_from_slice(tree.arena[curr_node_idx].val.states.as_slice());
        if curr_block.len() == 1 {
            continue;
        }

        for i in &hyp.input_alphabet() {
            let input_type = match is_input_valid(hyp, &tree, &curr_block, *i) {
                Some(x) => x,
                _ => continue,
            };
            if let SplitType::Destination(dest_idx) = input_type {
                // Do the destination split construction.
                // We need to get the separator from the destination node.
                let mut sep = vec![*i];
                {
                    let other_sep = tree.arena[dest_idx].val.separator.clone().unwrap();
                    sep.extend_from_slice(&other_sep);
                }
                let sep = sep.into_boxed_slice();
                // Get the output for each state for the entire sep word
                // and then split by that.
                let children = refine_block(&curr_block, hyp, sep, &mut tree, curr_node_idx);
                let curr_node = &mut tree.arena[curr_node_idx];
                for c in children {
                    curr_node.val.add_child(c);
                    work_list.push_back(c);
                }
            } else {
                // Do the output split construction.
                let sep = Box::new([*i]);
                let children = refine_block(&curr_block, hyp, sep, &mut tree, curr_node_idx);
                let curr_node = &mut tree.arena[curr_node_idx];
                for c in children {
                    curr_node.val.add_child(c);
                    work_list.push_back(c);
                }
            };
            have_split = true;
            no_progress_cnt = 0;
            break;
        }
        if !have_split {
            work_list.push_back(curr_node_idx);
        }
        if no_progress_cnt > work_list.len() {
            break;
        }
        no_progress_cnt += 1;
    }

    log::info!("Splitting tree is complete: {}", is_complete(&tree));
    tree
}

/// Refine the current block using the provided separator,
/// construct the child nodes to be processed, and
/// return the list of child node indices.
#[must_use]
fn refine_block(
    curr_block: &[State],
    hyp: &Mealy,
    sep: Box<[InputSymbol]>,
    tree: &mut ArenaTree<SplittingNode, ()>,
    curr_node_idx: usize,
) -> Vec<usize> {
    let mut output_map_split = FnvHashMap::default();
    curr_block
        .iter()
        .map(|s| (s, hyp.trace_from(*s, sep.as_ref()).1))
        .for_each(|(s, o)| output_map_split.entry(o).or_insert_with(Vec::new).push(*s));
    if output_map_split.len() < 2 {
        unreachable!();
    }
    assert_eq!(
        curr_block.len(),
        output_map_split
            .values()
            .map(std::vec::Vec::len)
            .sum::<usize>()
    );
    let curr_node = &mut tree.arena[curr_node_idx];
    curr_node.val.add_separator(sep);
    let curr_depth = curr_node.val.depth;
    let mut children = Vec::with_capacity(output_map_split.len());
    for (_, partition) in output_map_split {
        let child_node = SplittingNode::new(partition, vec![], None, curr_depth + 1);
        let child_node_idx = tree.node_with_parent(child_node, curr_node_idx, ());
        children.push(child_node_idx);
    }
    children
}

#[must_use]
fn is_complete(tree: &ArenaTree<SplittingNode, ()>) -> bool {
    (0..tree.size())
        .filter(|n| tree.arena[*n].val.children.is_empty())
        .all(|n| tree.arena[n].val.states.len() == 1)
}
