//! Learner Module
//!
//! This module contains the implementations for the L# library,
//!  specifically the implementations for an observation tree,
//!  computing witnesses, and the learner itself.

/// Provide functions for computing witnesses.
pub mod apartness;
/// Main algorithm implementation.
pub mod l_sharp;
/// Observation Trees are the main data structure we use to store query information.
pub mod obs_tree;
