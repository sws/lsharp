use std::collections::{HashMap, VecDeque};
use std::fmt::Debug;
use std::hash::BuildHasher;

use datasize::DataSize;
use serde::{Deserialize, Serialize};

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;
use crate::util::data_structs::arena_tree::ArenaTree;
use crate::util::toolbox;

use super::{DeletableObservationTree, ObservationTree};

#[derive(
    Copy,
    Clone,
    DataSize,
    Default,
    Debug,
    Eq,
    Hash,
    PartialEq,
    PartialOrd,
    Ord,
    Deserialize,
    Serialize,
)]
pub struct CompTreeState {
    idx: u32,
    offset: u32,
}

impl CompTreeState {
    #[must_use]
    fn new(idx: u32, offset: u32) -> Self {
        Self { idx, offset }
    }

    #[must_use]
    fn idx(self) -> u32 {
        self.idx
    }

    #[must_use]
    fn offset(self) -> u32 {
        self.offset
    }
}

/// A `Node` can have two forms: a `Normal` have at least two children,
/// and a `Compressed` Node has at most one child.
#[derive(Debug, Clone, DataSize, derive_more::IsVariant, derive_more::From)]
enum ObsNode<S>
where
    S: BuildHasher + Default,
{
    /// `Normal` nodes are used when we have two or more children.
    Norm(NormalNode<S>),
    /// `Compressed` nodes are used when we have at most one child.
    Comp(Compressed),
}

impl<S> ObsNode<S>
where
    S: BuildHasher + Default,
{
    fn compressed(idx: u32) -> Self {
        Self::Comp(Compressed::new_with_idx(idx))
    }
}

#[derive(Debug, Clone, Default, DataSize, Serialize, Deserialize)]
struct NormalNode<S>
where
    S: BuildHasher + Default,
{
    // The value of the map is the output symbol and the index of the next node.
    data: HashMap<InputSymbol, (OutputSymbol, CompTreeState), S>,
}

impl<S: BuildHasher + Default> NormalNode<S> {
    /// Add transition to the map of this node.
    fn add_transition(&mut self, i: InputSymbol, o: OutputSymbol, s: CompTreeState) {
        self.data.insert(i, (o, s));
    }
}

#[derive(Debug, Default, Clone, DataSize)]
struct Compressed {
    data: Vec<(InputSymbol, OutputSymbol)>,
    /// Assigning anything else other than a `NormalNode` state will lead to UB!
    ///
    /// The general idea is that as soon as a compressed node "ends", i.e., when the
    /// `data` sequence in the compressed node is finished, we mark the next node with
    /// this field. As you can imagine, this field is only meant to be used when we
    /// have a compressed node which points to a  normal node; in other words, that
    /// there exists a path in the observation tree which then splits.
    next_node: Option<CompTreeState>,
}

impl Compressed {
    #[must_use]
    fn new_with_idx(_idx: u32) -> Self {
        Self::default()
    }

    #[inline]
    #[must_use]
    fn is_terminal(&self) -> bool {
        self.next_node.is_none()
    }
}

#[derive(Debug, DataSize)]
pub struct CompObsTree<S>
where
    S: BuildHasher + Default,
{
    inner: ArenaTree<ObsNode<S>, InputSymbol>,
    #[data_size(skip)]
    input_size: usize,
}

impl<S: BuildHasher + Default + Debug> ObservationTree<InputSymbol, OutputSymbol>
    for CompObsTree<S>
{
    type S = CompTreeState;

    fn insert_observation(
        &mut self,
        start: Option<CompTreeState>,
        input_seq: &[InputSymbol],
        output_seq: &[OutputSymbol],
    ) -> CompTreeState {
        let mut curr = start.unwrap_or_else(|| CompTreeState::new(0, 0));
        let mut obs_iterator = Iterator::zip(input_seq.iter(), output_seq.iter());
        while !self.is_terminal_compressed(curr) {
            let Some((i, o)) = obs_iterator.next() else {
                return curr;
            };
            curr = self.add_transition_get_destination(curr, *i, *o);
        }
        let rest_data: Vec<_> = obs_iterator.map(|(i, o)| (*i, *o)).collect();
        self.insert_observation_terminal_compressed(rest_data, curr)
    }

    fn get_access_seq(&self, state: CompTreeState) -> Vec<InputSymbol> {
        self.get_transfer_seq(state, CompTreeState::new(0, 0))
    }

    #[allow(clippy::cast_possible_truncation)]
    fn get_transfer_seq(
        &self,
        to_state: CompTreeState,
        from_state: CompTreeState,
    ) -> Vec<InputSymbol> {
        if from_state == to_state {
            return vec![];
        }
        let mut access_seq = VecDeque::new();

        macro_rules! inputs_from_compressed {
            ($start:expr, $end:expr, $data:expr) => {
                $data[($start as usize)..($end as usize)]
                    .iter()
                    .map(|(i, _)| *i)
                    .rev()
            };
        }
        match (
            self.inner.ref_at(to_state.idx as usize),
            self.inner.ref_at(from_state.idx as usize),
        ) {
            (ObsNode::Comp(to_node), ObsNode::Norm(_)) => {
                for i in inputs_from_compressed!(0, to_state.offset, &to_node.data) {
                    access_seq.push_front(i);
                }
            }
            (ObsNode::Comp(to_node), ObsNode::Comp(_)) => {
                let same_index = to_state.idx == from_state.idx;
                // If the indices of the parent and child are the same, we only need to get
                // the inputs stored between the offsets and return.
                // Else we need to get the entire field.
                let start_offset = if same_index { from_state.offset } else { 0 };
                for i in inputs_from_compressed!(start_offset, to_state.offset, &to_node.data) {
                    access_seq.push_front(i);
                }
                if same_index {
                    return access_seq.into();
                }
            }
            _ => {}
        }
        let mut curr_state_raw = to_state.idx();

        loop {
            if from_state.idx() == curr_state_raw {
                // If the current_st and the from state are the same, then we check if the offsets match.
                // if from_state.offset() != 0 {
                // we need to get the offset state.
                if let ObsNode::Comp(c_node) = self.inner.ref_at(curr_state_raw as usize) {
                    let end = c_node.data.len() - 1;
                    for i in inputs_from_compressed!(from_state.offset, end, &c_node.data) {
                        access_seq.push_front(i);
                    }
                }
                break;
            }

            let (i, parent_idx) = self.inner.arena[curr_state_raw as usize]
                .parent
                .expect("No path found between parent and child node for a transfer sequence.");
            access_seq.push_front(i);

            if let ObsNode::Comp(c) = self.inner.ref_at(parent_idx) {
                if parent_idx != (from_state.idx as usize) {
                    let end = c.data.len() - 1;
                    for i in inputs_from_compressed!(0, end, &c.data) {
                        access_seq.push_front(i);
                    }
                }
            }
            curr_state_raw = parent_idx as u32;
        }
        access_seq.into()
    }

    fn get_observation(
        &self,
        start: Option<CompTreeState>,
        input_seq: &[InputSymbol],
    ) -> Option<Vec<OutputSymbol>> {
        let mut s = start.unwrap_or_else(|| CompTreeState::new(0, 0));
        let mut out_vec = Vec::with_capacity(input_seq.len());
        for &i in input_seq {
            let (o, d) = self.get_out_succ(s, i)?;
            out_vec.push(o);
            s = d;
        }
        Some(out_vec)
    }

    fn get_out_succ(
        &self,
        src: CompTreeState,
        input: InputSymbol,
    ) -> Option<(OutputSymbol, CompTreeState)> {
        let node = self.inner.ref_at(src.idx() as usize);
        match node {
            ObsNode::Norm(n_node) => {
                return n_node.data.get(&input).copied();
            }
            ObsNode::Comp(c_node) => {
                let src_offset = src.offset();
                // If the offset is equal to the length of the vector,
                // we will have to return None anyway.
                if c_node.data.is_empty() {
                    if let Some(nn) = c_node.next_node {
                        return self.get_out_succ(nn, input);
                    }
                }
                let (c_node_i, c_node_o) = c_node.data.get(src_offset as usize)?;
                if *c_node_i == input {
                    // Now, if the offset is equal to the max offset, the next
                    // node is actually one that is pointed to by the next_node
                    // field of the c_node. If that doesn't exist, we basically
                    // don't _have_ a next node.
                    if src_offset as usize == c_node.data.len() - 1 {
                        if let Some(nxt) = c_node.next_node {
                            return Some((*c_node_o, nxt));
                        }
                    }
                    // Current node is terminal.
                    let dest = CompTreeState::new(src.idx(), src_offset + 1);
                    Some((*c_node_o, dest))
                } else {
                    None
                }
            }
        }
    }

    fn get_succ(&self, src: CompTreeState, input: &[InputSymbol]) -> Option<CompTreeState> {
        input.iter().try_fold(src, |s, i| self._get_succ(s, *i))
    }

    fn no_succ_defined(
        &self,
        basis: &[CompTreeState],
        sort: bool,
    ) -> Vec<(CompTreeState, InputSymbol)> {
        let inputs = toolbox::inputs_iterator(self.input_size);
        let mut ret: Vec<_> =
            itertools::Itertools::cartesian_product(basis.iter().copied(), inputs)
                .filter(|(bs, i)| self._get_succ(*bs, *i).is_none())
                .collect();
        let acc_len = |s| self.get_access_seq(s).len();
        let len_cmp = |a, b| acc_len(a).cmp(&acc_len(b));
        if sort {
            ret.sort_unstable_by(|(a, _), (b, _)| len_cmp(*a, *b));
        }
        ret
    }

    fn size(&self) -> usize {
        self.inner.size()
    }

    fn input_size(&self) -> usize {
        self.input_size
    }

    fn tree_and_hyp_states_apart_sink(
        &self,
        s_t: CompTreeState,
        s_h: State,
        fsm: &Mealy,
        sink_output: OutputSymbol,
        _depth: usize,
    ) -> bool {
        let mut queue = VecDeque::from([(s_t, s_h)]);

        while let Some((q_t, q_h)) = queue.pop_front() {
            match self.ref_at(q_t) {
                ObsNode::Norm(n_node) => {
                    for (t_i, (t_o, t_d)) in &n_node.data {
                        let (h_d, h_o) = fsm.step_from(q_h, *t_i);
                        if h_o == *t_o {
                            if *t_o == sink_output {
                                continue;
                            }
                            queue.push_back((*t_d, h_d));
                        } else {
                            return true;
                        }
                    }
                }
                // The compressed node handling is a bit more complicated!
                ObsNode::Comp(c_node) => {
                    let tree_transitions = c_node.data.get(q_t.offset as usize..);
                    if let Some(trans) = tree_transitions {
                        // From these transitions, we have to take the difference between the
                        // current depth and the max depth.
                        let mut h_c = q_h;
                        let mut add_dest_to_queue = true;
                        'comp_trans: for (t_i, t_o) in trans {
                            let (h_d, h_o) = fsm.step_from(h_c, *t_i);
                            h_c = h_d;
                            if h_o == *t_o {
                                if *t_o == sink_output {
                                    add_dest_to_queue = false;
                                    break 'comp_trans;
                                }
                            } else {
                                return true;
                            }
                        }
                        if add_dest_to_queue {
                            if let Some(dest) = &c_node.next_node {
                                queue.push_back((*dest, h_c));
                            }
                        }
                    }
                }
            }
        }
        false
    }
}

impl<S: BuildHasher + Default + Debug> CompObsTree<S> {
    #[must_use]
    pub fn new(input_size: usize) -> Self {
        let root_node = ObsNode::Norm(NormalNode::default());
        let mut inner = ArenaTree::default();
        inner.node(root_node);
        Self { inner, input_size }
    }

    fn _get_succ(&self, src: CompTreeState, input: InputSymbol) -> Option<CompTreeState> {
        self.get_out_succ(src, input).map(|x| x.1)
    }

    fn ref_at(&self, st: CompTreeState) -> &ObsNode<S> {
        &self
            .inner
            .arena
            .get(st.idx as usize)
            .expect("No such state in O.T")
            .val
    }

    /// A state is terminal compressed if it is located at a compressed node with no child
    /// and the offset of the state is equal to the length of the data vector in the node (
    /// i.e., we want to insert something at the end of the vector.)
    fn is_terminal_compressed(&self, src: CompTreeState) -> bool {
        let node = self.ref_at(src);
        if let ObsNode::Comp(node) = node {
            if node.is_terminal() {
                let no_next_state = node.next_node.is_none();
                let final_state = src.offset as usize == node.data.len();
                return no_next_state && final_state;
            }
        }
        false
    }

    fn insert_node(
        &mut self,
        node: ObsNode<S>,
        parent_input: InputSymbol,
        parent_idx: u32,
    ) -> usize {
        self.inner
            .node_with_parent(node, parent_idx as usize, parent_input)
    }

    #[allow(clippy::too_many_lines)]
    fn add_transition_get_destination(
        &mut self,
        src: CompTreeState,
        i: InputSymbol,
        o: OutputSymbol,
    ) -> CompTreeState {
        if let Some((_, d)) = self.get_out_succ(src, i) {
            return d;
        }
        // We know now that this transition doesn't exist, so we have to add it.
        #[allow(clippy::cast_possible_truncation)]
        let tree_size = self.inner.size() as u32;
        let node_is_normal = self.inner.arena[src.idx() as usize].val.is_norm();
        if node_is_normal {
            self.add_transition_normal_node(src, i, o)
        } else {
            self.add_transition_compressed_node(tree_size, src, i, o)
        }
    }

    fn add_transition_normal_node(
        &mut self,
        src: CompTreeState,
        i: InputSymbol,
        o: OutputSymbol,
    ) -> CompTreeState {
        // If the current node is a normal node, things are very simple: we simply create a compressed node
        // for the destination and assign the transitions. The entry of the node and transition assignment are
        // a bit weird, but that is unavoidable due to Rust's borrowing rules.
        #[allow(clippy::cast_possible_truncation)]
        let dest_node_idx = self.inner.size() as u32;
        let dest_node = ObsNode::compressed(dest_node_idx);
        let dest_state = CompTreeState::new(dest_node_idx, 0);
        let ObsNode::Norm(n_node) = &mut self.inner.arena[src.idx() as usize].val else {
            panic!("Safe");
        };
        n_node.add_transition(i, o, dest_state);
        let assigned_idx = self.insert_node(dest_node, i, src.idx());
        assert_eq!(dest_node_idx as usize, assigned_idx);
        dest_state
    }

    fn add_transition_compressed_node(
        &mut self,
        tree_size: u32,
        src: CompTreeState,
        i: InputSymbol,
        o: OutputSymbol,
    ) -> CompTreeState {
        let ObsNode::Comp(c_node) = &mut self.inner.arena[src.idx as usize].val else {
            panic!("Safe");
        };

        let offset = src.offset;
        let c_data = c_node.data.clone();
        let c_next_node = c_node.next_node;
        #[allow(clippy::cast_possible_truncation)]
        let data_len = c_node.data.len() as u32;

        assert!(
            offset <= data_len,
            "Offset can never be *greater* than the data length!"
        );

        let is_terminal = c_node.is_terminal();
        let c_node_idx = src.idx;
        if offset == data_len {
            assert!(
                is_terminal,
                "We should not be adding transitions to the `next-next` state!"
            );
            c_node.data.push((i, o));
            return CompTreeState::new(c_node_idx, offset + 1);
        }

        // OFFSET < DATA_LEN
        // We are going to be branching somewhere in the defined transitions.
        if offset == 0 && data_len == 1 {
            self.initial_case_single_trans(tree_size, c_node_idx, i, o, &c_data, c_next_node)
        } else if offset == 0 {
            self.initial_case(tree_size, c_node_idx, i, o, &c_data, c_next_node)
        } else if offset == data_len - 1 {
            self.insert_at_end_of_vec(&c_data, tree_size, c_next_node, i, o, src)
        } else {
            self.insert_other(src, data_len, tree_size, &c_data, c_next_node, (i, o))
        }
    }

    fn insert_other(
        &mut self,
        src: CompTreeState,
        data_len: u32,
        tree_size: u32,
        c_data: &[(InputSymbol, OutputSymbol)],
        c_next_node: Option<CompTreeState>,
        (i, o): (InputSymbol, OutputSymbol),
    ) -> CompTreeState {
        let CompTreeState {
            idx: c_node_idx,
            offset,
        } = src;
        assert_ne!(offset, 0);
        assert!(data_len - offset > 1);
        let seq_len_to_give = ((offset + 1) as usize)..(data_len as usize);
        let new_dest_idx = tree_size;
        let new_dest_node = ObsNode::compressed(new_dest_idx);
        let old_dest_idx = new_dest_idx + 1;
        let old_dest_data: Vec<_> = c_data[seq_len_to_give].into();
        if old_dest_data.is_empty() && c_next_node.is_some() {
            unreachable!("We tried to make a compressed intermediate node with no data!");
        }
        if let Some(CompTreeState { idx, offset: _ }) = c_next_node {
            let parent = self.inner.arena[idx as usize]
                .parent
                .as_mut()
                .expect("Unreachable!");
            parent.1 = old_dest_idx as usize;
        }
        let old_dest_node = ObsNode::Comp(Compressed {
            data: old_dest_data,
            next_node: c_next_node,
        });
        let old_leading_trans = *c_data.get(offset as usize).expect("Safe");
        let split_node_idx = old_dest_idx + 1;
        let mut split_node = NormalNode::default();
        split_node.add_transition(i, o, CompTreeState::new(new_dest_idx, 0));
        split_node.add_transition(
            old_leading_trans.0,
            old_leading_trans.1,
            CompTreeState::new(old_dest_idx, 0),
        );
        assert_eq!(
            new_dest_idx as usize,
            self.inner
                .node_with_parent(new_dest_node, split_node_idx as usize, i)
        );
        assert_eq!(
            old_dest_idx as usize,
            self.inner.node_with_parent(
                old_dest_node,
                split_node_idx as usize,
                old_leading_trans.0
            )
        );
        assert_eq!(
            split_node_idx as usize,
            self.inner.node_with_parent(
                ObsNode::Norm(split_node),
                c_node_idx as usize,
                c_data
                    .get((offset - 1) as usize)
                    .map(|x| x.0)
                    .expect("Safe")
            )
        );
        let ObsNode::Comp(node) = &mut self.inner.arena[c_node_idx as usize].val else {
            unreachable!("Impossible.");
        };
        // Everything after, give away.
        // This node will contain the new transition. That is,
        // it will be an empty node, acting as a destination for the
        // transition (i,o).
        let new_data = node.data.get(..(offset as usize)).unwrap_or_default();
        node.data = new_data.to_vec();
        let dest_state = CompTreeState::new(split_node_idx, 0);
        node.next_node = Some(dest_state);
        CompTreeState::new(new_dest_idx, 0)
    }

    fn insert_at_end_of_vec(
        &mut self,
        c_data: &[(InputSymbol, OutputSymbol)],
        tree_size: u32,
        c_next_node: Option<CompTreeState>,
        i: InputSymbol,
        o: OutputSymbol,
        src: CompTreeState,
    ) -> CompTreeState {
        // There needs to be one more special case wherein offset is just one
        // smaller than the data_len: i.e., we are inserting at the last spot
        // in the data vector. As I'd (cynically) noted earlier: we do need to
        // treat this as a special case!

        // We want to insert at the very end of the data vector: i.e., we are splitting
        // the final transition of the vector.
        let CompTreeState {
            idx: c_node_idx,
            offset,
        } = src;
        let (&old_trans, transs_to_keep_with_current) = c_data.split_last().expect("Safe");
        let new_dest_idx = tree_size;
        let new_dest_state;
        let (old_dest_state, split_node_idx) = if let Some(nxt_node) = c_next_node {
            let n_dest_state = {
                let new_dest_node = ObsNode::compressed(new_dest_idx);
                assert_eq!(
                    new_dest_idx as usize,
                    self.inner.node_with_parent(
                        new_dest_node,
                        (tree_size + 1).try_into().unwrap(),
                        i,
                    )
                );
                CompTreeState::new(new_dest_idx, 0)
            };
            new_dest_state = n_dest_state;
            // We also need to update the parent of the old dest state here!
            // It should point to the split_node!
            (nxt_node, new_dest_idx + 1)
        } else {
            // Create a new empty node.
            let old_dest_node_idx = new_dest_idx + 1;

            let split_node_idx = old_dest_node_idx + 1;
            let n_dest_state = {
                let new_dest_node = ObsNode::compressed(new_dest_idx);
                assert_eq!(
                    new_dest_idx as usize,
                    self.inner.node_with_parent(
                        new_dest_node,
                        (split_node_idx).try_into().unwrap(),
                        i,
                    )
                );
                CompTreeState::new(new_dest_idx, 0)
            };
            new_dest_state = n_dest_state;
            let old_dest_node = ObsNode::compressed(old_dest_node_idx);
            assert_eq!(
                (old_dest_node_idx) as usize,
                self.inner.node_with_parent(
                    old_dest_node,
                    split_node_idx.try_into().unwrap(),
                    old_trans.0
                )
            );
            (CompTreeState::new(old_dest_node_idx, 0), split_node_idx)
        };
        {
            let (i, x) = self.inner.arena[old_dest_state.idx as usize]
                .parent
                .as_mut()
                .expect("Unreachable.");
            {
                *i = old_trans.0;
                *x = split_node_idx as usize;
            }
        }

        let mut split_node = NormalNode::default();
        split_node.add_transition(i, o, new_dest_state);
        split_node.add_transition(old_trans.0, old_trans.1, old_dest_state);
        let split_node = ObsNode::Norm(split_node);
        assert_eq!(
            split_node_idx as usize,
            self.inner.node_with_parent(
                split_node,
                c_node_idx.try_into().unwrap(),
                transs_to_keep_with_current.last().expect("Safe").0
            )
        );
        // Hence the current next_node will become the destination node for the old last transition.

        // Remove last trans from c_node!
        let ObsNode::Comp(node) = &mut self.inner.arena[c_node_idx as usize].val else {
            unreachable!("Impossible.");
        };
        node.data.remove(offset.try_into().unwrap());
        node.next_node = Some(CompTreeState::new(split_node_idx, 0));
        new_dest_state
    }

    fn initial_case_single_trans(
        &mut self,
        tree_size: u32,
        c_node_idx: u32,
        i: InputSymbol,
        o: OutputSymbol,
        c_data: &[(InputSymbol, OutputSymbol)],
        c_next_node: Option<CompTreeState>,
    ) -> CompTreeState {
        // Node only contains one transition.
        // We special-case offset = 0, as we must convert the *current* node
        // from a Compressed node to a Normal node.
        // We have created a new destination node for the split node.

        // If the next node is set, then we will set the destination
        // for the old transition to the next node, else we will
        // construct a new node and use that one.
        let new_dest_idx = tree_size;
        let new_dest_node = ObsNode::compressed(new_dest_idx);
        assert_eq!(
            new_dest_idx as usize,
            self.inner
                .node_with_parent(new_dest_node, c_node_idx as usize, i)
        );
        let mut new_curr_node = NormalNode::default();
        new_curr_node
            .data
            .insert(i, (o, CompTreeState::new(new_dest_idx, 0)));
        let old_trans = *c_data.get(0).expect("Safe");
        let old_dest_state = if let Some(c_next) = c_next_node {
            // Next node exists, use that one as the destination.
            c_next
        } else {
            // Construct a new destination for the old transition.
            let old_dest_idx = new_dest_idx + 1;
            let mut old_dest_node = Compressed::new_with_idx(old_dest_idx);
            old_dest_node.next_node = c_next_node;
            let old_dest_node = old_dest_node.into();
            assert_eq!(
                old_dest_idx as usize,
                self.inner
                    .node_with_parent(old_dest_node, c_node_idx as usize, old_trans.0,)
            );
            CompTreeState::new(old_dest_idx, 0)
        };
        new_curr_node
            .data
            .insert(old_trans.0, (old_trans.1, old_dest_state));
        self.inner.arena[c_node_idx as usize].val = ObsNode::Norm(new_curr_node);
        CompTreeState::new(new_dest_idx, 0)
    }

    fn initial_case(
        &mut self,
        tree_size: u32,
        c_node_idx: u32,
        i: InputSymbol,
        o: OutputSymbol,
        c_data: &[(InputSymbol, OutputSymbol)],
        c_next_node: Option<CompTreeState>,
    ) -> CompTreeState {
        let src = c_node_idx;
        // We special-case offset = 0, as we must convert the *current* node
        // from a Compressed node to a Normal node.
        let new_dest_idx = tree_size;
        let new_dest_node = ObsNode::compressed(new_dest_idx);
        // We have created a new destination node for the split node.
        assert_eq!(
            new_dest_idx as usize,
            self.inner
                .node_with_parent(new_dest_node, c_node_idx as usize, i)
        );
        let mut new_curr_node = NormalNode::<S>::default();
        new_curr_node
            .data
            .insert(i, (o, CompTreeState::new(new_dest_idx, 0)));
        let new_int_idx = new_dest_idx + 1;
        let inputs_to_int_node = *c_data.get(0).expect("Safe");
        new_curr_node.data.insert(
            inputs_to_int_node.0,
            (inputs_to_int_node.1, CompTreeState::new(new_int_idx, 0)),
        );
        self.inner.arena[c_node_idx as usize].val = ObsNode::Norm(new_curr_node);
        let mut new_intermediate_node = Compressed::new_with_idx(new_int_idx);
        new_intermediate_node.data.extend_from_slice(&c_data[1..]);

        if new_intermediate_node.data.is_empty() {
            unreachable!("Intermediate node has no data. Invariant broken!");
        }
        new_intermediate_node.next_node = c_next_node;
        if let Some(nn) = c_next_node {
            let x = self.inner.arena[nn.idx as usize].parent.expect("Safe");
            self.inner.arena[nn.idx as usize].parent = Some((x.0, new_int_idx.try_into().unwrap()));
        }
        let new_int_node = ObsNode::Comp(new_intermediate_node);

        assert_eq!(
            new_int_idx as usize,
            self.insert_node(new_int_node, inputs_to_int_node.0, src)
        );
        CompTreeState::new(new_dest_idx, 0)
    }

    fn insert_observation_terminal_compressed(
        &mut self,
        mut rest_data: Vec<(InputSymbol, OutputSymbol)>,
        curr: CompTreeState,
    ) -> CompTreeState {
        let ObsNode::Comp(c_node) = &mut self.inner.arena[curr.idx as usize].val else {
            unreachable!("Cannot be a NormalNode");
        };
        c_node.data.append(&mut rest_data);
        CompTreeState {
            idx: curr.idx,
            offset: c_node.data.len() as u32,
        }
    }
}

impl<S, I, O> DeletableObservationTree<I, O> for CompObsTree<S>
where
    S: BuildHasher + Default + Debug,
    CompObsTree<S>: ObservationTree<I, O>,
{
    fn delete_subtree_at(&mut self, _acc_seq: &[I]) {
        todo!()
    }

    fn clear_tree(&mut self) {
        let new_tree = CompObsTree::new(self.input_size);
        *self = new_tree;
    }
}

#[cfg(test)]
mod tests {
    use std::{hash::BuildHasherDefault, path::Path};

    use crate::{
        definitions::{
            mealy::{InputSymbol, Mealy, OutputSymbol},
            FiniteStateMachine,
        },
        learner::obs_tree::{compressed::CompTreeState, ObservationTree},
        util::parsers::machine::read_mealy_from_file,
    };

    use super::CompObsTree;

    use fnv::FnvHashSet;
    use itertools::Itertools;
    use proptest::{collection, proptest, strategy::Strategy};
    use rand::{rngs::StdRng, seq::SliceRandom, Rng, SeedableRng};
    use rand_distr::Uniform;
    use rstest::rstest;

    type DefaultFxHasher = BuildHasherDefault<rustc_hash::FxHasher>;
    type OTree = CompObsTree<DefaultFxHasher>;

    fn load_basic_fsm(name: &str) -> Mealy {
        let file_name = Path::new(name);
        read_mealy_from_file(file_name.to_str().expect("Safe")).0
    }

    fn try_gen_inputs() -> impl Strategy<Value = (Vec<InputSymbol>, Vec<OutputSymbol>)> {
        let num_inputs = 13usize;
        let fsm = load_basic_fsm("tests/src_models/BitVise.dot");
        #[allow(clippy::cast_possible_truncation)]
        let input_symbol_strat = (0..num_inputs)
            .prop_map(|x| x as u16)
            .prop_map(InputSymbol::from);
        let input_seq_strat = collection::vec(input_symbol_strat, 1..100);
        input_seq_strat
            .prop_map(move |is| (fsm.trace(&is).1.to_vec(), is))
            .prop_map(|(os, is)| (is, os))
    }

    fn gen_seq_sets(
        max_size_set: usize,
        x: impl Strategy<Value = (Vec<InputSymbol>, Vec<OutputSymbol>)>,
    ) -> impl Strategy<Value = Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)>> {
        collection::vec(x, 1..max_size_set)
    }

    /// FIXED.
    #[test]
    fn regression_xfer_seq_maintained_1() {
        let xyz = vec![vec![10, 12, 0, 0], vec![10, 12, 0, 1]];
        let xyz_out = vec![vec![1, 1, 0, 3], vec![1, 1, 0, 3]];
        let xyz = xyz
            .into_iter()
            .map(|is: Vec<u16>| is.into_iter().map(InputSymbol::from).collect_vec())
            .collect_vec();
        let xyz_out = xyz_out
            .into_iter()
            .map(|os| os.into_iter().map(OutputSymbol::from).collect_vec())
            .collect_vec();
        let seqs = xyz.into_iter().zip(xyz_out.into_iter()).collect_vec();

        let mut ret = OTree::new(13);
        for (idx, (is, os)) in seqs.iter().enumerate() {
            ret.insert_observation(None, is, os);
            assert!(ret.get_succ(CompTreeState::default(), is).is_some());
            for (in_idx, (iis, _)) in seqs[0..idx].iter().enumerate() {
                let ds = ret.get_succ(CompTreeState::default(), iis).unwrap();
                let rx_acc = ret.get_access_seq(ds);
                assert_eq!(
                    iis, &rx_acc,
                    "Failed at idx {idx} and sub-idx {in_idx}, \n after inserting {is:?}"
                );
                for i in 0..iis.len() {
                    let (pref, suff) = iis.split_at(i);
                    let pref_dest = ret.get_succ(CompTreeState::default(), pref).unwrap();
                    let xfer_seq = ret.get_transfer_seq(ds, pref_dest);
                    assert_eq!(suff, &xfer_seq);
                }
            }
        }
    }

    proptest! {

        #[test]
        fn xfer_seq_maintained(seqs in gen_seq_sets(100,try_gen_inputs())){
            let mut ret = OTree::new(13);
            for (idx, (is, os)) in seqs.iter().enumerate() {
                ret.insert_observation(None, is, os);
                assert!(ret.get_succ(CompTreeState::default(), is).is_some());
                for (in_idx, (iis, _)) in seqs[0..idx].iter().enumerate() {
                    let ds = ret.get_succ(CompTreeState::default(), iis).unwrap();
                    let rx_acc = ret.get_access_seq(ds);
                    assert_eq!(
                        iis, &rx_acc,
                        "Failed at idx {idx} and sub-idx {in_idx}, \n after inserting {is:?}"
                    );
                    for i in 0..iis.len() {
                        let (pref, suff) = iis.split_at(i);
                        let pref_dest = ret.get_succ(CompTreeState::default(), pref).unwrap();
                        let xfer_seq = ret.get_transfer_seq(ds, pref_dest);
                        assert_eq!(suff, &xfer_seq);
                    }
                }
            }
        }

        #[test]
        fn access_seq_maintained(seqs in gen_seq_sets(500,try_gen_inputs())){
              let mut ret = OTree::new(13);
              for (idx, (is, os)) in seqs.iter().enumerate() {
                ret.insert_observation(None, is, os);
                assert!(ret.get_succ(CompTreeState::default(), is).is_some());
                for (in_idx, (iis, _)) in seqs[0..idx].iter().enumerate() {
                 let ds = ret.get_succ(CompTreeState::default(), iis).unwrap();
                let rx_acc = ret.get_access_seq(ds);
                assert_eq!(
                    iis, &rx_acc,
                    "Failed at idx {idx} and sub-idx {in_idx}, \n after inserting {is:?}"
                );
                }
            }
            for (idx, (is, _)) in seqs.iter().enumerate() {
                if let Some(dest) = ret.get_succ(CompTreeState::default(), is) {
                    let acc_seq = ret.get_access_seq(dest);
                    assert_eq!(is, &acc_seq);
                } else {
                    // println!("Size of tree: {}", ret.size());
                    panic!("Seq number {idx} : {is:?} is not in tree?!");
                }
            }
        }

    }

    /// Given a path to an FSM and construct `num` words over the FSM.
    fn vec_of_observations(name: &str, num: usize) -> Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)> {
        let fsm = load_basic_fsm(name);
        let mut ret: FnvHashSet<(Vec<InputSymbol>, Vec<OutputSymbol>)> = FnvHashSet::default();
        let inputs = fsm.input_alphabet();
        let mut rng = StdRng::seed_from_u64(num as u64);
        let len_range: Uniform<usize> = Uniform::new_inclusive(1, 500);
        let len_seqs_vec = (&mut rng).sample_iter(len_range).take(num).collect_vec();
        for len in len_seqs_vec {
            // Generate a sequence of inputs and outputs with len.
            let in_seq = inputs.choose_multiple(&mut rng, len).copied().collect_vec();
            let out_seq = fsm.trace(&in_seq).1.to_vec();
            ret.insert((in_seq, out_seq));
        }
        ret.into_iter().collect()
    }

    #[rstest]
    #[case(REGRESSION_TEST_1_DATA_1)] // FIXED.
    #[case(REGRESSION_TEST_1_DATA_2)] // FIXED.
    fn regression_test_1(#[case] traces: &str) {
        let seqs: Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)> =
            serde_json::from_str(traces).expect("Safe");
        let mut ret = OTree::new(5);
        for (idx, (is, os)) in seqs.iter().enumerate() {
            ret.insert_observation(None, is, os);
            for (x, (iis, oos)) in seqs.iter().enumerate().take(idx + 1) {
                //  0..=idx {
                let dest = ret.insert_observation(None, iis, oos);
                let acc_seq = ret.get_access_seq(dest);
                assert_eq!(iis, &acc_seq, "Idx: {idx}, inner: {x}");
            }
        }
        for (is, _) in &seqs {
            let dest = ret.get_succ(CompTreeState::default(), is).unwrap();
            let acc_seq = ret.get_access_seq(dest);
            assert_eq!(is, &acc_seq);
        }
    }

    /// FIXED.    
    #[test]
    fn reg_acc_seq_maintained_4() {
        let xyz = vec![vec![0], vec![11, 4, 0, 0], vec![11, 4, 1]];
        let xyz_out = vec![vec![0], vec![2, 6, 6, 6], vec![2, 6, 6]];
        let xyz = xyz
            .into_iter()
            .map(|is: Vec<u16>| is.into_iter().map(InputSymbol::from).collect_vec())
            .collect_vec();
        let xyz_out = xyz_out
            .into_iter()
            .map(|os| os.into_iter().map(OutputSymbol::from).collect_vec())
            .collect_vec();
        let seqs = xyz.into_iter().zip(xyz_out.into_iter()).collect_vec();
        let mut ret = OTree::new(13);

        impl_add_access_seqs(&seqs, &mut ret);
        impl_check_access_seqs(&seqs, &mut ret);
    }

    /// FIXED.    
    #[test]
    fn reg_acc_seq_maintained_3() {
        let xyz = vec![vec![12, 1, 0, 7, 0], vec![12, 1, 0, 7, 1], vec![12, 1, 1]];
        let xyz_out = vec![vec![1, 0, 3, 1, 3], vec![1, 0, 3, 1, 3], vec![1, 0, 3]];
        let xyz = xyz
            .into_iter()
            .map(|is: Vec<u16>| is.into_iter().map(InputSymbol::from).collect_vec())
            .collect_vec();
        let xyz_out = xyz_out
            .into_iter()
            .map(|os| os.into_iter().map(OutputSymbol::from).collect_vec())
            .collect_vec();
        let seqs = xyz.into_iter().zip(xyz_out.into_iter()).collect_vec();
        let mut ret = OTree::new(13);
        impl_add_access_seqs(&seqs, &mut ret);
        impl_check_access_seqs(&seqs, &mut ret);
    }

    fn impl_add_access_seqs(
        seqs: &[(Vec<InputSymbol>, Vec<OutputSymbol>)],
        ret: &mut CompObsTree<DefaultFxHasher>,
    ) {
        for (idx, (is, os)) in seqs.iter().enumerate() {
            ret.insert_observation(None, is, os);
            assert!(ret.get_succ(CompTreeState::default(), is).is_some());
            for (in_idx, (iis, _)) in seqs[0..idx].iter().enumerate() {
                let ds = ret.get_succ(CompTreeState::default(), iis).unwrap();
                let rx_acc = ret.get_access_seq(ds);
                assert_eq!(
                    iis, &rx_acc,
                    "Failed at idx {idx} and sub-idx {in_idx}, \n after inserting {is:?}"
                );
            }
        }
    }

    fn impl_check_access_seqs(
        seqs: &[(Vec<InputSymbol>, Vec<OutputSymbol>)],
        ret: &mut CompObsTree<DefaultFxHasher>,
    ) {
        for (idx, (is, _)) in seqs.iter().enumerate() {
            if let Some(dest) = ret.get_succ(CompTreeState::default(), is) {
                let acc_seq = ret.get_access_seq(dest);
                assert_eq!(is, &acc_seq);
            } else {
                panic!("Seq number {idx} : {is:?} is not in tree?!");
            }
        }
    }

    /// FIXED.
    #[test]
    fn reg_acc_seq_maintained_2() {
        let xyz = vec![vec![7, 2, 0, 0], vec![7, 2, 0, 1], vec![7, 2, 1]];
        let xyz_out = vec![vec![1, 0, 3, 3], vec![1, 0, 3, 3], vec![1, 0, 3]];
        let xyz = xyz
            .into_iter()
            .map(|is: Vec<u16>| is.into_iter().map(InputSymbol::from).collect_vec())
            .collect_vec();
        let xyz_out = xyz_out
            .into_iter()
            .map(|os| os.into_iter().map(OutputSymbol::from).collect_vec())
            .collect_vec();
        let seqs = xyz.into_iter().zip(xyz_out.into_iter()).collect_vec();
        let mut ret = OTree::new(13);
        impl_add_access_seqs(&seqs, &mut ret);
        impl_check_access_seqs(&seqs, &mut ret);
    }

    /// FIXED.
    #[test]
    fn reg_acc_seq_maintained_1() {
        let xyz = vec![vec![8, 10, 0], vec![8, 10, 1], vec![8, 0], vec![8, 10, 2]];
        let xyz_out = vec![vec![0, 1, 3], vec![0, 1, 3], vec![0, 3], vec![0, 1, 3]];
        let xyz = xyz
            .into_iter()
            .map(|is: Vec<u16>| is.into_iter().map(InputSymbol::from).collect_vec())
            .collect_vec();
        let xyz_out = xyz_out
            .into_iter()
            .map(|os| os.into_iter().map(OutputSymbol::from).collect_vec())
            .collect_vec();
        let seqs = xyz.into_iter().zip(xyz_out.into_iter()).collect_vec();

        let mut ret = OTree::new(13);
        for (idx, (is, os)) in seqs.iter().enumerate() {
            ret.insert_observation(None, is, os);
            assert!(ret.get_succ(CompTreeState::default(), is).is_some());
            for (in_idx, (iis, _)) in seqs[0..idx].iter().enumerate() {
                assert!(
                    ret.get_succ(CompTreeState::default(), iis).is_some(),
                    "Seq missing at index: {idx}, sub-idx: {in_idx}"
                );
            }
        }
        impl_check_access_seqs(&seqs, &mut ret);
    }

    /// FIXED.
    #[test]
    fn regression_test_3() {
        let seqs: Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)> =
            serde_json::from_str(REGRESSION_TEST_3_DATA).expect("Safe");
        let mut ret = OTree::new(13);
        for (is, os) in &seqs {
            ret.insert_observation(None, is, os);
        }
    }

    /// FIXED.    
    #[test]
    fn regression_test_2() {
        let name = "tests/src_models/BitVise.dot";
        let num = 100;
        let num_inputs = load_basic_fsm(name).input_alphabet().len();
        let seqs = vec_of_observations(name, num);
        let mut ret = OTree::new(num_inputs);
        for (is, os) in &seqs {
            ret.insert_observation(None, is, os);
            assert!(ret.get_succ(CompTreeState::default(), is).is_some());
        }
    }
    const REGRESSION_TEST_1_DATA_1: &str = "[[[3,2,1,4,0],[1,1,0,1,0]],\
[[4,2,1,3],[1,1,0,1]],\
[[0,3,4,1,2],[0,1,1,0,0]],\
[[2,4,1,3,0],[1,1,0,1,0]],\
[[3,4,1,0,2],[1,1,0,0,0]]]";

    const REGRESSION_TEST_1_DATA_2: &str = "[[[4,3,2,1,0],[1,1,1,0,0]],\
[[4,1,2,3,0],[1,0,0,1,0]],\
[[0,1,3,2,4],[0,0,1,0,1]],\
[[4,2,3,0,1],[1,1,1,0,0]],\
[[4,1,2,0,3],[1,0,0,0,1]],\
[[1,3,4,2,0],[0,1,1,0,0]],\
[[2,1,4,3,0],[1,0,1,1,0]]]";

    const REGRESSION_TEST_3_DATA: &str = "[[[1,4,3,10,8,2,12,0],[0,3,3,1,3,3,1,3]],\
[[12,5,1,11,2,4,6,8,7,3,10,9,0],[1,1,0,3,3,3,3,3,3,3,3,3,3]],\
[[8,4,9,2,12,0,7,5,10,6,11,3,1],[0,3,3,3,1,3,1,1,1,3,3,3,3]],\
[[1,4,0,5,11,12,3,2,6,9,8,10,7],[0,3,3,1,3,1,3,3,3,3,3,3,1]],\
[[1,8,11,4,9,3,5,0,6,12,7,2,10],[0,3,3,3,3,3,1,3,3,3,3,3,3]],\
[[3,6,4,0,1,5,7,8,10,9,12,2,11],[0,3,3,3,3,3,3,3,3,3,1,3,3]],\
[[9,3,6,8,12,0,5,2,11,4,10,1,7],[2,5,3,3,3,3,3,3,3,3,3,3,1]],\
[[11,1,0,5,9,4,7,3,6,2,10,12,8],[2,7,5,1,3,3,1,3,3,3,3,1,3]],\
[[8,0,6,10,7,1,11,2,4,3,5,9,12],[0,3,3,3,1,3,3,3,3,3,1,3,1]],\
[[7,11,5,12,2,3,1,10,8,0,9,4,6],[1,2,1,1,5,3,3,1,3,3,3,3,3]],\
[[3,9,8,4,10,7,0,12,5,6,1,11],[0,3,3,3,1,1,3,1,1,3,3,3]],\
[[10,0,6,2,4,5,3,11,8,9,7,12,1],[1,0,3,3,3,3,3,3,3,3,3,3,3]],\
[[4,10,2,9,7,1,5,6,3,8,0,12,11],[0,1,3,3,1,3,1,3,3,3,3,3,3]],\
[[1,3,6,11,7,9,12,8,5,4,0,10,2],[0,3,3,3,3,3,3,3,3,3,3,3,3]],\
[[6,9,3,7,5,0,10,8,4,1,11,2,12],[0,3,3,3,3,3,3,3,3,3,3,3,1]],\
[[12,5,6,7,11,10,0,3,1,4,8,9,2],[1,1,0,3,3,3,3,3,3,3,3,3,3]],\
[[4,1,5,11,9,2,10,6,12,3,0,7,8],[0,3,1,3,3,3,1,3,3,3,3,3,3]],\
[[1,4,12,8,7,10,0,9,6,2,5,3,11],[0,3,1,3,1,1,3,3,3,3,3,3,3]],\
[[5,7,3,0,9,12,2,8],[1,1,0,3,3,1,3,3]],\
[[11,1,0,10,6,8,5,12,9,3,7,2,4],[2,7,5,1,3,3,3,3,3,3,3,3,3]],\
[[6,7,12,0,1,3,2,8,5,11,10,9,4],[0,3,3,3,3,3,3,3,3,3,3,3,3]],\
[[10,11,7,1,6,12,4,9,0,2,5,8,3],[1,2,1,7,5,3,3,3,3,3,3,3,3]],\
[[3,1,2,4,11,5,12,7,0,6,8,9,10],[0,3,3,3,3,1,1,1,3,3,3,3,3]],\
[[4,3,8,5,10,0,11,2,7,1,9,12,6],[0,3,3,1,1,3,3,3,1,3,3,1,3]],\
[[10,0,8,1,11,4,12,9,6,5,2,7,3],[1,0,3,3,3,3,1,3,3,3,3,3,3]],\
[[8,12,3,6,0,11,1,10,7,2,9,4,5],[0,1,3,3,3,3,3,3,1,3,3,3,1]],\
[[5,12,10,6,4,0,9,1,7,11,3,8,2],[1,1,1,0,3,3,3,3,3,3,3,3,3]],\
[[0,9,12,8,3,10,6,7,11,5,4,2,1],[0,3,1,3,3,1,3,3,3,3,3,3,3]],\
[[3,4,5,8,7,6,2,10,0,1,11,12,9],[0,3,1,3,1,3,3,3,3,3,3,1,3]],\
[[1,10,8,2,5,0,9,4,3,6,7,11,12],[0,1,3,3,1,3,3,3,3,3,3,3,3]],\
[[12,0,7,3,9,4,1,2,5,8,10,6,11],[1,0,1,3,3,3,3,3,1,3,1,3,3]],\
[[6,7,12,9,8,5,0,4,3,2,1,11,10],[0,3,3,3,3,3,3,3,3,3,3,3,3]],\
[[8,0,10,7,4,3,2,5,1,6,11,9,12],[0,3,1,1,3,3,3,1,3,3,3,3,3]],\
[[9,4,6,10,8,0,5,1,7,3,2,12,11],[2,6,6,6,6,6,1,6,1,6,6,1,6]],\
[[0,9,1,10,5,7,11,8,6,2,3,4,12],[0,3,3,1,1,1,3,3,3,3,3,3,3]],\
[[11,6,9,10,4,2,0,12,8,7,1,5,3],[2,5,3,3,3,3,3,1,3,1,3,1,3]],\
[[2,0,5,1,4,6,12,10,3,11,8,9,7],[0,3,1,3,3,3,3,3,3,3,3,3,1]],\
[[7,6,1,12,8,9,2,10,0,4,3,5,11],[1,0,3,3,3,3,3,3,3,3,3,1,3]],\
[[7,10,12,3,0,4,1,2,9,8,6,11,5],[1,1,1,0,3,3,3,3,3,3,3,3,3]],\
[[12,6,10,9,2,0,8,1,3,11,4,5,7],[1,0,3,3,3,3,3,3,3,3,3,1,1]],\
[[5,3,0,1,10,9,12,7,2,8,4,6,11],[1,0,3,3,1,3,1,1,3,3,3,3,3]],\
[[1,5,2,4,6,12,3,11,7,0,8,9,10],[0,1,3,3,3,3,3,3,3,3,3,3,3]],\
[[11,0,12,6,10,1,8,9,3,2,7,5,4],[2,5,1,3,3,3,3,3,3,3,1,1,3]]]";
}
