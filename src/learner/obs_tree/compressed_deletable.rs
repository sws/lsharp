use std::collections::VecDeque;
use std::fmt::Debug;

use datasize::{data_size, DataSize};
use rustc_hash::FxHashMap;
use serde::{Deserialize, Serialize};
use slotmap::SlotMap;

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;
use crate::util::toolbox;

use super::ObservationTree;

#[derive(
    Copy,
    Clone,
    DataSize,
    Default,
    Debug,
    Eq,
    Hash,
    PartialEq,
    PartialOrd,
    Ord,
    Deserialize,
    Serialize,
)]
pub struct TreeState {
    key: NodeKey,
    offset: u32,
}

impl TreeState {
    #[must_use]
    fn new(key: NodeKey, offset: u32) -> Self {
        Self { key, offset }
    }

    #[must_use]
    fn key(self) -> NodeKey {
        self.key
    }

    #[must_use]
    fn offset(self) -> u32 {
        self.offset
    }
}

/// A `Node` can have two forms: a `Normal` have at least two children,
/// and a `Compressed` Node has at most one child.
#[derive(Debug, Clone, DataSize, derive_more::IsVariant, derive_more::From)]
enum ObsNode {
    /// `Normal` nodes are used when we have two or more children.
    Norm(NormalNode),
    /// `Compressed` nodes are used when we have at most one child.
    Comp(Compressed),
}

impl ObsNode {
    fn parent(&self) -> (InputSymbol, TreeState) {
        match self {
            ObsNode::Norm(node) => node.parent,
            ObsNode::Comp(node) => node.parent,
        }
    }

    fn update_parent(&mut self, parent: (InputSymbol, TreeState)) {
        match self {
            ObsNode::Norm(node) => node.parent = parent,
            ObsNode::Comp(node) => node.parent = parent,
        }
    }
}

#[derive(Debug, Clone, Default, DataSize, Serialize, Deserialize)]
struct NormalNode {
    // The value of the map is the output symbol and the index of the next node.
    data: FxHashMap<InputSymbol, (OutputSymbol, TreeState)>,
    /// Parent state.
    parent: (InputSymbol, TreeState),
}

impl NormalNode {
    /// Add transition to the map of this node.
    fn add_transition(&mut self, i: InputSymbol, o: OutputSymbol, s: TreeState) {
        self.data.insert(i, (o, s));
    }

    #[must_use]
    fn with_parent(parent_input: InputSymbol, parent_state: TreeState) -> Self {
        Self {
            parent: (parent_input, parent_state),
            ..Default::default()
        }
    }
}

#[derive(Debug, Default, Clone, DataSize)]
struct Compressed {
    data: Vec<(InputSymbol, OutputSymbol)>,
    /// Parent state.
    parent: (InputSymbol, TreeState),
    /// Assigning anything else other than a `NormalNode` state will lead to UB!
    ///
    /// The general idea is that as soon as a compressed node "ends", i.e., when the
    /// `data` sequence in the compressed node is finished, we mark the next node with
    /// this field. As you can imagine, this field is only meant to be used when we
    /// have a compressed node which points to a  normal node; in other words, that
    /// there exists a path in the observation tree which then splits.
    next_node: Option<TreeState>,
}

impl Compressed {
    #[must_use]
    fn with_parent(parent_input: InputSymbol, parent_state: TreeState) -> Self {
        Self {
            parent: (parent_input, parent_state),
            ..Default::default()
        }
    }
    #[inline]
    #[must_use]
    fn is_terminal(&self) -> bool {
        self.next_node.is_none()
    }
}

slotmap::new_key_type! {
struct NodeKey; }

impl DataSize for NodeKey {
    const IS_DYNAMIC: bool = false;

    /// u32 + u32
    const STATIC_HEAP_SIZE: usize = 8;

    fn estimate_heap_size(&self) -> usize {
        8
    }
}

#[derive(Debug)]
struct TreeMap<V>(SlotMap<NodeKey, V>);

impl<V> TreeMap<V> {
    fn new() -> Self {
        Self(SlotMap::with_key())
    }
}

#[derive(Debug, DataSize)]
pub struct Tree {
    inner: TreeMap<ObsNode>,
    #[data_size(skip)]
    input_size: usize,
    root_key: NodeKey,
}

impl<V> DataSize for TreeMap<V> {
    const IS_DYNAMIC: bool = true;

    const STATIC_HEAP_SIZE: usize = 3;

    fn estimate_heap_size(&self) -> usize {
        let mut total = 0;
        for x in self.0.iter() {
            total += data_size(&x);
        }
        total
    }
}

use std::ops::{Deref, DerefMut};

impl<V> Deref for TreeMap<V> {
    type Target = SlotMap<NodeKey, V>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<V> DerefMut for TreeMap<V> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl ObservationTree<InputSymbol, OutputSymbol> for Tree {
    type S = TreeState;

    fn insert_observation(
        &mut self,
        start: Option<TreeState>,
        input_seq: &[InputSymbol],
        output_seq: &[OutputSymbol],
    ) -> TreeState {
        let start = start.map(|x| self.get_root_if_default(x));
        //TODO: Add optimisation to check if the curr node is a terminal compressed node.
        // If so, then we can just construct a single node directly and insert the sequence
        // in one go instead of looping over each symbol pair.
        let mut curr = start.unwrap_or_else(|| TreeState::new(self.root_key, 0));
        for (i, o) in Iterator::zip(input_seq.iter(), output_seq.iter()) {
            curr = self.add_transition_get_destination(curr, *i, *o);
        }
        curr
    }

    fn get_access_seq(&self, state: TreeState) -> Vec<InputSymbol> {
        let state = self.get_root_if_default(state);

        self.get_transfer_seq(state, TreeState::new(self.root_key, 0))
    }

    fn get_transfer_seq(&self, to_state: TreeState, from_state: TreeState) -> Vec<InputSymbol> {
        let to_state = self.get_root_if_default(to_state);
        let from_state = self.get_root_if_default(from_state);

        if from_state == to_state {
            return vec![];
        }
        let mut access_seq = VecDeque::new();

        macro_rules! inputs_from_compressed {
            ($start:expr, $end:expr, $data:expr) => {
                $data[($start as usize)..($end as usize)]
                    .iter()
                    .map(|(i, _)| *i)
                    .rev()
            };
        }
        match (self.ref_at(to_state), self.ref_at(from_state)) {
            (ObsNode::Comp(to_node), ObsNode::Norm(_)) => {
                for i in inputs_from_compressed!(0, to_state.offset, &to_node.data) {
                    access_seq.push_front(i);
                }
            }
            (ObsNode::Comp(to_node), ObsNode::Comp(_)) => {
                let same_index = to_state.key == from_state.key;
                // If the indices of the parent and child are the same, we only need to get
                // the inputs stored between the offsets and return.
                // Else we need to get the entire field.
                let start_offset = if same_index { from_state.offset } else { 0 };
                for i in inputs_from_compressed!(start_offset, to_state.offset, &to_node.data) {
                    access_seq.push_front(i);
                }
                if same_index {
                    return access_seq.into();
                }
            }
            _ => {}
        }
        let mut curr_state_key = to_state.key();

        loop {
            if from_state.key() == curr_state_key {
                // If the current_st and the from state are the same, then we check if the offsets match.
                // if from_state.offset() != 0 {
                // we need to get the offset state.
                if let ObsNode::Comp(c_node) = self.ref_at_key(curr_state_key) {
                    let end = c_node.data.len() - 1;
                    for i in inputs_from_compressed!(from_state.offset, end, &c_node.data) {
                        access_seq.push_front(i);
                    }
                }
                break;
            }

            let (i, parent_idx) = self.ref_at_key(curr_state_key).parent();
            access_seq.push_front(i);

            if let ObsNode::Comp(c) = self.ref_at(parent_idx) {
                if parent_idx.key != (from_state.key) {
                    let end = c.data.len() - 1;
                    for i in inputs_from_compressed!(0, end, &c.data) {
                        access_seq.push_front(i);
                    }
                }
            }
            curr_state_key = parent_idx.key;
        }
        access_seq.into()
    }

    fn get_observation(
        &self,
        start: Option<TreeState>,
        input_seq: &[InputSymbol],
    ) -> Option<Vec<OutputSymbol>> {
        let start = start.map(|x| self.get_root_if_default(x));

        let mut s = start.unwrap_or_else(|| TreeState::new(self.root_key, 0));
        let mut out_vec = Vec::with_capacity(input_seq.len());
        for &i in input_seq {
            let (o, d) = self.get_out_succ(s, i)?;
            out_vec.push(o);
            s = d;
        }
        Some(out_vec)
    }

    fn get_out_succ(
        &self,
        src: TreeState,
        input: InputSymbol,
    ) -> Option<(OutputSymbol, TreeState)> {
        let src = self.get_root_if_default(src);

        let node = self.ref_at(src);
        match node {
            ObsNode::Norm(n_node) => {
                return n_node.data.get(&input).copied();
            }
            ObsNode::Comp(c_node) => {
                let src_offset = src.offset();
                // If the offset is equal to the length of the vector,
                // we will have to return None anyway.
                if c_node.data.is_empty() {
                    if let Some(nn) = c_node.next_node {
                        return self.get_out_succ(nn, input);
                    }
                }
                let (c_node_i, c_node_o) = c_node.data.get(src_offset as usize)?;
                if *c_node_i == input {
                    // Now, if the offset is equal to the max offset, the next
                    // node is actually one that is pointed to by the next_node
                    // field of the c_node. If that doesn't exist, we basically
                    // don't _have_ a next node.
                    if src_offset as usize == c_node.data.len() - 1 {
                        if let Some(nxt) = c_node.next_node {
                            return Some((*c_node_o, nxt));
                        }
                    }
                    // Current node is terminal.
                    let dest = TreeState::new(src.key(), src_offset + 1);
                    Some((*c_node_o, dest))
                } else {
                    None
                }
            }
        }
    }

    fn get_succ(&self, src: TreeState, input: &[InputSymbol]) -> Option<TreeState> {
        let src = self.get_root_if_default(src);

        let mut curr = src;
        for i in input {
            curr = self._get_succ(curr, *i)?;
        }
        Some(curr)
        // input.iter().try_fold(src, |s, i| self._get_succ(s, *i))
    }

    fn no_succ_defined(&self, basis: &[TreeState], sort: bool) -> Vec<(TreeState, InputSymbol)> {
        let inputs = toolbox::inputs_iterator(self.input_size);
        let mut ret: Vec<_> =
            itertools::Itertools::cartesian_product(basis.iter().copied(), inputs)
                .filter(|(bs, i)| self._get_succ(*bs, *i).is_none())
                .collect();
        let acc_len = |s| self.get_access_seq(s).len();
        let len_cmp = |a, b| acc_len(a).cmp(&acc_len(b));
        if sort {
            ret.sort_unstable_by(|(a, _), (b, _)| len_cmp(*a, *b));
        }
        ret
    }

    fn size(&self) -> usize {
        self.inner.len()
    }

    fn input_size(&self) -> usize {
        self.input_size
    }

    fn tree_and_hyp_states_apart_sink(
        &self,
        s_t: TreeState,
        s_h: State,
        fsm: &Mealy,
        sink_output: OutputSymbol,
        _depth: usize,
    ) -> bool {
        let mut queue = VecDeque::from([(s_t, s_h)]);

        while let Some((q_t, q_h)) = queue.pop_front() {
            match self.ref_at(q_t) {
                ObsNode::Norm(n_node) => {
                    for (t_i, (t_o, t_d)) in &n_node.data {
                        let (h_d, h_o) = fsm.step_from(q_h, *t_i);
                        if h_o == *t_o {
                            if *t_o == sink_output {
                                continue;
                            }
                            queue.push_back((*t_d, h_d));
                        } else {
                            return true;
                        }
                    }
                }
                // The compressed node handling is a bit more complicated!
                ObsNode::Comp(c_node) => {
                    let tree_transitions = c_node.data.get(q_t.offset as usize..);
                    if let Some(trans) = tree_transitions {
                        // From these transitions, we have to take the difference between the
                        // current depth and the max depth.
                        let mut h_c = q_h;
                        let mut add_dest_to_queue = true;
                        'comp_trans: for (t_i, t_o) in trans {
                            let (h_d, h_o) = fsm.step_from(h_c, *t_i);
                            h_c = h_d;
                            if h_o == *t_o {
                                if *t_o == sink_output {
                                    add_dest_to_queue = false;
                                    break 'comp_trans;
                                }
                            } else {
                                return true;
                            }
                        }
                        if add_dest_to_queue {
                            if let Some(dest) = &c_node.next_node {
                                queue.push_back((*dest, h_c));
                            }
                        }
                    }
                }
            }
        }
        false
    }
}

impl Tree {
    #[must_use]
    pub fn new(input_size: usize) -> Self {
        let root_node = ObsNode::Norm(NormalNode::default());
        let mut inner = TreeMap::new();
        let root_key = inner.insert(root_node.clone());
        Self {
            inner,
            input_size,
            root_key,
        }
    }

    fn _get_succ(&self, src: TreeState, input: InputSymbol) -> Option<TreeState> {
        self.get_out_succ(src, input).map(|x| x.1)
    }

    fn ref_at(&self, st: TreeState) -> &ObsNode {
        self.ref_at_key(st.key)
    }

    fn ref_at_key(&self, key: NodeKey) -> &ObsNode {
        // SAFETY: We're already slow enough, no thank you.
        unsafe { self.inner.get_unchecked(key) }
    }

    fn get_root_if_default(&self, st: TreeState) -> TreeState {
        if st == TreeState::default() {
            TreeState::new(self.root_key, 0)
        } else {
            st
        }
    }

    /// A state is terminal compressed if it is located at a compressed node with no child
    /// and the offset of the state is equal to the length of the data vector in the node (
    /// i.e., we want to insert something at the end of the vector.)
    fn _is_terminal_compressed(&self, src: TreeState) -> bool {
        let node = self.ref_at(src);
        if let ObsNode::Comp(node) = node {
            if node.is_terminal() {
                return src.offset as usize == node.data.len();
            }
        }
        false
    }

    #[allow(clippy::too_many_lines)]
    fn add_transition_get_destination(
        &mut self,
        src: TreeState,
        i: InputSymbol,
        o: OutputSymbol,
    ) -> TreeState {
        if let Some((_, d)) = self.get_out_succ(src, i) {
            return d;
        }
        // We know now that this transition doesn't exist, so we have to add it.
        let node_is_normal = self.ref_at(src).is_norm();
        if node_is_normal {
            self.add_transition_normal_node(src, i, o)
        } else {
            self.add_transition_compressed_node(src, i, o)
        }
    }

    fn add_transition_normal_node(
        &mut self,
        src: TreeState,
        i: InputSymbol,
        o: OutputSymbol,
    ) -> TreeState {
        // If the current node is a normal node, things are very simple: we simply create a compressed node
        // for the destination and assign the transitions. The entry of the node and transition assignment are
        // a bit weird, but that is unavoidable due to Rust's borrowing rules.
        #[allow(clippy::cast_possible_truncation)]
        let dest_node = Compressed::with_parent(i, src);
        let dest_node_key = self.inner.insert(dest_node.into());
        let dest_state = TreeState::new(dest_node_key, 0);
        let Some(ObsNode::Norm(n_node)) = &mut self.inner.get_mut(src.key) else {
            panic!("Safe");
        };
        n_node.add_transition(i, o, dest_state);
        dest_state
    }

    fn add_transition_compressed_node(
        &mut self,
        src: TreeState,
        i: InputSymbol,
        o: OutputSymbol,
    ) -> TreeState {
        let Some(ObsNode::Comp(c_node)) = &mut self.inner.get_mut(src.key) else {
            panic!("Safe");
        };

        let offset = src.offset;
        let c_data = c_node.data.clone();
        let c_next_node = c_node.next_node;
        #[allow(clippy::cast_possible_truncation)]
        let data_len = c_node.data.len() as u32;

        assert!(
            offset <= data_len,
            "Offset can never be *greater* than the data length!"
        );

        let is_terminal = c_node.is_terminal();
        let c_node_idx = src.key;
        if offset == data_len {
            assert!(
                is_terminal,
                "We should not be adding transitions to the `next-next` state!"
            );
            c_node.data.push((i, o));
            return TreeState::new(c_node_idx, offset + 1);
        }

        // OFFSET < DATA_LEN
        // We are going to be branching somewhere in the defined transitions.
        if offset == 0 && data_len == 1 {
            self.split_singleton(c_node_idx, (i, o), &c_data)
        } else if offset == 0 {
            self.split_at_beginning(c_node_idx, (i, o), &c_data)
        } else if offset == data_len - 1 {
            self.split_at_last_symbol(&c_data, c_next_node, (i, o), src)
        } else {
            self.split_in_middle(src, (i, o))
        }
    }

    #[allow(clippy::cast_possible_truncation)]
    fn split_in_middle(
        &mut self,
        src: TreeState,
        (i, o): (InputSymbol, OutputSymbol),
    ) -> TreeState {
        //  ___________
        // | [c_data]   |
        // | (I , O)    | <--- These will be kept here.
        // | (i',o')    | <--- "Add" (i,o) here, by moving both transitions.
        // | (I , O)    | <--- These will be moved to another compressed node.
        // |___________ |
        // | next_node  | }
        // |___________ | }

        let TreeState {
            key: c_node_idx,
            offset,
        } = src;

        // Clear out the excess transitions from the current node.
        let ObsNode::Comp(curr_node) = (unsafe { self.inner.get_unchecked_mut(c_node_idx) }) else {
            unreachable!("Safe.")
        };
        let curr_node_next_node = curr_node.next_node;
        let ((i_p, o_p), transitions_to_donate) = {
            let mut iter = curr_node.data.drain(offset as usize..);
            let prime_transition = iter.next().expect("Safe");
            let rest: Vec<_> = iter.collect();
            (prime_transition, rest)
        };

        // Construct the donatee compressed node.
        let new_comp_node = Compressed {
            data: transitions_to_donate,
            next_node: curr_node_next_node,
            ..Default::default()
        };
        let new_comp_node_key = self.inner.insert(new_comp_node.into());
        // Don't forget that the parent of curr_node_next_node needs to be fixed.
        if let Some(curr_node_next_node) = curr_node_next_node {
            let (input, offset) = {
                let ObsNode::Comp(new_comp_node) =
                    (unsafe { self.inner.get_unchecked_mut(new_comp_node_key) })
                else {
                    unreachable!("Safe.")
                };
                let x = new_comp_node.data.last().expect("Safe");
                (x.0, new_comp_node.data.len() - 1)
            };
            let ObsNode::Norm(curr_node_next_node) =
                (unsafe { self.inner.get_unchecked_mut(curr_node_next_node.key) })
            else {
                unreachable!("Safe.")
            };
            curr_node_next_node.parent = (input, TreeState::new(new_comp_node_key, offset as u32));
        }

        // Construct new destination compressed node for (i,o) transition.
        let dest_comp_node = Compressed::default();
        let dest_comp_node_key = self.inner.insert(dest_comp_node.into());

        let mut normal_node = NormalNode::default();
        normal_node.add_transition(i, o, TreeState::new(dest_comp_node_key, 0));
        normal_node.add_transition(i_p, o_p, TreeState::new(new_comp_node_key, 0));
        {
            let ObsNode::Comp(curr_node) = (unsafe { self.inner.get_unchecked_mut(c_node_idx) })
            else {
                unreachable!("Safe.")
            };
            let pos = curr_node.data.len() - 1; // last position
            let &(input, _) = curr_node.data.last().expect("Safe");
            normal_node.parent = (input, TreeState::new(c_node_idx, pos as u32));
        }
        let normal_node_key = self.inner.insert(normal_node.into());

        {
            let ObsNode::Comp(curr_node) = (unsafe { self.inner.get_unchecked_mut(c_node_idx) })
            else {
                unreachable!("Safe.")
            };
            curr_node.next_node = Some(TreeState::new(normal_node_key, 0));
        }

        // Handle parents.

        // Dest comp node.
        let ObsNode::Comp(dest_comp_node) =
            (unsafe { self.inner.get_unchecked_mut(dest_comp_node_key) })
        else {
            unreachable!("Safe.")
        };
        dest_comp_node.parent = (i, TreeState::new(normal_node_key, 0));

        // Donatee comp node.
        let ObsNode::Comp(new_comp_node) =
            (unsafe { self.inner.get_unchecked_mut(new_comp_node_key) })
        else {
            unreachable!("Safe.")
        };
        new_comp_node.parent = (i_p, TreeState::new(normal_node_key, 0));

        TreeState::new(dest_comp_node_key, 0)
    }

    #[allow(clippy::cast_possible_truncation)]
    fn split_at_last_symbol(
        &mut self,
        c_data: &[(InputSymbol, OutputSymbol)],
        c_next_node: Option<TreeState>,
        new_trans: (InputSymbol, OutputSymbol),
        src: TreeState,
    ) -> TreeState {
        //  ___________
        // | [c_data]   |
        // | (I , O)    | }  <----  These will be kept here.
        // | (I_a , O_a)| }  <---- Parent info of the new normal node.
        // | (i',o')    |    <--- Loc 0 : "Add" (i,o) here, by moving both transitions.
        // |___________ |
        // | next_node  | }  <--- next_node, if it exists, will become the destination
        // |___________ | }       for (i',o') in the new node; else we make a new comp node
        //                        for the transition.

        // Aka "old_trans".
        let &[(i_a, _o_a), (i_p, o_p)] = c_data.get(c_data.len() - 2..).expect("Safe") else {
            unreachable!("Safe")
        };
        let (i, o) = new_trans;

        // Add the new (i,o) destination first, we'll update its parent later.
        let dest_state = {
            let dest_node = Compressed::default();
            let dest_key = self.inner.insert(dest_node.into());
            TreeState::new(dest_key, 0)
        };

        // First, we construct a normal node with the current node as the parent.
        let mut normal_node = {
            // The parent input is I_a, the *second last* input.
            let parent_offset = c_data.len() - 2;
            NormalNode::with_parent(i_a, TreeState::new(src.key, parent_offset as u32))
        };
        // Add transition for the new destination.
        normal_node.data.insert(i, (o, dest_state));

        let dest_for_old_trans = {
            // If the current compressed node has a child, add the transition for it in the
            // normal_node.
            if let Some(old_trans_child) = c_next_node {
                old_trans_child
            } else {
                let old_trans_child = Compressed::default();
                let old_trans_key = self.inner.insert(old_trans_child.into());
                TreeState::new(old_trans_key, 0)
            }
        };
        normal_node.data.insert(i_p, (o_p, dest_for_old_trans));

        // Insert the normal node and update the two destination nodes'
        // parent fields.
        let normal_node_key = self.inner.insert(normal_node.into());
        let normal_node_state = TreeState::new(normal_node_key, 0);
        {
            let child_node = unsafe { self.inner.get_unchecked_mut(dest_for_old_trans.key) };
            child_node.update_parent((i_p, normal_node_state));

            let child_node = unsafe { self.inner.get_unchecked_mut(dest_state.key) };
            child_node.update_parent((i, normal_node_state));
        }

        // From the current node, we remove the last transition and update its next_node
        // to the normal_node.
        let ObsNode::Comp(curr_node) = (unsafe { self.inner.get_unchecked_mut(src.key) }) else {
            unreachable!("Safe.")
        };
        curr_node.data.remove(curr_node.data.len() - 1);
        curr_node.next_node = Some(normal_node_state);

        dest_state
    }

    /// The compressed node has just one transition. We add another transition
    /// at the same location by converting the compressed node to a normal node.
    fn split_singleton(
        &mut self,
        c_node_idx: NodeKey,
        new_trans: (InputSymbol, OutputSymbol),
        c_data: &[(InputSymbol, OutputSymbol)],
    ) -> TreeState {
        //  ___________
        // | [c_data]   |
        // | (i', o')   | }  <--- Loc 0 : "Add" (i,o) here by moving both transitions
        // |___________ | }        to a new normal node.
        // | next_node  | }  <--- If there is a next_node, then the destination for the
        // |___________ | }        (i',o') transition will go there, else we construct
        //                         an empty destination node for it.
        let (i_p, o_p) = c_data.last().copied().expect("Safe");
        let (i, o) = new_trans;
        // The new destination for the (i,o) transition.
        let new_dest_node = Compressed::default(); //with_parent(i, TreeState::new(c_node_idx, 0));
        let new_dest_key = self.inner.insert(new_dest_node.into());

        // The new normal node.
        let c_node_parent = self.inner.get(c_node_idx).expect("Safe").parent();
        let mut new_curr_node = NormalNode::with_parent(c_node_parent.0, c_node_parent.1);
        new_curr_node.add_transition(i, o, TreeState::new(new_dest_key, 0));
        new_curr_node.add_transition(i_p, o_p, TreeState::new(c_node_idx, 0));
        let new_curr_node_key = self.inner.insert(new_curr_node.into());

        // Set parent of the dest node to the new_curr_node.
        let Some(ObsNode::Comp(new_dest_node)) = self.inner.get_mut(new_dest_key) else {
            unreachable!("Safe.")
        };
        new_dest_node.parent = (i, TreeState::new(new_curr_node_key, 0));

        // The parent of the current c_node must update its child to be the new_c_node.
        // We have a guarantee that the parent of the c_node is a normal node, since
        // two compressed nodes cannot follow each other.
        {
            let ObsNode::Norm(c_node_parent_node) =
                (unsafe { self.inner.get_unchecked_mut(c_node_parent.1.key) })
            else {
                panic!("Two compressed nodes in a row not possible.");
            };
            let (_, child) = c_node_parent_node
                .data
                .get_mut(&c_node_parent.0)
                .expect("Two compressed nodes not possible.");
            *child = TreeState::new(new_curr_node_key, 0);
        }

        // new_curr_node now has an (i',o') transition to c_node. However,
        // c_node is supposed to be emptied. Thus, if c_node has a child,
        // we will update the transition to point to the child and delete c_node.
        // Else, we will simply empty c_node and we're done.

        //In either case, we first  empty c_node.
        let ObsNode::Comp(c_node) = (unsafe { self.inner.get_unchecked_mut(c_node_idx) }) else {
            unreachable!("Safe.")
        };
        c_node.data.clear();
        c_node.parent = (i_p, TreeState::new(new_curr_node_key, 0));

        let c_node_child = c_node.next_node;
        if let Some(child) = c_node_child {
            let ObsNode::Norm(new_curr_node) =
                (unsafe { self.inner.get_unchecked_mut(new_curr_node_key) })
            else {
                unreachable!("Safe.")
            };
            let (_, dest) = new_curr_node.data.get_mut(&i_p).expect("Safe");
            *dest = child;

            // We also need to update the parent field of the child.
            let child_node = unsafe { self.inner.get_unchecked_mut(child.key) };
            let parent = (i_p, TreeState::new(new_curr_node_key, 0));
            match child_node {
                ObsNode::Norm(child_node) => child_node.parent = parent,
                ObsNode::Comp(_) => unreachable!(
                    "A compressed node cannot have another compressed node as a child."
                ),
            }

            // Delete the original compressed node now, it is useless.
            self.inner.remove(c_node_idx);
        }

        TreeState::new(new_dest_key, 0)
    }

    /// Add a symbol at position 0 while the length of the
    /// compressed data vector is more than 1.
    fn split_at_beginning(
        &mut self,
        c_node_idx: NodeKey,
        new_trans: (InputSymbol, OutputSymbol),
        c_data: &[(InputSymbol, OutputSymbol)],
    ) -> TreeState {
        //  ___________
        // | [c_data]   |
        // | (i', o')   | <--- Loc 0 : "Add" (i,o) here, by moving both transitions.
        // | (I , O)    | }  <--- These will be kept here. We will instead remove
        // | (I , O)    | }       (i',o'): that transition is moved to the new normal
        // |___________ |         node.
        // | next_node  | }
        // |___________ | }
        let (i_p, o_p) = c_data.first().copied().expect("Safe");
        let (i, o) = new_trans;
        // The new destination for the (i,o) transition.
        let new_dest_node = Compressed::with_parent(i, TreeState::new(c_node_idx, 0));
        let new_dest_key = self.inner.insert(new_dest_node.into());

        // The new normal node.
        let curr_node_parent = self.inner.get(c_node_idx).expect("Safe").parent();
        let mut new_c_node = NormalNode::with_parent(curr_node_parent.0, curr_node_parent.1);
        new_c_node.add_transition(i, o, TreeState::new(new_dest_key, 0));
        new_c_node.add_transition(i_p, o_p, TreeState::new(c_node_idx, 0));
        let new_c_node_key = self.inner.insert(new_c_node.into());

        // The parent of the current c_node must update its child to be the new_c_node.
        // We have a guarantee that the parent of the c_node is a normal node, since
        // two compressed nodes cannot follow each other.
        {
            let ObsNode::Norm(c_node_parent_node) =
                (unsafe { self.inner.get_unchecked_mut(curr_node_parent.1.key) })
            else {
                panic!("Two compressed nodes in a row not possible.");
            };
            let (_o, child) = c_node_parent_node
                .data
                .get_mut(&curr_node_parent.0)
                .expect("Two compressed nodes not possible.");
            *child = TreeState::new(new_c_node_key, 0);
        }

        // Now we just need to remove the first transition from the current node.
        let ObsNode::Comp(c_node) = (unsafe { self.inner.get_unchecked_mut(c_node_idx) }) else {
            unreachable!("Safe.")
        };
        c_node.data.remove(0);

        // Fix parents.
        c_node.parent = (i_p, TreeState::new(new_c_node_key, 0));
        let ObsNode::Comp(dest_node) = (unsafe { self.inner.get_unchecked_mut(new_dest_key) })
        else {
            unreachable!("Safe.")
        };
        dest_node.parent = (i, TreeState::new(new_c_node_key, 0));
        TreeState::new(new_dest_key, 0)
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use crate::{
        definitions::{
            mealy::{InputSymbol, Mealy, OutputSymbol},
            FiniteStateMachine,
        },
        learner::obs_tree::{compressed_deletable::TreeState, ObservationTree},
        util::parsers::machine::read_mealy_from_file,
    };

    use fnv::FnvHashSet;
    use itertools::Itertools;
    use proptest::{collection, proptest, strategy::Strategy};
    use rand::{rngs::StdRng, seq::SliceRandom, Rng, SeedableRng};
    use rand_distr::Uniform;
    use rstest::rstest;

    use super::Tree;

    type OTree = Tree;

    fn load_basic_fsm(name: &str) -> Mealy {
        let file_name = Path::new(name);
        read_mealy_from_file(file_name.to_str().expect("Safe")).0
    }

    fn gen_traces() -> impl Strategy<Value = (Vec<InputSymbol>, Vec<OutputSymbol>)> {
        let num_inputs = 13usize;
        let fsm = load_basic_fsm("tests/src_models/BitVise.dot");
        #[allow(clippy::cast_possible_truncation)]
        let input_symbol_strat = (0..num_inputs)
            .prop_map(|x| x as u16)
            .prop_map(InputSymbol::from);
        let input_seq_strat = collection::vec(input_symbol_strat, 1..100);
        input_seq_strat
            .prop_map(move |is| (fsm.trace(&is).1.to_vec(), is))
            .prop_map(|(os, is)| (is, os))
    }

    fn gen_vec_of_traces<Word>(
        max_size_set: usize,
        x: Word,
    ) -> impl Strategy<Value = Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)>>
    where
        Word: Strategy<Value = (Vec<InputSymbol>, Vec<OutputSymbol>)>,
    {
        collection::vec(x, 1..max_size_set)
    }

    /// FIXED.
    #[rstest]
    #[case("[[[2,2,10,0],[0,0,0,0]],[[2,2,10,1],[0,0,0,0]]]")]
    #[case("[[[10,12,0,0],[1,1,0,3]],[[10,12,0,1],[1,1,0,3]]]")]
    fn regression_xfer_seq_maintained(#[case] traces: &str) {
        let seqs: Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)> =
            serde_json::from_str(traces).expect("Safe");

        let mut ret = OTree::new(13);
        for (idx, (is, os)) in seqs.iter().enumerate() {
            ret.insert_observation(None, is, os);
            assert!(ret.get_succ(TreeState::default(), is).is_some());
            for (in_idx, (iis, _)) in seqs[0..idx].iter().enumerate() {
                let ds = ret.get_succ(TreeState::default(), iis).unwrap();
                let rx_acc = ret.get_access_seq(ds);
                assert_eq!(
                    iis, &rx_acc,
                    "Failed at idx {idx} and sub-idx {in_idx}, \n after inserting {is:?}"
                );
                for i in 0..iis.len() {
                    let (pref, suff) = iis.split_at(i);
                    let pref_dest = ret.get_succ(TreeState::default(), pref).unwrap();
                    let xfer_seq = ret.get_transfer_seq(ds, pref_dest);
                    assert_eq!(suff, &xfer_seq);
                }
            }
        }
    }

    proptest! {

        #[test]
        fn xfer_seq_maintained(seqs in gen_vec_of_traces(100,gen_traces())){
            let mut ret = OTree::new(13);
            for (idx, (is, os)) in seqs.iter().enumerate() {
                ret.insert_observation(None, is, os);
                assert!(ret.get_succ(TreeState::default(), is).is_some());
                for (in_idx, (iis, _)) in seqs[0..idx].iter().enumerate() {
                    let ds = ret.get_succ(TreeState::default(), iis).unwrap();
                    let rx_acc = ret.get_access_seq(ds);
                    assert_eq!(
                        iis, &rx_acc,
                        "Failed at idx {idx} and sub-idx {in_idx}, \n after inserting {is:?}"
                    );
                    for i in 0..iis.len() {
                        let (pref, suff) = iis.split_at(i);
                        let pref_dest = ret.get_succ(TreeState::default(), pref).unwrap();
                        let xfer_seq = ret.get_transfer_seq(ds, pref_dest);
                        assert_eq!(suff, &xfer_seq);
                    }
                }
            }
        }

        #[test]
        fn access_seq_maintained(seqs in gen_vec_of_traces(500,gen_traces())){
              let mut ret = OTree::new(13);
              for (idx, (is, os)) in seqs.iter().enumerate() {
                ret.insert_observation(None, is, os);
                assert!(ret.get_succ(TreeState::default(), is).is_some());
                for (in_idx, (iis, _)) in seqs[0..idx].iter().enumerate() {
                 let ds = ret.get_succ(TreeState::default(), iis).unwrap();
                let rx_acc = ret.get_access_seq(ds);
                assert_eq!(
                    iis, &rx_acc,
                    "Failed at idx {idx} and sub-idx {in_idx}, \n after inserting {is:?}"
                );
                }
            }
            for (idx, (is, _)) in seqs.iter().enumerate() {
                if let Some(dest) = ret.get_succ(TreeState::default(), is) {
                    let acc_seq = ret.get_access_seq(dest);
                    assert_eq!(is, &acc_seq);
                } else {
                    // println!("Size of tree: {}", ret.size());
                    panic!("Seq number {idx} : {is:?} is not in tree?!");
                }
            }
        }

    }

    /// Given a path to an FSM and construct `num` words over the FSM.
    fn vec_of_observations(name: &str, num: usize) -> Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)> {
        let fsm = load_basic_fsm(name);
        let mut ret: FnvHashSet<(Vec<InputSymbol>, Vec<OutputSymbol>)> = FnvHashSet::default();
        let inputs = fsm.input_alphabet();
        let mut rng = StdRng::seed_from_u64(num as u64);
        let len_range: Uniform<usize> = Uniform::new_inclusive(1, 500);
        let len_seqs_vec = (&mut rng).sample_iter(len_range).take(num).collect_vec();
        for len in len_seqs_vec {
            // Generate a sequence of inputs and outputs with len.
            let in_seq = inputs.choose_multiple(&mut rng, len).copied().collect_vec();
            let out_seq = fsm.trace(&in_seq).1.to_vec();
            ret.insert((in_seq, out_seq));
        }
        ret.into_iter().collect()
    }

    #[rstest]
    #[case::regression_r3(REGRESSION_R3)] // FIXED.
    #[case::regression_r2(REGRESSION_R2)] // FIXED.
    #[case::regression_r1(REGRESSION_R1)] // FIXED.
    #[case::split_comp_pos0_len1(SPLIT_AT_INIT_COMP_NODE_LEN_1)] // FIXED.
    #[case::simple_insert(SIMPLE_INSERT)] // FIXED.
    #[case::split_normal_initial(SPLIT_NORMAL_INITIAL)] // FIXED.
    #[case(REGRESSION_TEST_1_DATA_1)] // FIXED.
    #[case(REGRESSION_TEST_1_DATA_2)] // FIXED.
    fn regression_tests(#[case] traces: &str) {
        let seqs: Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)> =
            serde_json::from_str(traces).expect("Safe");
        let mut ret = OTree::new(5);
        for (idx, (is, os)) in seqs.iter().enumerate() {
            ret.insert_observation(None, is, os);
            assert!(ret.get_succ(TreeState::default(), is).is_some());
            for (x, (iis, oos)) in seqs.iter().enumerate().take(idx + 1) {
                //  0..=idx {
                let dest = ret.insert_observation(None, iis, oos);
                let acc_seq = ret.get_access_seq(dest);
                assert_eq!(iis, &acc_seq, "Idx: {idx}, inner: {x}");
            }
        }
        for (is, _) in &seqs {
            let dest = ret.get_succ(TreeState::default(), is).unwrap();
            let acc_seq = ret.get_access_seq(dest);
            assert_eq!(is, &acc_seq);
        }
    }

    /// FIXED.    
    #[test]
    fn reg_acc_seq_maintained_4() {
        let xyz = vec![vec![0], vec![11, 4, 0, 0], vec![11, 4, 1]];
        let xyz_out = vec![vec![0], vec![2, 6, 6, 6], vec![2, 6, 6]];
        let xyz = xyz
            .into_iter()
            .map(|is: Vec<u16>| is.into_iter().map(InputSymbol::from).collect_vec())
            .collect_vec();
        let xyz_out = xyz_out
            .into_iter()
            .map(|os| os.into_iter().map(OutputSymbol::from).collect_vec())
            .collect_vec();
        let seqs = xyz.into_iter().zip(xyz_out.into_iter()).collect_vec();
        let mut ret = OTree::new(13);

        impl_add_access_seqs(&seqs, &mut ret);
        impl_check_access_seqs(&seqs, &mut ret);
    }

    /// FIXED.    
    #[test]
    fn reg_acc_seq_maintained_3() {
        let xyz = vec![vec![12, 1, 0, 7, 0], vec![12, 1, 0, 7, 1], vec![12, 1, 1]];
        let xyz_out = vec![vec![1, 0, 3, 1, 3], vec![1, 0, 3, 1, 3], vec![1, 0, 3]];
        let xyz = xyz
            .into_iter()
            .map(|is: Vec<u16>| is.into_iter().map(InputSymbol::from).collect_vec())
            .collect_vec();
        let xyz_out = xyz_out
            .into_iter()
            .map(|os| os.into_iter().map(OutputSymbol::from).collect_vec())
            .collect_vec();
        let seqs = xyz.into_iter().zip(xyz_out.into_iter()).collect_vec();
        let mut ret = OTree::new(13);
        impl_add_access_seqs(&seqs, &mut ret);
        impl_check_access_seqs(&seqs, &mut ret);
    }

    fn impl_add_access_seqs(seqs: &[(Vec<InputSymbol>, Vec<OutputSymbol>)], ret: &mut OTree) {
        for (idx, (is, os)) in seqs.iter().enumerate() {
            ret.insert_observation(None, is, os);
            assert!(ret.get_succ(TreeState::default(), is).is_some());
            for (in_idx, (iis, _)) in seqs[0..idx].iter().enumerate() {
                let ds = ret.get_succ(TreeState::default(), iis).unwrap();
                let rx_acc = ret.get_access_seq(ds);
                assert_eq!(
                    iis, &rx_acc,
                    "Failed at idx {idx} and sub-idx {in_idx}, \n after inserting {is:?}"
                );
            }
        }
    }

    fn impl_check_access_seqs(seqs: &[(Vec<InputSymbol>, Vec<OutputSymbol>)], ret: &mut OTree) {
        for (idx, (is, _)) in seqs.iter().enumerate() {
            if let Some(dest) = ret.get_succ(TreeState::default(), is) {
                let acc_seq = ret.get_access_seq(dest);
                assert_eq!(is, &acc_seq);
            } else {
                panic!("Seq number {idx} : {is:?} is not in tree?!");
            }
        }
    }

    /// FIXED.
    #[test]
    fn reg_acc_seq_maintained_2() {
        let xyz = vec![vec![7, 2, 0, 0], vec![7, 2, 0, 1], vec![7, 2, 1]];
        let xyz_out = vec![vec![1, 0, 3, 3], vec![1, 0, 3, 3], vec![1, 0, 3]];
        let xyz = xyz
            .into_iter()
            .map(|is: Vec<u16>| is.into_iter().map(InputSymbol::from).collect_vec())
            .collect_vec();
        let xyz_out = xyz_out
            .into_iter()
            .map(|os| os.into_iter().map(OutputSymbol::from).collect_vec())
            .collect_vec();
        let seqs = xyz.into_iter().zip(xyz_out.into_iter()).collect_vec();
        let mut ret = OTree::new(13);
        impl_add_access_seqs(&seqs, &mut ret);
        impl_check_access_seqs(&seqs, &mut ret);
    }

    /// FIXED.
    #[test]
    fn reg_acc_seq_maintained_1() {
        let xyz = vec![vec![8, 10, 0], vec![8, 10, 1], vec![8, 0], vec![8, 10, 2]];
        let xyz_out = vec![vec![0, 1, 3], vec![0, 1, 3], vec![0, 3], vec![0, 1, 3]];
        let xyz = xyz
            .into_iter()
            .map(|is: Vec<u16>| is.into_iter().map(InputSymbol::from).collect_vec())
            .collect_vec();
        let xyz_out = xyz_out
            .into_iter()
            .map(|os| os.into_iter().map(OutputSymbol::from).collect_vec())
            .collect_vec();
        let seqs = xyz.into_iter().zip(xyz_out.into_iter()).collect_vec();

        let mut ret = OTree::new(13);
        for (idx, (is, os)) in seqs.iter().enumerate() {
            ret.insert_observation(None, is, os);
            assert!(ret.get_succ(TreeState::default(), is).is_some());
            for (in_idx, (iis, _)) in seqs[0..idx].iter().enumerate() {
                assert!(
                    ret.get_succ(TreeState::default(), iis).is_some(),
                    "Seq missing at index: {idx}, sub-idx: {in_idx}"
                );
            }
        }
        impl_check_access_seqs(&seqs, &mut ret);
    }

    /// FIXED.
    #[test]
    fn regression_test_3() {
        let seqs: Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)> =
            serde_json::from_str(REGRESSION_TEST_3_DATA).expect("Safe");
        let mut ret = OTree::new(13);
        for (is, os) in &seqs {
            ret.insert_observation(None, is, os);
        }
    }

    /// FIXED.    
    #[test]
    fn regression_test_2() {
        let name = "tests/src_models/BitVise.dot";
        let num = 100;
        let num_inputs = load_basic_fsm(name).input_alphabet().len();
        let seqs = vec_of_observations(name, num);
        let mut ret = OTree::new(num_inputs);
        for (is, os) in &seqs {
            ret.insert_observation(None, is, os);
            assert!(ret.get_succ(TreeState::default(), is).is_some());
        }
    }

    const SIMPLE_INSERT: &str = "[[[1,1], [0,0]]]";
    const SPLIT_NORMAL_INITIAL: &str = "[[[1,1], [0,0]],[[2,1], [0,0]]]";
    const SPLIT_AT_INIT_COMP_NODE_LEN_1: &str = "[[[1,1], [0,0]],[[1,0], [0,0]]]";
    const REGRESSION_R1: &str = "[[[6,0,0], [0,0,0]],[[6,1], [0,0]]]";
    const REGRESSION_R2: &str = "[[[6,0,0], [0,0,0]],[[6,0,1], [0,0,0]]]";
    const REGRESSION_R3: &str = "[[[11,6,0,0], [0,0,0,0]],[[11,6,1], [0,0,0]]]";
    const REGRESSION_TEST_1_DATA_1: &str = "[[[3,2,1,4,0],[1,1,0,1,0]],\
[[4,2,1,3],[1,1,0,1]],\
[[0,3,4,1,2],[0,1,1,0,0]],\
[[2,4,1,3,0],[1,1,0,1,0]],\
[[3,4,1,0,2],[1,1,0,0,0]]]";

    const REGRESSION_TEST_1_DATA_2: &str = "[[[4,3,2,1,0],[1,1,1,0,0]],\
[[4,1,2,3,0],[1,0,0,1,0]],\
[[0,1,3,2,4],[0,0,1,0,1]],\
[[4,2,3,0,1],[1,1,1,0,0]],\
[[4,1,2,0,3],[1,0,0,0,1]],\
[[1,3,4,2,0],[0,1,1,0,0]],\
[[2,1,4,3,0],[1,0,1,1,0]]]";

    const REGRESSION_TEST_3_DATA: &str = "[[[1,4,3,10,8,2,12,0],[0,3,3,1,3,3,1,3]],\
[[12,5,1,11,2,4,6,8,7,3,10,9,0],[1,1,0,3,3,3,3,3,3,3,3,3,3]],\
[[8,4,9,2,12,0,7,5,10,6,11,3,1],[0,3,3,3,1,3,1,1,1,3,3,3,3]],\
[[1,4,0,5,11,12,3,2,6,9,8,10,7],[0,3,3,1,3,1,3,3,3,3,3,3,1]],\
[[1,8,11,4,9,3,5,0,6,12,7,2,10],[0,3,3,3,3,3,1,3,3,3,3,3,3]],\
[[3,6,4,0,1,5,7,8,10,9,12,2,11],[0,3,3,3,3,3,3,3,3,3,1,3,3]],\
[[9,3,6,8,12,0,5,2,11,4,10,1,7],[2,5,3,3,3,3,3,3,3,3,3,3,1]],\
[[11,1,0,5,9,4,7,3,6,2,10,12,8],[2,7,5,1,3,3,1,3,3,3,3,1,3]],\
[[8,0,6,10,7,1,11,2,4,3,5,9,12],[0,3,3,3,1,3,3,3,3,3,1,3,1]],\
[[7,11,5,12,2,3,1,10,8,0,9,4,6],[1,2,1,1,5,3,3,1,3,3,3,3,3]],\
[[3,9,8,4,10,7,0,12,5,6,1,11],[0,3,3,3,1,1,3,1,1,3,3,3]],\
[[10,0,6,2,4,5,3,11,8,9,7,12,1],[1,0,3,3,3,3,3,3,3,3,3,3,3]],\
[[4,10,2,9,7,1,5,6,3,8,0,12,11],[0,1,3,3,1,3,1,3,3,3,3,3,3]],\
[[1,3,6,11,7,9,12,8,5,4,0,10,2],[0,3,3,3,3,3,3,3,3,3,3,3,3]],\
[[6,9,3,7,5,0,10,8,4,1,11,2,12],[0,3,3,3,3,3,3,3,3,3,3,3,1]],\
[[12,5,6,7,11,10,0,3,1,4,8,9,2],[1,1,0,3,3,3,3,3,3,3,3,3,3]],\
[[4,1,5,11,9,2,10,6,12,3,0,7,8],[0,3,1,3,3,3,1,3,3,3,3,3,3]],\
[[1,4,12,8,7,10,0,9,6,2,5,3,11],[0,3,1,3,1,1,3,3,3,3,3,3,3]],\
[[5,7,3,0,9,12,2,8],[1,1,0,3,3,1,3,3]],\
[[11,1,0,10,6,8,5,12,9,3,7,2,4],[2,7,5,1,3,3,3,3,3,3,3,3,3]],\
[[6,7,12,0,1,3,2,8,5,11,10,9,4],[0,3,3,3,3,3,3,3,3,3,3,3,3]],\
[[10,11,7,1,6,12,4,9,0,2,5,8,3],[1,2,1,7,5,3,3,3,3,3,3,3,3]],\
[[3,1,2,4,11,5,12,7,0,6,8,9,10],[0,3,3,3,3,1,1,1,3,3,3,3,3]],\
[[4,3,8,5,10,0,11,2,7,1,9,12,6],[0,3,3,1,1,3,3,3,1,3,3,1,3]],\
[[10,0,8,1,11,4,12,9,6,5,2,7,3],[1,0,3,3,3,3,1,3,3,3,3,3,3]],\
[[8,12,3,6,0,11,1,10,7,2,9,4,5],[0,1,3,3,3,3,3,3,1,3,3,3,1]],\
[[5,12,10,6,4,0,9,1,7,11,3,8,2],[1,1,1,0,3,3,3,3,3,3,3,3,3]],\
[[0,9,12,8,3,10,6,7,11,5,4,2,1],[0,3,1,3,3,1,3,3,3,3,3,3,3]],\
[[3,4,5,8,7,6,2,10,0,1,11,12,9],[0,3,1,3,1,3,3,3,3,3,3,1,3]],\
[[1,10,8,2,5,0,9,4,3,6,7,11,12],[0,1,3,3,1,3,3,3,3,3,3,3,3]],\
[[12,0,7,3,9,4,1,2,5,8,10,6,11],[1,0,1,3,3,3,3,3,1,3,1,3,3]],\
[[6,7,12,9,8,5,0,4,3,2,1,11,10],[0,3,3,3,3,3,3,3,3,3,3,3,3]],\
[[8,0,10,7,4,3,2,5,1,6,11,9,12],[0,3,1,1,3,3,3,1,3,3,3,3,3]],\
[[9,4,6,10,8,0,5,1,7,3,2,12,11],[2,6,6,6,6,6,1,6,1,6,6,1,6]],\
[[0,9,1,10,5,7,11,8,6,2,3,4,12],[0,3,3,1,1,1,3,3,3,3,3,3,3]],\
[[11,6,9,10,4,2,0,12,8,7,1,5,3],[2,5,3,3,3,3,3,1,3,1,3,1,3]],\
[[2,0,5,1,4,6,12,10,3,11,8,9,7],[0,3,1,3,3,3,3,3,3,3,3,3,1]],\
[[7,6,1,12,8,9,2,10,0,4,3,5,11],[1,0,3,3,3,3,3,3,3,3,3,1,3]],\
[[7,10,12,3,0,4,1,2,9,8,6,11,5],[1,1,1,0,3,3,3,3,3,3,3,3,3]],\
[[12,6,10,9,2,0,8,1,3,11,4,5,7],[1,0,3,3,3,3,3,3,3,3,3,1,1]],\
[[5,3,0,1,10,9,12,7,2,8,4,6,11],[1,0,3,3,1,3,1,1,3,3,3,3,3]],\
[[1,5,2,4,6,12,3,11,7,0,8,9,10],[0,1,3,3,3,3,3,3,3,3,3,3,3]],\
[[11,0,12,6,10,1,8,9,3,2,7,5,4],[2,5,1,3,3,3,3,3,3,3,1,1,3]]]";
}
