use std::collections::HashMap;
use std::hash::BuildHasher;

use datasize::DataSize;

use crate::definitions::mealy::{InputSymbol, OutputSymbol, State};
use crate::learner::apartness::tree_and_hyp_states_apart_sunk_bounded;
use crate::util::data_structs::arena_tree::ArenaTree;
use crate::util::toolbox;

use super::ObservationTree;

#[allow(clippy::module_name_repetitions)]
/// Type-alias for an uncompressed array-based observation tree.
pub type ArrayObsTree = ObsTree<ArrayTransitions>;
#[allow(clippy::module_name_repetitions)]
/// Type-alias for an uncompressed map-based observation tree.
pub type MapObsTree<S> = ObsTree<MapTransitions<S>>;

/// A basic, uncompressed observation tree.
///
/// We provide two helper type-aliases to avoid re-implementing functionality,
/// see [`MapObsTree`] and [`ArrayObsTree`].
#[derive(Debug, DataSize)]
pub struct ObsTree<T> {
    tree: ArenaTree<T, InputSymbol>,
    input_alphabet_size: usize,
}

impl<T: TransitionInformation> ObservationTree<InputSymbol, OutputSymbol> for ObsTree<T> {
    type S = State;

    fn insert_observation(
        &mut self,
        start: Option<State>,
        input_seq: &[InputSymbol],
        output_seq: &[OutputSymbol],
    ) -> State {
        let start = start.unwrap_or_else(|| State::from(0));
        let mut curr = start;
        for (i, o) in input_seq.iter().zip(output_seq.iter()) {
            curr = self.add_transition_get_destination(curr, *i, *o);
        }
        curr
    }

    fn get_access_seq(&self, state: State) -> Vec<InputSymbol> {
        self.get_transfer_seq(state, State::new(0))
    }

    fn get_transfer_seq(&self, to_state: State, from_state: State) -> Vec<InputSymbol> {
        let mut access_seq = Vec::new();
        if to_state == from_state {
            return access_seq;
        }
        let dest_parent_idx = from_state.raw() as usize;
        let mut curr_state = to_state.raw() as usize;
        loop {
            let (i, parent_idx) = self.tree.arena[curr_state].parent.unwrap();
            access_seq.push(i);
            if parent_idx == dest_parent_idx {
                break;
            }
            curr_state = parent_idx;
        }
        access_seq.reverse();
        access_seq
    }

    fn get_observation(
        &self,
        start: Option<State>,
        input_seq: &[InputSymbol],
    ) -> Option<Vec<OutputSymbol>> {
        let mut s = start.unwrap_or_else(|| State::new(0));
        let mut out_vec = Vec::with_capacity(input_seq.len());
        for &i in input_seq {
            let (o, d) = self.get_out_succ(s, i)?;
            out_vec.push(o);
            s = d;
        }
        Some(out_vec)
    }

    fn get_out_succ(&self, src: State, input: InputSymbol) -> Option<(OutputSymbol, State)> {
        self.tree.ref_at(src.raw() as usize).get_out_succ(input)
    }

    fn get_succ(&self, src: State, input: &[InputSymbol]) -> Option<State> {
        input.iter().try_fold(src, |s, i| self._get_succ(s, *i))
    }

    fn no_succ_defined(&self, basis: &[State], sort: bool) -> Vec<(State, InputSymbol)> {
        let inputs = toolbox::inputs_iterator(self.input_alphabet_size);
        let mut ret: Vec<_> =
            itertools::Itertools::cartesian_product(basis.iter().copied(), inputs)
                .filter(|&(bs, i)| self.get_succ(bs, &[i]).is_none())
                .collect();
        let acc_len = |s| self.get_access_seq(s).len();
        let len_cmp = |a, b| acc_len(a).cmp(&acc_len(b));
        if sort {
            ret.sort_unstable_by(|(a, _), (b, _)| len_cmp(*a, *b));
        }
        ret
    }

    fn size(&self) -> usize {
        self.tree.size()
    }

    fn input_size(&self) -> usize {
        self.input_alphabet_size
    }

    fn tree_and_hyp_states_apart_sink(
        &self,
        s_t: Self::S,
        s_h: State,
        fsm: &crate::definitions::mealy::Mealy,
        sink_output: OutputSymbol,
        depth: usize,
    ) -> bool {
        tree_and_hyp_states_apart_sunk_bounded(self, s_t, s_h, fsm, sink_output, depth)
    }
}

impl<T: TransitionInformation> ObsTree<T> {
    #[must_use]
    pub fn new(input_size: usize) -> Self {
        let node = T::empty(input_size);
        let mut tree = ArenaTree::default();
        let _ = tree.node(node);
        Self {
            tree,
            input_alphabet_size: input_size,
        }
    }
    fn _get_succ(&self, src: State, input: InputSymbol) -> Option<State> {
        self.get_out_succ(src, input).map(|x| x.1)
    }
    fn add_transition_get_destination(
        &mut self,
        src: State,
        i: InputSymbol,
        o: OutputSymbol,
    ) -> State {
        let src_raw: usize = src.raw() as usize;

        if let Some((_o, d)) = self.tree.ref_at(src_raw).get_out_succ(i) {
            d
        } else {
            let dest_node_idx = self
                .tree
                .node_with_parent(T::empty(self.input_size()), src_raw, i);
            let dest_state = State::new(dest_node_idx.try_into().unwrap());
            self.tree.arena[src_raw].val.add_trans(i, o, dest_state);
            dest_state
        }
    }
}

pub trait TransitionInformation: DataSize {
    fn empty(in_size: usize) -> Self;
    fn get_out_succ(&self, i: InputSymbol) -> Option<(OutputSymbol, State)>;
    fn add_trans(&mut self, i: InputSymbol, o: OutputSymbol, d: State);
}

#[derive(Debug, DataSize)]
pub struct MapTransitions<S> {
    trans: HashMap<InputSymbol, (OutputSymbol, State), S>,
}

impl<S: BuildHasher + Default> TransitionInformation for MapTransitions<S> {
    fn empty(_: usize) -> Self {
        Self {
            trans: HashMap::default(),
        }
    }

    fn get_out_succ(&self, i: InputSymbol) -> Option<(OutputSymbol, State)> {
        self.trans.get(&i).copied()
    }

    fn add_trans(&mut self, i: InputSymbol, o: OutputSymbol, d: State) {
        assert_eq!(self.trans.insert(i, (o, d)), None);
    }
}

impl<S: BuildHasher + Default> MapTransitions<S> {
    #[must_use]
    pub fn empty() -> Self {
        Self {
            trans: HashMap::default(),
        }
    }
}

#[derive(Debug, DataSize)]
pub struct ArrayTransitions {
    trans: Vec<Option<(OutputSymbol, State)>>,
}

impl TransitionInformation for ArrayTransitions {
    fn empty(in_size: usize) -> Self {
        ArrayTransitions {
            trans: vec![None; in_size],
        }
    }

    fn get_out_succ(&self, i: InputSymbol) -> Option<(OutputSymbol, State)> {
        self.trans.get(i.raw() as usize).copied()?
    }

    fn add_trans(&mut self, i: InputSymbol, o: OutputSymbol, d: State) {
        let trans = self.trans.get_mut(i.raw() as usize).unwrap();
        *trans = Some((o, d));
    }
}

#[cfg(test)]
mod tests {
    use std::{hash::BuildHasherDefault, path::Path};

    use crate::{
        definitions::{
            mealy::{InputSymbol, Mealy, OutputSymbol},
            FiniteStateMachine,
        },
        learner::obs_tree::ObservationTree,
        util::parsers::machine::read_mealy_from_file,
    };

    use proptest::{collection, proptest, strategy::Strategy};

    use super::MapObsTree;

    type DefaultFxHasher = BuildHasherDefault<rustc_hash::FxHasher>;
    type OTree = MapObsTree<DefaultFxHasher>;
    type S = <OTree as ObservationTree<InputSymbol, OutputSymbol>>::S;

    fn load_basic_fsm(name: &str) -> Mealy {
        let file_name = Path::new(name);
        read_mealy_from_file(file_name.to_str().expect("Safe")).0
    }

    fn try_gen_inputs() -> impl Strategy<Value = (Vec<InputSymbol>, Vec<OutputSymbol>)> {
        let num_inputs = 13usize;
        let fsm = load_basic_fsm("tests/src_models/BitVise.dot");
        #[allow(clippy::cast_possible_truncation)]
        let input_symbol_strat = (0..num_inputs)
            .prop_map(|x| x as u16)
            .prop_map(InputSymbol::from);
        let input_seq_strat = collection::vec(input_symbol_strat, 1..100);
        input_seq_strat
            .prop_map(move |is| (fsm.trace(&is).1.to_vec(), is))
            .prop_map(|(os, is)| (is, os))
    }

    fn gen_seq_sets(
        max_size_set: usize,
        x: impl Strategy<Value = (Vec<InputSymbol>, Vec<OutputSymbol>)>,
    ) -> impl Strategy<Value = Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)>> {
        collection::vec(x, 1..max_size_set)
    }

    proptest! {

        #[test]
        fn xfer_seq_maintained(seqs in gen_seq_sets(100,try_gen_inputs())){
            let mut ret = OTree::new(13);
            for (idx, (is, os)) in seqs.iter().enumerate() {
                ret.insert_observation(None, is, os);
                assert!(ret.get_succ(S::default(), is).is_some());
                for (in_idx, (iis, _)) in seqs[0..idx].iter().enumerate() {
                    let ds = ret.get_succ(S::default(), iis).unwrap();
                    let rx_acc = ret.get_access_seq(ds);
                    assert_eq!(
                        iis, &rx_acc,
                        "Failed at idx {idx} and sub-idx {in_idx}, \n after inserting {is:?}"
                    );
                    for i in 0..iis.len() {
                        let (pref, suff) = iis.split_at(i);
                        let pref_dest = ret.get_succ(S::default(), pref).unwrap();
                        let xfer_seq = ret.get_transfer_seq(ds, pref_dest);
                        assert_eq!(suff, &xfer_seq);
                    }
                }
            }
        }

        #[test]
        fn access_seq_maintained(seqs in gen_seq_sets(500,try_gen_inputs())){
              let mut ret = OTree::new(13);
              for (idx, (is, os)) in seqs.iter().enumerate() {
                ret.insert_observation(None, is, os);
                assert!(ret.get_succ(S::default(), is).is_some());
                for (in_idx, (iis, _)) in seqs[0..idx].iter().enumerate() {
                 let ds = ret.get_succ(S::default(), iis).unwrap();
                let rx_acc = ret.get_access_seq(ds);
                assert_eq!(
                    iis, &rx_acc,
                    "Failed at idx {idx} and sub-idx {in_idx}, \n after inserting {is:?}"
                );
                }
            }
            for (idx, (is, _)) in seqs.iter().enumerate() {
                if let Some(dest) = ret.get_succ(S::default(), is) {
                    let acc_seq = ret.get_access_seq(dest);
                    assert_eq!(is, &acc_seq);
                } else {
                    panic!("Seq number {idx} : {is:?} is not in tree?!");
                }
            }
        }

    }
}
