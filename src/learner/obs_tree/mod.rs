/// Module implementing a compressed observation tree inspired by a Patricia tree.
mod compressed;
/// Compressed observation tree, where nodes are deletable.
mod compressed_deletable;
/// Normal observation tree, with uncompressed nodes.
mod normal;

use std::fmt::Debug;

use datasize::DataSize;

use crate::definitions::mealy::{Mealy, OutputSymbol, State};

pub use crate::learner::obs_tree::compressed::CompObsTree as CompressedObservationTree;
pub use crate::learner::obs_tree::compressed_deletable::Tree as CompressedDeletableObservationTree;
#[allow(clippy::module_name_repetitions)]
pub use crate::learner::obs_tree::normal::ArrayObsTree;
#[allow(clippy::module_name_repetitions)]
pub use crate::learner::obs_tree::normal::MapObsTree;
pub use crate::learner::obs_tree::normal::ObsTree as UncompressedObservationTree;

/// Trait for any struct acting as an observation tree.
///
/// The observation tree is the primary data-structure of L#.
/// Any struct which is an observation tree must implement
/// this trait. The default implementation is [`CompressedObservationTree`].
pub trait ObservationTree<I, O>: DataSize {
    /// Type for the `state` of an observation tree.
    type S: Clone + Default + Send + Sync + Debug;

    /// Insert an observation, with an optional `start` state.
    /// If `start` is None, inserts from root.
    fn insert_observation(
        &mut self,
        start: Option<Self::S>,
        input_seq: &[I],
        output_seq: &[O],
    ) -> Self::S;

    // Getter methods now, a lot compared to insertion ;)

    /// Get the access sequence for `state`, beginning at the root node.
    fn get_access_seq(&self, state: Self::S) -> Vec<I>;
    /// Get the transfer sequence from `from_state` to `to_state`.
    fn get_transfer_seq(&self, to_state: Self::S, from_state: Self::S) -> Vec<I>;
    /// Given an optional `start` state, get the output sequence for `input_seq`,
    /// if defined.
    fn get_observation(&self, start: Option<Self::S>, input_seq: &[I]) -> Option<Vec<O>>;
    /// Given a state and an input, return the output and successor state, if defined.
    fn get_out_succ(&self, src: Self::S, input: I) -> Option<(O, Self::S)>;
    /// Given a state and an input, return the output, if defined.
    fn get_out(&self, src: Self::S, input: I) -> Option<O> {
        self.get_out_succ(src, input).map(|x| x.0)
    }
    /// Given a state and an *input sequence*, return the successor state, if defined.
    fn get_succ(&self, src: Self::S, input: &[I]) -> Option<Self::S>;
    /// Return an array of`(S, I)` where &forall;q &in; `basis`, &forall;i &in; I : &delta;(q,i) is undefined.
    fn no_succ_defined(&self, basis: &[Self::S], sort: bool) -> Vec<(Self::S, I)>;

    // Utility methods

    /// Size of the tree.
    fn size(&self) -> usize;
    /// Size of the input alphabet.
    fn input_size(&self) -> usize;

    fn tree_and_hyp_states_apart_sink(
        &self,
        s_t: Self::S,
        s_h: State,
        fsm: &Mealy,
        sink_output: OutputSymbol,
        depth: usize,
    ) -> bool;
}

/// Supports counting number of nodes of a (sub)-tree from an [`ObservationTree`].
pub trait CountingObservationTree<I, O>: ObservationTree<I, O> {
    /// Number of nodes in the sub-tree rooted at the node reachable with `root_acc_seq`.
    fn count_subtree_nodes(&self, root_acc_seq: &[I]) -> usize;
}

/// Supports deletion of (sub)-trees from an [`ObservationTree`].
pub trait DeletableObservationTree<I, O>: ObservationTree<I, O> {
    /// Delete the sub-tree rooted at the node reachable with `acc_seq`.
    fn delete_subtree_at(&mut self, acc_seq: &[I]);

    /// Clear the entire observation tree.
    fn clear_tree(&mut self);
}
