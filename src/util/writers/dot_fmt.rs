use std::{collections::HashMap, hash::BuildHasher, io::Write};

use itertools::Itertools;

use crate::definitions::mealy::{InputSymbol, OutputSymbol};
use crate::definitions::FiniteStateMachine;

/// Accepts an fsm and conversion maps and returns a vector of bytes.
///
/// If the input and output maps are empty, we print the bare natural numbers of the alphabets.
///
/// # Errors
/// If writing to the internal buffers fail or some state/input is undefined
/// in the maps.
pub fn write_to_dot<S: BuildHasher + Default, M: FiniteStateMachine>(
    fsm: &M,
    input_rev_map: &HashMap<InputSymbol, String, S>,
    output_rev_map: &HashMap<OutputSymbol, String, S>,
) -> std::io::Result<Vec<u8>> {
    let mut vec = Vec::new();
    writeln!(&mut vec, "digraph g {{\n")?;
    let mut state_info = HashMap::<_, _, S>::default();
    for s in fsm.states() {
        state_info.insert(s, vec![]);
        writeln!(
            &mut vec,
            "\t s{0} [shape = \"circle\" label=\"s{0}\"];",
            s.raw()
        )?;
    }
    let num_inputs = fsm.input_alphabet().len();
    for (src, input) in
        Itertools::cartesian_product(fsm.states().into_iter(), fsm.input_alphabet().into_iter())
    {
        let (dest, output) = fsm.step_from(src, input);
        state_info
            .entry(src)
            .or_insert_with(|| Vec::with_capacity(num_inputs))
            .push((input, output, dest));
    }

    if input_rev_map.is_empty() && output_rev_map.is_empty() {
        for (src, transitions) in state_info.into_iter().sorted() {
            for (i_s, o_s, dest) in transitions {
                let i: usize = i_s.into();
                let o: u16 = o_s.raw();
                writeln!(
                    &mut vec,
                    "\t s{} -> s{} [label=\"{} / {}\"];",
                    src.raw(),
                    dest.raw(),
                    i,
                    o
                )?;
            }
        }
    } else {
        for (src, transitions) in state_info.into_iter().sorted() {
            for (i_s, o_s, dest) in transitions {
                let i_str = input_rev_map.get(&i_s).expect("Input absent from map");
                let o_str = output_rev_map.get(&o_s).expect("Output absent from map");
                writeln!(
                    &mut vec,
                    "\t s{} -> s{} [label=\"{} / {}\"];",
                    src.raw(),
                    dest.raw(),
                    i_str,
                    o_str
                )?;
            }
        }
    }

    writeln!(
        &mut vec,
        r#"__start0 [label="" shape="none" width="0" height="0"];"#
    )?;
    writeln!(
        &mut vec,
        "\t\t__start0 -> s{};\n\n}}\n",
        fsm.initial_state().raw()
    )?;
    Ok(vec)
}
