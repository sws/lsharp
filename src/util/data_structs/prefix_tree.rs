use std::collections::VecDeque;

use rand::{rngs::StdRng, seq::SliceRandom, SeedableRng};
use rustc_hash::FxHashMap;

use crate::definitions::mealy::InputSymbol;

use super::arena_tree::ArenaTree;

#[derive(Debug, Default)]
struct PrefixNode {
    // An InputSymbol and its corresponding child node.
    children: FxHashMap<InputSymbol, usize>,
}

/// A `PrefixTree`, given a set of input sequences,
/// stores only the maximal input sequences.
#[derive(Debug)]
pub struct PrefixTree {
    tree: ArenaTree<PrefixNode, ()>,
}

impl Default for PrefixTree {
    fn default() -> Self {
        let node = PrefixNode::default();
        let mut tree = ArenaTree::default();
        tree.node(node);
        Self { tree }
    }
}

impl FromIterator<Vec<InputSymbol>> for PrefixTree {
    fn from_iter<T: IntoIterator<Item = Vec<InputSymbol>>>(iter: T) -> Self {
        let mut tree = Self::default();
        for x in iter {
            tree.insert(&x);
        }
        tree
    }
}

use std::collections::hash_map::Entry;
impl PrefixTree {
    /// Insert a sequence `word` into the tree.
    /// Returns `true` if inserted (i.e., `word` was maximal.)
    pub fn insert(&mut self, word: &[InputSymbol]) -> bool {
        let mut curr_node_idx = 0usize;
        let mut inserted = false;
        for i in word {
            let c_children = &mut self.tree.arena[curr_node_idx].val.children;
            if let Entry::Occupied(nxt) = c_children.entry(*i) {
                curr_node_idx = *nxt.get();
            } else {
                let d_node = PrefixNode::default();
                let x = self.tree.node(d_node);
                self.tree.arena[curr_node_idx].val.children.insert(*i, x);
                curr_node_idx = x;
                inserted = true;
            }
        }
        inserted
    }

    pub fn random_dfs_iter(&self, seed: u64) -> RandomizedPrefixTreeIterator {
        let rng = StdRng::seed_from_u64(seed);
        RandomizedPrefixTreeIterator {
            word: Vec::new(),
            to_visit: VecDeque::from([(InputSymbol::new(u16::MAX), 1, 0)]),
            tree: self,
            rng,
        }
    }
}

impl<'a> IntoIterator for &'a PrefixTree {
    type Item = Vec<InputSymbol>;

    type IntoIter = PrefixTreeIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        PrefixTreeIterator {
            word: Vec::new(),
            // We ignore the first input in the iterator.
            to_visit: VecDeque::from([(InputSymbol::new(u16::MAX), 1, 0)]),
            tree: self,
        }
    }
}

#[allow(clippy::module_name_repetitions)]
#[derive(Debug)]
pub struct PrefixTreeIterator<'a> {
    word: Vec<InputSymbol>,
    to_visit: VecDeque<(InputSymbol, usize, usize)>,
    tree: &'a PrefixTree,
}

impl<'a> Iterator for PrefixTreeIterator<'a> {
    type Item = Vec<InputSymbol>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.to_visit.is_empty() {
            return None;
        }
        while let Some((i, depth, c_node)) = self.to_visit.pop_front() {
            self.word.resize(depth - 1, InputSymbol::new(u16::MAX));
            self.word.push(i);
            if self.tree.tree.arena[c_node].val.children.is_empty() {
                let ret = Some(self.word[1..].to_vec());
                self.word.pop();
                return ret;
            }
            for (x, nxt) in &self.tree.tree.arena[c_node].val.children {
                self.to_visit.push_front((*x, depth + 1, *nxt));
            }
        }
        unreachable!("Impossible case when iterating over PrefixTree.");
    }
}

pub struct RandomizedPrefixTreeIterator<'a> {
    word: Vec<InputSymbol>,
    to_visit: VecDeque<(InputSymbol, usize, usize)>,
    tree: &'a PrefixTree,
    rng: StdRng,
}

impl<'a> Iterator for RandomizedPrefixTreeIterator<'a> {
    type Item = Vec<InputSymbol>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.to_visit.is_empty() {
            return None;
        }
        while let Some((i, depth, c_node)) = self.to_visit.pop_front() {
            self.word.resize(depth - 1, InputSymbol::new(u16::MAX));
            self.word.push(i);
            if self.tree.tree.arena[c_node].val.children.is_empty() {
                let ret = Some(self.word[1..].to_vec());
                self.word.pop();
                return ret;
            }
            let mut children: Vec<_> = self.tree.tree.arena[c_node].val.children.iter().collect();
            children.shuffle(&mut self.rng);
            for (x, nxt) in children {
                self.to_visit.push_front((*x, depth + 1, *nxt));
            }
        }
        unreachable!("Impossible case when iterating over PrefixTree.");
    }
}

#[cfg(test)]
mod tests {
    use std::collections::VecDeque;

    use fnv::FnvHashSet;
    use itertools::Itertools;
    use proptest::strategy::Strategy;
    use proptest::{collection, proptest};
    use rustc_hash::FxHashSet;

    use crate::definitions::mealy::InputSymbol;
    use crate::util::sequences::FixedInfixGenerator;
    use crate::util::toolbox;

    use super::PrefixTree;

    #[test]
    fn can_create() {
        let x = PrefixTree::default();
        assert_eq!(x.tree.size(), 1);
    }

    #[test]
    fn insertion_and_iteration() {
        let i_str = "[[1,2,3],[1,2],[1],[1,2,5],[1,2,5,6],[1,2,5,4],[6],[7]]";
        let input_seqs: Vec<Vec<InputSymbol>> = serde_json::from_str(i_str).expect("Safe");

        let mut p_tree = PrefixTree::default();
        for x in input_seqs {
            p_tree.insert(&x);
        }

        let e_str = "[[1,2,3],[1,2,5,6],[1,2,5,4],[6],[7]]";
        let e_seqs_set: Vec<Vec<InputSymbol>> = serde_json::from_str(e_str).expect("Safe");
        let e_seqs_set: FxHashSet<_> = e_seqs_set.into_iter().collect();

        let seq_set = p_tree.into_iter().collect();
        assert_eq!(e_seqs_set, seq_set);
    }

    /// Example from Joshua's dissertation for Fig 2.5 and the W-method.
    #[test]
    fn example() {
        let a = InputSymbol::new(0);
        let b = InputSymbol::new(1);
        let c = InputSymbol::new(2);
        let state_cover = vec![vec![], vec![a], vec![a, a], vec![b], vec![b, a]];
        let fig = FixedInfixGenerator::new(vec![a, b, c], 0, crate::util::sequences::Order::ASC);
        let q: Vec<Vec<InputSymbol>> = state_cover
            .iter()
            .cloned()
            .cartesian_product(fig.generate())
            .map(|(x, y)| toolbox::concat_slices(&[&x, &y]))
            .collect();
        let p_u_q = {
            let mut ret = FxHashSet::default();
            ret.extend(q.into_iter());
            ret.extend(state_cover.iter().cloned());
            ret
        };
        let char_set = vec![vec![a, a], vec![a, c], vec![c]];
        let ts: FxHashSet<_> = p_u_q
            .into_iter()
            .cartesian_product(char_set.into_iter())
            .map(|(x, y)| toolbox::concat_slices(&[&x, &y]))
            .collect();
        let tree: PrefixTree = ts.into_iter().collect();
        let result: FxHashSet<_> = tree.into_iter().collect();
        let expected = vec![
            vec![a, a, a, a, a],
            vec![a, a, a, a, c],
            vec![a, a, a, c],
            vec![a, a, b, a, a],
            vec![a, a, b, a, c],
            vec![a, a, b, c],
            vec![a, a, c, a, a],
            vec![a, a, c, a, c],
            vec![a, a, c, c],
            vec![a, b, a, a],
            vec![a, b, a, c],
            vec![a, b, c],
            vec![a, c, a, a],
            vec![a, c, a, c],
            vec![a, c, c],
            vec![b, a, a, a, a],
            vec![b, a, a, a, c],
            vec![b, a, a, c],
            vec![b, a, b, a, a],
            vec![b, a, b, a, c],
            vec![b, a, b, c],
            vec![b, a, c, a, a],
            vec![b, a, c, a, c],
            vec![b, a, c, c],
            vec![b, b, a, a],
            vec![b, b, a, c],
            vec![b, b, c],
            vec![b, c, a, a],
            vec![b, c, a, c],
            vec![b, c, c],
            vec![c, a, a],
            vec![c, a, c],
            vec![c, c],
        ];
        let expected: FxHashSet<_> = expected.into_iter().collect();
        assert_eq!(expected, result);
    }

    fn naive_compute_maximal_set(
        mut char_set: Vec<Vec<InputSymbol>>,
    ) -> FnvHashSet<Vec<InputSymbol>> {
        // Sort into descending order;
        char_set.sort_by_key(|x| std::cmp::Reverse(x.len()));
        let mut char_set: VecDeque<_> = char_set.into_iter().collect();

        let first_seq = char_set
            .pop_front()
            .expect("Should be at least one element!");
        let mut reduced_set = Vec::new();
        reduced_set.push(first_seq);
        for seq in char_set {
            if reduced_set.iter().any(|other| is_subseq_of(&seq, other)) {
                continue;
            }
            reduced_set.push(seq);
        }
        reduced_set.into_iter().collect()
    }

    fn is_subseq_of(seq: &[InputSymbol], other: &[InputSymbol]) -> bool {
        if seq.len() > other.len() {
            return false;
        }
        assert!(
            !(seq.is_empty() || other.is_empty()),
            "Cannot compare empty sequences."
        );
        // seq's len is now \leq other's len.
        let is_subseq = Iterator::zip(seq.iter(), other.iter()).all(|(i1, i2)| *i1 == *i2);
        is_subseq
    }

    fn prefix_tree_reduce(char_set: Vec<Vec<InputSymbol>>) -> FnvHashSet<Vec<InputSymbol>> {
        let p_tree: PrefixTree = char_set.into_iter().collect();
        p_tree.into_iter().collect()
    }

    fn try_gen_inputs() -> impl Strategy<Value = Vec<InputSymbol>> {
        let num_inputs = 13usize;
        #[allow(clippy::cast_possible_truncation)]
        let input_symbol_strat = (0..num_inputs)
            .prop_map(|x| x as u16)
            .prop_map(InputSymbol::from);
        collection::vec(input_symbol_strat, 1..100)
    }

    fn gen_seq_sets(
        max_size_set: usize,
        x: impl Strategy<Value = Vec<InputSymbol>>,
    ) -> impl Strategy<Value = Vec<Vec<InputSymbol>>> {
        collection::vec(x, 1..max_size_set)
    }

    proptest! {
        /// Check whether the reduction applied by a PrefixTree is the
        /// same as a naive reduction.
        #[test]
        fn prefix_tree_reduction(set in gen_seq_sets(100,try_gen_inputs())){
            let naive_reduced_set = naive_compute_maximal_set(set.clone());
            let prefix_reduced_set = prefix_tree_reduce(set);
            assert_eq!(naive_reduced_set, prefix_reduced_set);
        }
    }
}
