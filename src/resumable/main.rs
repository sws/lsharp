use std::{cell::RefCell, hash::BuildHasherDefault, rc::Rc};

use datasize::DataSize;
use fnv::FnvHashMap;
use learning_config::EqOracle;
use lsharp_ru::definitions::mealy::{InputSymbol, OutputSymbol};
use lsharp_ru::definitions::FiniteStateMachine;
use lsharp_ru::learner::l_sharp::{Lsharp, Rule2, Rule3};
use lsharp_ru::learner::obs_tree::CompressedObservationTree as CompObsTree;
use lsharp_ru::oracles::equivalence::generic::{CharStyle, EOParams};
use lsharp_ru::oracles::equivalence::incomplete::iads::IadsEO;
use lsharp_ru::oracles::equivalence::EquivalenceOracle;
use lsharp_ru::oracles::equivalence::InfixStyle;
use lsharp_ru::oracles::OutputOracle as OQOracle;
use lsharp_ru::sul::Simulator;
use lsharp_ru::sul::{SinkWrapper, SystemUnderLearning};
use lsharp_ru::util::parsers::machine::read_mealy_from_file_using_maps;
use serde::{Deserialize, Serialize};

mod learning_config;

#[derive(derive_builder::Builder, Serialize, Deserialize)]
pub struct Options {
    pub rule2_mode: Rule2,
    pub rule3_mode: Rule3,
    pub oracle_choice: EqOracle,
    pub seed: u64,
    pub extra_states: usize,
    pub expected_rnd_length: usize,
    pub use_ly_ads: bool,
    pub infix_style: InfixStyle,
    pub quiet: bool,
    pub shuffle: bool,
    pub comp_tree: bool,
    pub cache_rules: bool,
}

type DefaultFxHasher = BuildHasherDefault<rustc_hash::FxHasher>;

pub fn main() -> Result<(), Box<dyn std::error::Error>> {
    let options: Options = get_options("./resumable_config/opts.json");
    let (logs, input_map, output_map) = get_minimal_sequences("./resumable_config/tree.json");

    let mealy_machine = read_mealy_from_file_using_maps(
        "esm_models/esm-manual-controller.dot",
        &input_map,
        &output_map,
    )?;

    let trial_wo_sink = Simulator::new(&mealy_machine, &input_map, &output_map);
    let sink_output_asml = *output_map
        .get("error")
        .unwrap_or(&OutputSymbol::new(u16::MAX));
    let mut trial = SinkWrapper::new(trial_wo_sink, sink_output_asml);
    let comp_tree = CompObsTree::<DefaultFxHasher>::new(trial.input_map().len());

    let oq_oracle = Rc::new(RefCell::new(OQOracle::new(
        &mut trial,
        options.rule2_mode.clone(),
        options.rule3_mode.clone(),
        sink_output_asml,
        options.seed,
        comp_tree,
        options.cache_rules,
    )));

    let mut learner: Lsharp<_> = Lsharp::new(
        Rc::clone(&oq_oracle),
        mealy_machine.input_alphabet().len(),
        options.use_ly_ads,
        options.cache_rules,
    );
    let mut eq_oracle = {
        let gen_eo_params = |char_style| {
            EOParams::new(
                options.extra_states,
                options.expected_rnd_length,
                options.seed,
                char_style,
                options.shuffle,
                options.infix_style,
            )
        };
        let iads_oracle = IadsEO::new(
            Rc::clone(&oq_oracle),
            options.extra_states,
            gen_eo_params(CharStyle::Hads),
        );
        iads_oracle
    };
    println!("Loaded full configuration.");
    learner.init_obs_tree(Some(logs));
    println!("Loaded minimal traces.");

    let mut idx = 1;
    let success: bool;
    let mut hyp_states;

    loop {
        if !options.quiet {
            println!("LOOP {idx}");
        }

        let hypothesis = learner.build_hypothesis();
        let (l_inputs, l_resets) = learner.get_counts();
        if !options.quiet {
            println!("Learning queries/symbols: {l_resets}/{l_inputs}",);
        }
        log::info!("Learning queries/symbols: {l_resets}/{l_inputs}");
        hyp_states = hypothesis.states().len();
        println!(
            "Hypothesis size: {} and model size: {}",
            hyp_states,
            mealy_machine.states().len()
        );
        println!("Calling for BFS CEX...");
        // let ideal_ce = shortest_separating_sequence(&sul.clone(), &hypothesis, None, None);
        let ideal_ce = Some((vec![], vec![]));
        println!("BFS CEX found!");
        if ideal_ce.is_none() {
            if !options.quiet {
                println!("Learning finished!");
            }
            break;
        }
        log::info!(
            "Length of shortest separating sequence: {}",
            ideal_ce.as_ref().map_or(0, |x| x.0.len())
        );
        log::info!(
            "Ideal CE: {:?}",
            ideal_ce.as_ref().map_or(&vec![], |x| &x.0)
        );
        if !options.quiet {
            println!("Searching for CEX...");
        }
        let ce = if options.oracle_choice == EqOracle::Internal {
            ideal_ce.clone()
        } else {
            eq_oracle.find_counterexample(&hypothesis)
        };
        let (t_inputs, t_resets) = eq_oracle.get_counts();
        if !options.quiet {
            println!("Testing queries/symbols: {t_resets}/{t_inputs}");
        }
        log::info!("Testing queries/symbols: {}/{}", t_resets, t_inputs);

        let ce_len = ce.as_ref().map_or(0, |f| f.0.len());
        log::info!("EQStats:{{'Iter':{idx},'HypSize':{hyp_states},'CESize':{ce_len}}}");
        let Some(ce) = ce else {
            success = ideal_ce.is_none();
            if !options.quiet {
                println!("No CE found.");
                println!("Model learned: {success}");
            }
            break;
        };
        log::info!("CE: {:?}", ce.0);
        if !options.quiet {
            println!("CE found, refining observation tree!");
        }
        learner.process_cex(Some(ce), &hypothesis);

        let temp = RefCell::borrow(&oq_oracle);
        let tree = temp.borrow_tree();

        let tree_size = tree.estimate_heap_size();
        log::info!("OTreeSize: {}", tree_size);
        idx += 1;
    }

    Ok(())
}

fn get_minimal_sequences(
    file: &str,
) -> (
    Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)>,
    FnvHashMap<String, InputSymbol>,
    FnvHashMap<String, OutputSymbol>,
) {
    let seq_str = std::fs::read(file).expect("Could not read options.");
    serde_json::from_slice(&seq_str).expect("Could not parse sequences.")
}

fn get_options(file: &str) -> Options {
    let opts_str = std::fs::read(file).expect("Could not read options.");
    serde_json::from_slice(&opts_str).expect("Could not parse options.")
}
