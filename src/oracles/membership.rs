use std::borrow::Cow;
use std::collections::{BTreeMap, BTreeSet};

use datasize::DataSize;
use itertools::Itertools;
use rand::{prelude::SliceRandom, rngs::StdRng, SeedableRng};
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use rustc_hash::{FxHashMap, FxHashSet};

use crate::ads::Ads as OtADS;
use crate::ads::{AdaptiveDistinguishingSequence, AdsStatus};
use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;
use crate::learner::apartness::{acc_states_are_apart, compute_witness, states_are_apart};
use crate::learner::l_sharp::{Rule2, Rule3};
use crate::learner::obs_tree::ObservationTree;
use crate::oracles::equivalence::incomplete::impl_ads::AdsTree as IADSTree;
use crate::oracles::equivalence::incomplete::tree::SplittingTree as IADSSplitTree;
use crate::oracles::equivalence::CounterExample;
use crate::sul::SystemUnderLearning;
use crate::util::toolbox;

use self::util::Pair;

macro_rules! get_state_from_acc {
    ($self:ident,$acc:ident) => {
        $self
            .obs_tree
            .get_succ(T::S::default(), $acc)
            .expect("Safe")
    };
}

/// `OutputOracle` is the primary structure used to interact with the SUL.
/// All queries go via this struct.
#[derive(DataSize)]
pub struct Oracle<'a, T> {
    #[data_size(skip)]
    sul: &'a mut dyn SystemUnderLearning,
    obs_tree: T,
    #[data_size(skip)]
    rule2: Rule2,
    #[data_size(skip)]
    rule3: Rule3,
    #[data_size(skip)]
    /// Sink state, if assigned.
    sink_state: Option<Vec<InputSymbol>>,
    #[data_size(skip)]
    /// Sink output.
    pub sink_output: OutputSymbol,
    #[data_size(skip)]
    /// Rng
    rng: StdRng,

    // Store ADSs/Seq in cache.
    #[data_size(skip)]
    ads_cache: BTreeMap<BTreeSet<Vec<InputSymbol>>, OtADS>,
    #[data_size(skip)]
    seq_cache: BTreeMap<Pair, Vec<InputSymbol>>,
    cache: bool,
    #[data_size(skip)]
    prev_basis_size: usize,
    #[data_size(skip)]
    old_basis_access: FxHashMap<Vec<InputSymbol>, State>,
    #[data_size(skip)]
    last_ce_consistency: bool,
    #[data_size(skip)]
    ads_support: CachedADSSuport,
}

impl<'a, T> Oracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    pub fn new(
        sul: &'a mut dyn SystemUnderLearning,
        rule2: Rule2,
        rule3: Rule3,
        sink_output: OutputSymbol,
        seed: u64,
        obs_tree: T,
        cache: bool,
    ) -> Self {
        Self {
            obs_tree,
            sul,
            rule2,
            rule3,
            sink_state: None,
            sink_output,
            rng: SeedableRng::seed_from_u64(seed),
            ads_cache: BTreeMap::default(),
            seq_cache: BTreeMap::default(),
            cache,
            prev_basis_size: 1,
            old_basis_access: FxHashMap::default(),
            last_ce_consistency: false,
            ads_support: CachedADSSuport::default(),
        }
    }

    pub fn get_counts(&mut self) -> (usize, usize) {
        self.sul.get_counts()
    }

    /// Immutably borrow the underlying observation tree.
    pub fn borrow_tree(&self) -> &T {
        &self.obs_tree
    }

    /// Mutably borrow the underlying observation tree.
    pub fn borrow_mut_tree(&mut self) -> &mut T {
        &mut self.obs_tree
    }

    /// Insert extra symbols in the observation tree for sink states.
    /// We won't then ever need to do queries where the sink-state is the source.
    fn make_sink(&mut self, s: &T::S) {
        for i in toolbox::inputs_iterator(self.obs_tree.input_size()) {
            self.obs_tree
                .insert_observation(Some(s.clone()), &[i], &[self.sink_output]);
        }
    }

    fn identify_frontier_old_ads(
        &mut self,
        candidates: &[Cow<[InputSymbol]>],
        prefix: &[InputSymbol],
    ) -> (Vec<InputSymbol>, Vec<OutputSymbol>) {
        let ads = self.ads_support.get_ads_if_unvisited(prefix);
        if let Some(mut ads) = ads {
            let (input_seq, output_seq) = self.adaptive_output_query_no_infix(prefix, &mut ads);
            self.obs_tree
                .insert_observation(None, &input_seq, &output_seq);
            let norm_increased = candidates
                .par_iter()
                .filter(|b| acc_states_are_apart(&self.obs_tree, prefix, b))
                .count();
            if norm_increased > 0 {
                let states_split = (norm_increased as f64) / (candidates.len() as f64);
                log::info!("Split {} of {}", states_split, candidates.len());
                return (input_seq, output_seq);
            }
        }
        log::info!("Cached hyp ADS no help.");
        self.identify_frontier_ads_no_cache(candidates, prefix)
    }
}

impl<'a, T> Oracle<'a, T>
where
    T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
{
    pub fn clear_rule_caches(&mut self) {
        self.ads_cache.clear();
        self.seq_cache.clear();
    }

    /// Identifies state `fs` amongst the basis candidates to just one basis candidate.
    /// # Panics
    /// If norm did not increase.
    pub fn identify_frontier(
        &mut self,
        fs_acc: &[InputSymbol],
        candidates: &mut Vec<Cow<[InputSymbol]>>,
    ) {
        // For identification of the frontier, we need to make changes for the old basis states.
        candidates.retain(|bs_acc| !acc_states_are_apart(&self.obs_tree, fs_acc, &bs_acc));
        let org_cand_len = candidates.len();
        if org_cand_len < 2 {
            // Already identified/isolated.
            return;
        }
        let prefix = fs_acc.to_vec();
        let (input_seq, output_seq) = match (&self.rule3, candidates.len()) {
            (Rule3::SepSeq, _) | (_, 2) => self.identify_frontier_sepseq(candidates, prefix),
            (Rule3::Ads, _) => self.identify_frontier_ads(candidates, &prefix),
        };
        self.obs_tree
            .insert_observation(None, &input_seq, &output_seq);
        candidates.retain(|b| !acc_states_are_apart(&self.obs_tree, fs_acc, b));
        assert!(candidates.len() < org_cand_len, "Did not increase norm!");
    }

    #[inline]
    fn get_or_compute_witness(
        &mut self,
        q1_acc: &[InputSymbol],
        q2_acc: &[InputSymbol],
    ) -> Vec<InputSymbol> {
        let pair = Pair::from([q1_acc.as_ref(), q2_acc.as_ref()]);
        self.seq_cache
            .entry(pair)
            .or_insert_with(|| {
                let q1 = get_state_from_acc!(self, q1_acc);
                let q2 = get_state_from_acc!(self, q2_acc);
                compute_witness(&self.obs_tree, q1, q2).expect("Safe")
            })
            .clone()
    }

    fn identify_frontier_sepseq(
        &mut self,
        candidates: &[Cow<[InputSymbol]>],
        prefix: Vec<InputSymbol>,
    ) -> (Vec<InputSymbol>, Vec<OutputSymbol>) {
        let (q1_acc, q2_acc) = candidates
            .choose_multiple(&mut self.rng, 2)
            .collect_tuple()
            .expect("Safe");
        let mut wit = self.get_or_compute_witness(q1_acc, q2_acc);
        let mut input_seq = prefix;
        input_seq.append(&mut wit);
        let output_seq = self.output_query(&input_seq);
        (input_seq, output_seq)
    }

    /// Use ADSs to identify frontier states.
    fn identify_frontier_ads(
        &mut self,
        candidates: &[Cow<[InputSymbol]>],
        prefix: &[InputSymbol],
    ) -> (Vec<InputSymbol>, Vec<OutputSymbol>) {
        let old_basis = self.filter_old_basis(candidates);
        if old_basis.len() > 2 && self.cache {
            self.identify_frontier_old_ads(candidates, prefix)
        } else {
            self.identify_frontier_ads_no_cache(candidates, prefix)
        }
    }

    fn identify_frontier_ads_no_cache(
        &mut self,
        candidates: &[Cow<[InputSymbol]>],
        prefix: &[InputSymbol],
    ) -> (Vec<InputSymbol>, Vec<OutputSymbol>)
    where
        T: ObservationTree<InputSymbol, OutputSymbol> + Sync + Send,
    {
        let set: BTreeSet<_> = candidates.iter().map(|x| x.to_vec()).collect();
        let mut suffix = {
            if let Some(ads) = self.ads_cache.get(&set) {
                ads.clone()
            } else {
                let candss = candidates
                    .iter()
                    .map(|acc| get_state_from_acc!(self, acc))
                    .collect_vec();
                let suff = OtADS::new(&self.obs_tree, &candss, Some(self.sink_output));
                if self.cache {
                    self.ads_cache.insert(set, suff.clone());
                }
                suff
            }
        };
        self.adaptive_output_query_no_infix(prefix, &mut suffix)
    }

    /// Explores the frontier for all basis states and returns a vector of the frontier states
    /// paired with their corresponding basis candidates.
    #[must_use]
    pub fn explore_frontier(
        &mut self,
        basis: &[Cow<[InputSymbol]>],
    ) -> Vec<(Vec<InputSymbol>, Vec<Vec<InputSymbol>>)> {
        let inputs = toolbox::inputs_iterator(self.obs_tree.input_size());
        let bs_st_acc_iter = basis.iter().map(|bs_acc| {
            let bs = get_state_from_acc!(self, bs_acc);
            (bs, bs_acc)
        });
        let to_explore: Vec<_> = bs_st_acc_iter
            .cartesian_product(inputs.clone())
            .filter(|((bs_st, _bs_acc), i)| self.obs_tree.get_succ(bs_st.clone(), &[*i]).is_none())
            .map(|((_, bs_acc), i)| (bs_acc, i))
            .collect();

        to_explore
            .into_iter()
            .inspect(|(q, i)| log::debug!("{:?} has {:?} undefined", q, i))
            .map(|(q_acc, i)| self._explore_frontier(q_acc, i, basis))
            .collect()
    }

    /// Explore the frontier for a single (state, input) pair.
    fn _explore_frontier(
        &mut self,
        acc_q: &[InputSymbol],
        i: InputSymbol,
        basis: &[Cow<[InputSymbol]>],
    ) -> (Vec<InputSymbol>, Vec<Vec<InputSymbol>>) {
        // We will not have to deal with exploring the frontier for old basis
        // states, as those are already extended. For the new basis states, we
        // don't need to make any changes to the code.
        let mut access_q = acc_q.to_vec();
        let q = get_state_from_acc!(self, acc_q);
        #[allow(clippy::cast_precision_loss)]
        let update_r2_ads = basis.len() as f64 >= 1.1 * (self.prev_basis_size as f64);
        if update_r2_ads {
            self.prev_basis_size = basis.len();
        }
        let (input_seq, output_seq) = match (self.rule2, basis.len()) {
            (Rule2::Ads, _) => {
                log::debug!("Constructing ADS for {:?} at {:?}", access_q, i);
                let mut suffix = {
                    let set: BTreeSet<_> = basis.iter().map(|x| x.to_vec()).collect();
                    if update_r2_ads {
                        let candss = basis
                            .iter()
                            .map(|acc| get_state_from_acc!(self, acc))
                            .collect_vec();
                        let suff = OtADS::new(&self.obs_tree, &candss, Some(self.sink_output));
                        if self.cache {
                            self.ads_cache.insert(set.clone(), suff.clone());
                        }
                    }
                    if let Some(ads) = self.ads_cache.get(&set) {
                        ads.clone()
                    } else {
                        let candss = basis
                            .iter()
                            .map(|acc| get_state_from_acc!(self, acc))
                            .collect_vec();
                        let suff = OtADS::new(&self.obs_tree, &candss, Some(self.sink_output));
                        if self.cache {
                            self.ads_cache.insert(set, suff.clone());
                        }
                        suff
                    }
                };
                self.adaptive_output_query(&mut access_q, Some(i), &mut suffix)
            }
            // We will always have at least one basis state.
            (Rule2::Nothing, _) | (Rule2::SepSeq, 1) => {
                let mut prefix = access_q;
                prefix.push(i);
                let o_seq = self.output_query(&prefix);
                (prefix, o_seq)
            }
            (Rule2::SepSeq, _) => {
                let wit = {
                    let (q1_acc, q2_acc) = basis
                        .choose_multiple(&mut self.rng, 2)
                        .collect_tuple()
                        .expect("Safe");
                    self.get_or_compute_witness(q1_acc, q2_acc)
                };
                let mut input_seq = access_q;
                input_seq.push(i);
                input_seq.extend(wit);
                let output_seq = self.output_query(&input_seq);
                (input_seq, output_seq)
            }
        };
        self.obs_tree
            .insert_observation(None, &input_seq, &output_seq);
        let fs = self.obs_tree.get_succ(q, &[i]).expect("Safe");
        log::debug!("Added frontier {:?}.", fs);
        let bs_not_sep = basis
            .par_iter()
            .filter(|&b| {
                let bs = get_state_from_acc!(self, b);
                !states_are_apart(&self.obs_tree, fs.clone(), bs)
            })
            .map(|x| x.to_vec())
            .collect();
        let mut fs = acc_q.to_vec();
        fs.push(i);
        (fs, bs_not_sep)
    }

    /// Make a non-adaptive output query.
    pub fn output_query(&mut self, input_seq: &[InputSymbol]) -> Vec<OutputSymbol> {
        if let Some(x) = self.obs_tree.get_observation(None, input_seq) {
            return x;
        }
        self.sul.reset();
        let out_seq: Vec<_> = input_seq.iter().map(|&i| self.sul.single_step(i)).collect();
        if self.sink_state.is_none() && out_seq.last().copied() == Some(self.sink_output) {
            self.sink_state = Some(input_seq.to_vec());
        }
        self.add_observation(input_seq, &out_seq);
        out_seq
    }

    /// Make an adaptive output query with no infix.
    fn adaptive_output_query_no_infix<ADS: AdaptiveDistinguishingSequence>(
        &mut self,
        prefix: &[InputSymbol],
        suffix: &mut ADS,
    ) -> (Vec<InputSymbol>, Vec<OutputSymbol>) {
        self._adaptive_output_query(prefix, suffix)
    }

    /// Make an adaptive output query of the form (prefix -> infix -> ADS).
    fn adaptive_output_query<ADS: AdaptiveDistinguishingSequence>(
        &mut self,
        prefix: &mut Vec<InputSymbol>,
        infix: Option<InputSymbol>,
        suffix: &mut ADS,
    ) -> (Vec<InputSymbol>, Vec<OutputSymbol>) {
        if let Some(i) = infix {
            prefix.push(i);
        }
        self._adaptive_output_query(prefix, suffix)
    }

    /// Make an adaptive query, where the infix has been moved into the prefix.
    fn _adaptive_output_query<ADS: AdaptiveDistinguishingSequence>(
        &mut self,
        prefix: &[InputSymbol],
        suffix: &mut ADS,
    ) -> (Vec<InputSymbol>, Vec<OutputSymbol>) {
        let tree_reply = self
            .obs_tree
            .get_succ(T::S::default(), prefix)
            .and_then(|curr_state| self.answer_ads_from_tree(suffix, curr_state));
        suffix.reset_to_root();
        if let Some((mut inputs_sent, mut outputs_received)) = tree_reply {
            let mut input_word = prefix.to_vec();
            input_word.append(&mut inputs_sent);
            let mut output_word = self.obs_tree.get_observation(None, prefix).expect("Safe");
            output_word.append(&mut outputs_received);
            return (input_word, output_word);
        }
        self.sul.reset();
        let prefix_out = self.sul.step(prefix);
        // If the last output was a sink output, then we know that the current state of the
        // SUL is a sink state.
        if prefix_out.last().copied() == Some(self.sink_output) {
            let sink = self.add_observation(prefix, &prefix_out);
            if self.sink_state.is_none() {
                self.sink_state = Some(prefix.to_vec());
            }
            self.make_sink(&sink);
            return (prefix.to_vec(), prefix_out.to_vec());
        }
        let (mut suffix_inputs, suffix_outputs) = self.sul_adaptive_query(suffix);
        let mut input_seq = prefix.to_vec();
        input_seq.append(&mut suffix_inputs);
        let output_seq = toolbox::concat_slices(&[&prefix_out, &suffix_outputs]);
        self.add_observation(&input_seq, &output_seq);
        (input_seq.clone(), output_seq)
    }

    // Assuming the prefix has been sent to the SUL, perform the adaptive query.
    fn sul_adaptive_query<ADS: AdaptiveDistinguishingSequence>(
        &mut self,
        ads: &mut ADS,
    ) -> (Vec<InputSymbol>, Box<[OutputSymbol]>) {
        let mut inputs_sent = Vec::new();
        let mut outputs_received = Vec::new();
        let mut last_output = None;
        while let Ok(next_input) = ads.next_input(last_output) {
            log::debug!("Next input: {:?}", next_input);
            inputs_sent.push(next_input);
            let o = self.sul.single_step(next_input);
            last_output = Some(o);
            outputs_received.push(o);
        }
        log::debug!("Next input undefined.");
        (inputs_sent, outputs_received.into())
    }

    /// .
    ///
    /// # Panics
    ///
    /// Panics if query was duplicate.
    pub fn ads_equiv_test(
        &mut self,
        ads: &mut impl AdaptiveDistinguishingSequence,
        prefix: &[InputSymbol],
        fsm: &Mealy,
    ) -> CounterExample {
        // For sinks.
        {
            let mut curr = T::S::default();
            for i in prefix {
                if let Some((o, succ)) = self.obs_tree.get_out_succ(curr, *i) {
                    // We encountered a sink state, might as well exit.
                    if o == self.sink_output {
                        return None;
                    }
                    curr = succ;
                } else {
                    break;
                }
            }
        }

        if let Some(tree_prefix_out) = self.obs_tree.get_observation(None, prefix) {
            let hyp_out = fsm.trace(prefix).1.to_vec();
            for (idx, (tree_o, hyp_o)) in tree_prefix_out.iter().zip(hyp_out.iter()).enumerate() {
                if tree_o != hyp_o {
                    let prefix_in = prefix[..=idx].to_vec();
                    let hyp_out = hyp_out[..=idx].to_vec();
                    return Some((prefix_in, hyp_out));
                }
            }
            let ts = self
                .obs_tree
                .get_succ(T::S::default(), prefix)
                .expect("Safe");
            let ads_res = self.answer_ads_from_tree(ads, ts);
            if ads_res.is_some() {
                unreachable!("Tree had a reply, duplicate query!");
            } else {
                ads.reset_to_root();
            }
        }

        self.sul.reset();
        let prefix_out = self.sul.step(prefix);
        let (mut hyp_curr, hyp_out) = fsm.trace(prefix);
        for (idx, (tree_o, hyp_o)) in prefix_out.iter().zip(hyp_out.iter()).enumerate() {
            if tree_o != hyp_o {
                let prefix_in = prefix[..=idx].to_vec();
                let prefix_out = prefix_out[..=idx].to_vec();
                return Some((prefix_in, prefix_out));
            }
        }
        let mut inputs_sent = prefix.to_vec();
        let mut outputs_received = prefix_out.to_vec();
        let len_diff = inputs_sent.len() - outputs_received.len();
        for _ in 0..len_diff {
            outputs_received.push(self.sink_output);
        }
        let mut hyp_out;
        let mut prev_output = None;
        loop {
            let next_input = ads.next_input(prev_output);
            match next_input {
                Ok(input) => {
                    log::debug!("Input sent: {:?}", input);
                    inputs_sent.push(input);
                    let o = self.sul.single_step(input);
                    (hyp_curr, hyp_out) = fsm.step_from(hyp_curr, input);
                    log::debug!("Output Received: {:?}", o);
                    prev_output = Some(o);
                    outputs_received.push(o);
                    if o != hyp_out {
                        self.add_observation(&inputs_sent, &outputs_received);
                        return Some((inputs_sent, outputs_received));
                    }
                }
                Err(ads_err) => match ads_err {
                    AdsStatus::Done => {
                        self.add_observation(&inputs_sent, &outputs_received);
                        return None;
                    }
                    AdsStatus::Unexpected => {
                        // This should never occur during EQs:
                        // either the hypothesis and SUL disagree on an ouput,
                        // OR the ADS is `Done`.
                        println!("Prefix: {prefix:?}");
                        unreachable!("IADS root must always contain the hyp state to identify.");
                    }
                },
            }
        }
    }

    #[allow(clippy::type_complexity)]
    fn answer_ads_from_tree(
        &self,
        ads: &mut impl AdaptiveDistinguishingSequence,
        from_state: T::S,
    ) -> Option<(Vec<InputSymbol>, Vec<OutputSymbol>)> {
        let mut prev_output = None;
        let mut inputs_sent = vec![];
        let mut outputs_received = vec![];
        let mut curr_state = from_state;
        while let Ok(next_input) = ads.next_input(prev_output) {
            inputs_sent.push(next_input);
            let (output, dest) = self.obs_tree.get_out_succ(curr_state, next_input)?;
            prev_output = Some(output);
            outputs_received.push(output);
            curr_state = dest;
        }
        ads.reset_to_root();
        Some((inputs_sent, outputs_received))
    }

    /// Add a trace to the observation tree directly.
    pub fn add_observation(
        &mut self,
        input_seq: &[InputSymbol],
        output_seq: &[OutputSymbol],
    ) -> T::S {
        self.obs_tree
            .insert_observation(None, input_seq, output_seq)
    }
}

impl<'a, T> Oracle<'a, T> {
    #[allow(clippy::type_complexity)]
    pub fn pass_maps(&self) -> (Vec<(String, InputSymbol)>, Vec<(String, OutputSymbol)>) {
        (self.sul.input_map(), self.sul.output_map())
    }

    pub fn set_old_basis_access(&mut self, old_basis_seqs: FxHashMap<Vec<InputSymbol>, State>) {
        self.old_basis_access = old_basis_seqs;
    }

    pub fn set_old_hypothesis(&mut self, old_hypothesis: &Mealy) {
        self.ads_support.update_fsm(old_hypothesis.clone());
    }

    pub fn reset_last_cce(&mut self) {
        self.last_ce_consistency = false;
    }

    pub fn set_last_cce(&mut self) {
        self.last_ce_consistency = true;
    }

    fn filter_old_basis(&self, candidates: &[Cow<[InputSymbol]>]) -> Vec<Vec<InputSymbol>> {
        let old_accesses = &self.old_basis_access;
        candidates
            .par_iter()
            .filter(|seq| old_accesses.contains_key(&seq[..]))
            .map(|x| x.to_vec())
            .collect()
    }
}

#[derive(Default)]
struct CachedADSSuport {
    fsm: Option<Mealy>,
    split_tree: Option<IADSSplitTree>,
    ads: Option<IADSTree>,
    prefixes_visited: FxHashSet<Vec<InputSymbol>>,
}

impl CachedADSSuport {
    fn get_ads_if_unvisited(&mut self, prefix: &[InputSymbol]) -> Option<IADSTree> {
        if self.prefixes_visited.insert(prefix.to_vec()) {
            self.ads.clone()
        } else {
            None
        }
    }

    fn update_fsm(&mut self, fsm: Mealy) {
        let states = fsm.states();
        if states.len() > 1 {
            let mut tree = IADSSplitTree::new(&fsm, &states);
            let ads = IADSTree::construct(&fsm, &mut tree, &states, 0);
            self.fsm = Some(fsm);
            self.split_tree = Some(tree);
            self.ads = Some(ads);
            self.prefixes_visited.clear();
        }
    }
}

mod util {
    use derive_more::Constructor;

    use crate::definitions::mealy::InputSymbol;

    /// A `Pair` containing two access sequences, where the first is always [lexicographically][https://doc.rust-lang.org/std/cmp/trait.Ord.html#lexicographical-comparison] larger than
    /// the second.
    #[derive(Debug, Constructor, PartialEq, Eq, PartialOrd, Ord)]
    pub(super) struct Pair {
        seq1: Vec<InputSymbol>,
        seq2: Vec<InputSymbol>,
    }

    impl<T> From<[T; 2]> for Pair
    where
        T: Into<Vec<InputSymbol>>,
    {
        fn from(value: [T; 2]) -> Self {
            let [x, y] = value;
            let x = x.into();
            let y = y.into();
            if x > y {
                Self::new(x, y)
            } else {
                Self::new(y, x)
            }
        }
    }
}
