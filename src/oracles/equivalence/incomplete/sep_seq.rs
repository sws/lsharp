#[derive(Debug, PartialEq, Eq, Clone, Copy)]
/// Status of the separating sequence, always one of three -
///
/// 1. `Inj`, or
/// 2. `NonInj`.
enum Status {
    /// Injective
    Inj,
    /// Non-Injective
    NonInj,
}

/// Store a separating sequence and whether it's (non-) injective.
#[derive(Debug, Default, PartialEq, Eq, Clone)]
pub(super) struct SepSeq<T> {
    seq: Vec<T>,
    status: Option<Status>,
}

impl<T> SepSeq<T> {
    pub fn seq(&self) -> &Vec<T> {
        &self.seq
    }

    /// `true` if the separating sequence is injective.
    pub fn is_inj(&self) -> bool {
        matches!(self.status, Some(Status::Inj))
    }

    /// `true` if the separating sequence is set.
    pub fn is_set(&self) -> bool {
        self.status.is_some()
    }

    /// Construct an injective separating sequence.
    pub fn inj(seq: Vec<T>) -> Self {
        Self {
            seq,
            status: Some(Status::Inj),
        }
    }

    /// Construct a non-injective separating sequence.
    pub fn non_inj(seq: Vec<T>) -> Self {
        Self {
            seq,
            status: Some(Status::NonInj),
        }
    }
}
