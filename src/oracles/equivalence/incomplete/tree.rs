use itertools::Itertools;
use rayon::prelude::{IntoParallelIterator, ParallelIterator};
use rustc_hash::{FxHashMap, FxHashSet};

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;
use crate::util::data_structs::arena_tree::ArenaTree;

use super::best_node::BestR;
use super::index::Index;
use super::node::Node;
use super::prio_queue::{DependentPrioQueue, PrioQueue};
use super::scoring::{score_sep, score_xfer};
use super::sep_seq::SepSeq;
use super::separating_nodes::SeparatingNodes;
use super::splits::{analyse_input_symb, input_word_is_sep_inj, Type};

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Default)]
pub struct SplittingTree {
    pub tree: ArenaTree<Node, ()>,
    /// separating Nodes LCA
    sep_lca: SeparatingNodes<State, Index>,
    /// Analysed Indices,
    analysed: FxHashSet<Index>,
}

/// Check score for `r` using input `x` to go to node `r_x`.
/// If it is better than the current score in `best_r`, update the `best_r`.
fn update_if_better(
    r: Index,
    input: InputSymbol,
    r_x: Index,
    fsm: &Mealy,
    tree: &SplittingTree,
    best_r: &mut BestR,
) {
    let score = score_xfer(tree.ref_at(r), input, tree.ref_at(r_x), fsm);
    if score < best_r.score() {
        best_r.update(input, r_x, score);
    }
}

impl SplittingTree {
    pub(super) fn separating_sequence(&self, block: &[State]) -> SepSeq<InputSymbol> {
        let lca = self.get_lca(block);
        let ret = lca.map(|lca| self.tree.ref_at(lca).sep_seq.clone());
        ret.unwrap_or_default()
    }

    pub(super) fn get_lca(&self, block: &[State]) -> Option<Index> {
        let block = block.iter().copied().unique().collect_vec();
        let (&pivot, rem) = block.split_first().expect("Block contains no states.");
        let lca_found = rem
            .iter()
            .filter_map(|&x| self.sep_lca.check_pair(x, pivot))
            .max_by_key(|x| self.tree.apply_at(x.to_index(), Node::size));
        lca_found
    }

    #[must_use]
    pub fn new(fsm: &Mealy, root_label: &[State]) -> Self {
        let mut helpers = Helpers::default();
        let mut ret = Self::default();
        ret.construct(fsm, root_label, &mut helpers);
        ret.analysed.extend(helpers.analysed_indices);
        ret
    }

    /// Construct a splitting tree using the ST-IADS algorithm.
    ///
    /// # Panics
    /// If some pair of states does not have a separating sequence.
    pub fn construct(&mut self, fsm: &Mealy, initial_label: &[State], helpers: &mut Helpers) {
        helpers
            .analysed_indices
            .extend(self.analysed.iter().copied());
        let root_node = Node::from_block(initial_label);
        let root_idx = Index::from(self.tree.node(root_node));
        helpers.nodes_in_tree.insert(root_idx);
        let original_block_size = initial_label.len();
        helpers.partition.push(root_idx, original_block_size);

        while let Some(r_idx) = helpers.partition.pop() {
            let r_idx = r_idx;

            // No need to 'split' nodes which have singleton labels.
            if self.ref_at(r_idx).size() == 1 {
                continue;
            }
            if !helpers.analysed_indices.contains(&r_idx) {
                self.analyse(r_idx, fsm, &mut helpers.analysed_indices);
            }

            if self.ref_at(r_idx).sep_seq.is_set() {
                self.separate(r_idx, fsm, helpers);
            } else {
                helpers.dependent.insert(r_idx);
            }
            let curr_block_size = self.ref_at(r_idx).size();
            if !helpers.dependent.is_empty() && helpers.partition.maximal() <= curr_block_size {
                let mut nodes_seen = FxHashSet::default();
                let mut stable = false;
                while !stable {
                    stable = true;
                    for r in helpers.dependent.clone() {
                        if !nodes_seen.insert(r) {
                            continue;
                        }
                        stable &= self.init_trans_on_inj_inputs(fsm, r, helpers);
                    }
                    if !stable {
                        nodes_seen.clear();
                    }
                }
                self.process_dependent(fsm, helpers);
                if !helpers.dependent.is_empty() {
                    let mut nodes_seen = FxHashSet::default();
                    let mut stable = false;
                    while !stable {
                        stable = true;
                        for r in helpers.dependent.clone() {
                            if !nodes_seen.insert(r) {
                                continue;
                            }
                            stable &= self.init_trans_on_inj_inputs(fsm, r, helpers);
                            if !self.ref_at(r).sep_seq.is_inj() {
                                stable &= self.init_trans_on_non_inj_inputs(fsm, r, helpers);
                            }
                        }
                        if !stable {
                            nodes_seen.clear();
                        }
                    }
                    self.process_dependent(fsm, helpers);
                }
            } else if !helpers.dependent.is_empty() {
                unreachable!(
                    "How do we have a block in the partition bigger than the current block?"
                );
            }
            if !self.ref_at(r_idx).is_separated() {
                helpers.partition.push(r_idx, self.ref_at(r_idx).size());
            }
        }

        let block = initial_label.iter().copied().collect_vec();
        let mut sep_nodes = vec![];
        Itertools::cartesian_product(block.iter().copied(), block.iter().copied())
            .filter(|(x, y)| x < y)
            .map(|(s1, s2)| (s1, s2, self.lca_of_two(s1, s2).expect("Safe")))
            .for_each(|(s1, s2, lca)| {
                sep_nodes.push((s1, s2, lca));
            });
        for (s1, s2, lca) in sep_nodes {
            if self.sep_lca.check_pair(s1, s2).is_none() {
                self.sep_lca.insert_pair(s1, s2, lca);
            }
        }
    }

    /// Initialise transitions on injective inputs.
    fn init_trans_on_inj_inputs(&mut self, fsm: &Mealy, r: Index, helpers: &mut Helpers) -> bool {
        let mut stable = true;
        let mut best_r = helpers.best_r.get(&r).cloned().unwrap_or_default();
        let injective_xfer_inputs = self.ref_at(r).inputs_of_type(Type::XferInj);

        // For each valid transferring input,
        for input in injective_xfer_inputs {
            // get lca of the dest states.
            let dest_block = self
                .ref_at(r)
                .successors
                .get(&input)
                .expect("Injective xfer input should have successors.");
            if let Some(r_x) = self.maybe_lca(dest_block, &helpers.nodes_in_tree) {
                if helpers.dependent.contains(&r_x) {
                    helpers.add_transition_from_to_via(r, r_x, input);
                } else {
                    update_if_better(r, input, r_x, fsm, self, &mut best_r);
                }
            } else {
                let new_node = Node::from_block(dest_block);
                let r_x = self.find_node_exact(dest_block).unwrap_or_else(|| {
                    let uidx = self.tree.node(new_node);
                    Index::from(uidx)
                });

                if helpers.analysed_indices.contains(&r_x) && self.ref_at(r_x).sep_seq.is_inj() {
                    update_if_better(r, input, r_x, fsm, self, &mut best_r);
                }
                if !helpers.analysed_indices.contains(&r_x) || !self.ref_at(r_x).sep_seq.is_set() {
                    if !helpers.analysed_indices.contains(&r_x) {
                        self.analyse(r_x, fsm, &mut helpers.analysed_indices);
                    }
                    if self.ref_at(r_x).sep_seq.is_set() {
                        update_if_better(r, input, r_x, fsm, self, &mut best_r);
                    } else {
                        stable = !helpers.dependent.insert(r_x);
                    }
                    helpers.add_transition_from_to_via(r, r_x, input);
                } else {
                    update_if_better(r, input, r_x, fsm, self, &mut best_r);
                }
            }
        }
        helpers.best_r.insert(r, best_r);

        // If the best_r.next node has a valid separating sequence, then add the current node to the
        // dependent priority queue.
        let best_r = helpers.best_r.get(&r).expect("Safe");
        let next_has_inj_seq = best_r
            .next()
            .map(|x| self.ref_at(x).sep_seq.is_inj())
            .unwrap_or_default();

        if next_has_inj_seq {
            helpers.dependent_prio_queue.push(r, best_r.score());
        }
        stable
    }

    fn process_dependent(&mut self, fsm: &Mealy, helpers: &mut Helpers) {
        while let Some(r) = helpers.dependent_prio_queue.pop() {
            helpers.dependent.remove(&r);
            if self.ref_at(r).is_separated() {
                continue;
            }
            let best_r = helpers.best_r.get(&r).expect("Safe");
            let sep_seq = {
                let xfer_input = best_r
                    .input()
                    .expect("Xfer input should be defined for a dependent node.");
                let mut sep_seq = vec![xfer_input];
                let seq = best_r.next().map(|idx| self.ref_at(idx).sep_seq.seq());
                if let Some(seq) = seq {
                    sep_seq.extend_from_slice(seq);
                }
                sep_seq
            };

            let iw_is_sep_inj = input_word_is_sep_inj(fsm, &sep_seq, &self.ref_at(r).label);
            let sep_seq = if iw_is_sep_inj {
                SepSeq::inj(sep_seq)
            } else {
                SepSeq::non_inj(sep_seq)
            };
            self.tree.arena[r.to_index()].val.sep_seq = sep_seq;
            self.separate(r, fsm, helpers);
            let Some(trans_to_r) = helpers.transitions_to.get(&r) else {
                continue;
            };
            for &(p_idx, input) in trans_to_r {
                if self.ref_at(p_idx).sep_seq.is_set() {
                    continue;
                }
                let score_p = score_xfer(self.ref_at(p_idx), input, self.ref_at(r), fsm);
                let best_p_score = helpers.score_of(p_idx);
                if score_p < best_p_score {
                    let best_p = BestR::construct(input, r, score_p);
                    helpers.best_r.insert(p_idx, best_p);
                    helpers.dependent_prio_queue.push(p_idx, score_p);
                }
            }
        }
    }

    fn init_trans_on_non_inj_inputs(
        &mut self,
        fsm: &Mealy,
        r: Index,
        helpers: &mut Helpers,
    ) -> bool {
        let mut stable = true;
        let best_r = helpers.best_r.entry(r).or_default();
        let input_non_inj_separating =
            |i, label| analyse_input_symb(fsm, i, label).non_inj_sep_input();
        let best_non_inj_sep_input = fsm
            .input_alphabet()
            .into_iter()
            .filter(|&x| input_non_inj_separating(x, &self.ref_at(r).label))
            .map(|x| (x, score_sep(self.ref_at(r), x, fsm)))
            .filter(|(_, score)| *score < best_r.score()) // score is better than best score.
            .min_by_key(|(_, score)| *score); // choose the best score.

        // If the result is Some, then the invalid separating input has a lower score than
        // whatever score we had before, so we update best_r with that information.
        if let Some((i, score)) = best_non_inj_sep_input {
            best_r.update(i, None, score);
        }

        // Now, we must check all non-injective transferring inputs.
        let invalid_xfer_inputs = self.ref_at(r).inputs_of_type(Type::XferNonInj);
        // If one of them leads to a lower score than the non-injective separating
        // input that we might have selected earlier, we update the best node accordingly.
        for input in invalid_xfer_inputs {
            let succ = self
                .ref_at(r)
                .successors
                .get(&input)
                .expect("Analysis was incomplete?");
            let maybe_r_x = self.maybe_lca(succ, &helpers.nodes_in_tree);
            let r_x;
            if let Some(next_r) = maybe_r_x {
                r_x = next_r;
                if self.ref_at(r_x).sep_seq.is_set() {
                    update_if_better(r, input, r_x, fsm, self, best_r);
                }
            } else {
                let new_node = Node::from_block(succ);
                r_x = self.find_node_exact(succ).unwrap_or_else(|| {
                    let uidx = self.tree.node(new_node);
                    Index::from(uidx)
                });
                if !helpers.analysed_indices.contains(&r_x) || !self.ref_at(r_x).sep_seq.is_set() {
                    if !helpers.analysed_indices.contains(&r_x) {
                        self.analyse(r_x, fsm, &mut helpers.analysed_indices);
                    }
                    if self.ref_at(r_x).sep_seq.is_set() {
                        update_if_better(r, input, r_x, fsm, self, best_r);
                    }
                    helpers
                        .transitions_to
                        .entry(r_x)
                        .or_default()
                        .insert((r, input));
                }
                if !self.ref_at(r_x).sep_seq.is_set() {
                    stable = !helpers.dependent.insert(r_x);
                }
            }
            if self.ref_at(r_x).sep_seq.is_set() {
                update_if_better(r, input, r_x, fsm, self, best_r);
            }
        }
        let r_priority = helpers.score_of(r);
        if r_priority != usize::MAX {
            helpers.dependent_prio_queue.push(r, r_priority);
        }
        stable
    }

    fn maybe_lca(&self, block: &[State], nodes_in_tree: &FxHashSet<Index>) -> Option<Index> {
        if nodes_in_tree.len() == 1 {
            return Some(Index::from(0));
        }
        Itertools::cartesian_product(block.iter().copied(), block.iter().copied())
            .filter(|(x, y)| x != y)
            .filter_map(|(s1, s2)| self.lca_of_two(s1, s2))
            .max_by_key(|a| self.ref_at(*a).size())
    }

    fn lca_of_two(&self, s1: State, s2: State) -> Option<Index> {
        let mut cand = Index::from(0);
        let find_child_with_state = |r: &Node, st: State| {
            r.children
                .values()
                .find(|&&s| self.ref_at(Index::from(s)).has_state(st))
                .copied()
        };
        let find_children_at_node = |r: &Node| {
            let s1_child = find_child_with_state(r, s1);
            let s2_child = find_child_with_state(r, s2);
            s1_child.zip(s2_child)
        };
        let node_splits =
            |node: &Node| find_children_at_node(node).map_or(false, |(c1, c2)| c1 != c2);

        let next_node = |node: &Node| {
            find_children_at_node(node)
                .filter(|(c1, c2)| c1 == c2)
                .map(|(x, _)| Index::from(x))
        };

        loop {
            if node_splits(self.ref_at(cand)) {
                return Some(cand);
            }
            cand = next_node(self.ref_at(cand))?;
        }
    }

    /// Find the node index containing the exact `block` of states, if it exists.
    fn find_node_exact(&self, block: &[State]) -> Option<Index> {
        let idx_has_state = |s: State, idx: Index| self.ref_at(idx).has_state(s);
        let idx_has_block = |idx: &Index| block.iter().all(|s| idx_has_state(*s, *idx));
        let block_and_label_size_match = |idx: &Index| block.len() == self.ref_at(*idx).size();
        let idx_iterator = (0..self.tree.size()).into_par_iter().map(Index::from);
        idx_iterator
            .filter(block_and_label_size_match)
            .find_first(idx_has_block)
    }

    fn analyse(&mut self, r_idx: Index, fsm: &Mealy, analysed: &mut FxHashSet<Index>) {
        self.tree.arena[r_idx.to_index()].val.analyse(fsm);
        analysed.insert(r_idx);
    }

    fn separate(&mut self, r_idx: Index, fsm: &Mealy, helpers: &mut Helpers) {
        // We only want to separate the nodes in the tree.
        if !helpers.nodes_in_tree.contains(&r_idx) {
            return;
        }
        let r_node = self.ref_at(r_idx);
        let seq = r_node.sep_seq.seq();
        let mut output_src_map = FxHashMap::<Box<[OutputSymbol]>, Vec<State>>::default();
        for s in &r_node.label {
            let outs = fsm.trace_from(*s, seq).1;
            output_src_map.entry(outs).or_default().push(*s);
        }
        // Create and add successors to the splitting tree using the output_src_map.
        let child_indxs = output_src_map
            .into_iter()
            .map(|(out_seq, src_label)| {
                let os = *out_seq.last().expect("Safe");
                let child_node = Node::from_block(&src_label);
                (os, child_node)
            })
            .map(|(out_symbol, child_node)| {
                let child_node_idx = self.tree.node(child_node);
                (out_symbol, child_node_idx)
            })
            .collect_vec();
        let r_node = &mut self.tree.arena[r_idx.to_index()].val;
        r_node.children.extend(child_indxs);
        let r_node = self.ref_at(r_idx);
        r_node
            .children
            .values()
            .map(|idx| Index::from(*idx))
            .filter(|c| self.ref_at(*c).size() > 1)
            .for_each(|c| {
                let prio = self.ref_at(c).size();
                helpers.nodes_in_tree.insert(c);
                helpers.partition.push(c, prio);
            });
    }

    fn ref_at(&self, idx: Index) -> &Node {
        self.tree.ref_at(idx)
    }
}

#[derive(Debug, Default)]
pub struct Helpers {
    /// Set of nodes in the splitting tree.
    /// We do not want the auxilary nodes to be present in the tree.
    nodes_in_tree: FxHashSet<Index>,
    /// Queue of indices indicating leaf nodes of the splitting tree.
    partition: PrioQueue<Index, usize>,
    /// Nodes that do not have a sep. input. We need a xfering or invalid input.
    dependent: FxHashSet<Index>,
    /// Helper queue for processing nodes from ``dependent``.
    dependent_prio_queue: DependentPrioQueue<Index>,
    /// Links from a node to others.
    /// Key is the dest idx and value is a set of (src,input) tuples.
    transitions_to: FxHashMap<Index, FxHashSet<(Index, InputSymbol)>>,

    // Utilities: Yes, really.
    analysed_indices: FxHashSet<Index>,
    /// Map a node to the current "best" possible node to split the former.
    best_r: FxHashMap<Index, BestR>,
}

impl Helpers {
    /// Add a transition from index `src` to index `dest` using input `via`.
    #[inline]
    fn add_transition_from_to_via(&mut self, src: Index, dest: Index, via: InputSymbol) {
        self.transitions_to
            .entry(dest)
            .or_default()
            .insert((src, via));
    }

    /// Get score of the node `r`.
    #[inline]
    fn score_of(&self, r: Index) -> usize {
        self.best_r.get(&r).map(BestR::score).expect("Safe")
    }
}

#[cfg(test)]
mod tests {

    use super::SplittingTree;
    use crate::{definitions::FiniteStateMachine, util::parsers::machine::read_mealy_from_file};
    use itertools::Itertools;
    use rstest::rstest;

    #[rstest]
    #[case::asml_loes("tests/src_models/hypothesis_79.dot")]
    #[case("tests/src_models/esm_hypothesis_534.dot")]
    #[case("tests/src_models/hypothesis_6.dot")]
    #[case("tests/src_models/hypothesis_21.dot")]
    #[case("tests/src_models/hypothesis_23.dot")]
    #[case("tests/src_models/TCP_FreeBSD_Server.dot")]
    #[case("tests/src_models/bestr_TCP_Win_serv_hyp10.dot")]
    #[case::win8_server_loop("tests/src_models/tcp_win8_serv_hyp10.dot")]
    #[case::linux_server_hyp24("tests/src_models/stree_tcp_linux_srv_hyp24.dot")]
    #[case("tests/src_models/iads_example.dot")]
    fn all_states_split(#[case] file_name: &str) {
        let (fsm, _, _) = read_mealy_from_file(file_name);
        let fsm_states = fsm.states();
        let s_tree = SplittingTree::new(&fsm, &fsm.states());
        let state_pairs = fsm_states.into_iter().tuple_combinations();
        for (x, y) in state_pairs {
            s_tree.get_lca(&[x, y]).unwrap();
            let seq = s_tree.separating_sequence(&[x, y]);
            let sep_seq = seq.seq();
            assert!(!sep_seq.is_empty(), "No separating sequence for pair!");
        }
    }
}
