use super::tree::{Helpers, SplittingTree};
use crate::{
    ads::{AdaptiveDistinguishingSequence, AdsStatus},
    definitions::{
        mealy::{InputSymbol, Mealy, OutputSymbol, State},
        FiniteStateMachine,
    },
    util::data_structs::arena_tree::ArenaTree,
};
use itertools::Itertools;
use rand::{
    prelude::{SliceRandom, StdRng},
    Rng, SeedableRng,
};
use rayon::{iter::ParallelIterator, prelude::IntoParallelRefIterator};
use rustc_hash::{FxHashMap, FxHashSet};
use serde::Serialize;
use std::collections::{HashMap, VecDeque};

#[derive(Debug, Default, Clone, PartialEq, Eq, Serialize)]
pub struct AdsNode {
    pub initial: Vec<State>,
    current: Vec<State>,
    input: Option<InputSymbol>,
    chidren: FxHashMap<OutputSymbol, usize>,
}

impl AdsNode {
    fn construct(initial: Vec<State>, current: Vec<State>) -> Self {
        Self {
            initial,
            current,
            ..Default::default()
        }
    }

    fn from_block(block: impl Iterator<Item = State>) -> Self {
        let initial: Vec<_> = block.collect();
        let current = initial.clone();
        Self {
            initial,
            current,
            ..Default::default()
        }
    }
}

#[derive(Debug, Default, Serialize, Clone)]
pub struct AdsTree {
    pub tree: ArenaTree<AdsNode, ()>,
    /// Each root is an incomplete IADS.
    /// We store the IADSs in a single ArenaTree as opposed to multiple ones.
    roots: Vec<usize>,
    initial_idx: usize,
    pub curr_idx: usize,
    seed: u64,
}

impl AdsTree {
    pub fn construct(
        fsm: &Mealy,
        splitting_tree: &mut SplittingTree,
        initial_label: &[State],
        seed: u64,
    ) -> Self {
        let mut ret = Self::default();
        ret._construct(splitting_tree, fsm, initial_label);
        ret.initial_idx = 0;
        ret.curr_idx = 0;
        ret.seed = seed;
        ret
    }

    fn _construct(
        &mut self,
        og_splitting_tree: &mut SplittingTree,
        fsm: &Mealy,
        initial_label: &[State],
    ) {
        // let init_root = splitting_tree.get_lca(initial_label);
        let all_states = initial_label.to_vec(); // splitting_tree.arena[init_root.to_index()].val.label.clone();
        let mut undistinguished = VecDeque::from([all_states]);
        while let Some(s_prime) = undistinguished.pop_front() {
            let curr_root_idx = self.tree.node(AdsNode::from_block(s_prime.into_iter()));
            self.roots.push(curr_root_idx);
            let mut unprocessed = VecDeque::from([curr_root_idx]);
            while let Some(mut curr_node_idx) = unprocessed.pop_front() {
                let block: Vec<_> = self.tree.arena[curr_node_idx]
                    .val
                    .current
                    .iter()
                    .copied()
                    .unique()
                    .collect();
                let sep_seq = {
                    let seq = og_splitting_tree.separating_sequence(&block);
                    if seq.is_set() {
                        seq.seq().clone()
                    } else {
                        let mut h = Helpers::default();
                        og_splitting_tree.construct(fsm, &block, &mut h);
                        // let new_stree = TreeCons::construct(fsm, &block, false);
                        let seq = og_splitting_tree.separating_sequence(&block);
                        let seq = seq.seq();
                        assert!(!seq.is_empty());
                        seq.clone()
                    }
                    // if let Some(seq) = og_splitting_tree.separating_sequence(&block) {
                    //     seq
                    // } else {
                    //     let mut h = Helpers::default();
                    //     og_splitting_tree.construct(fsm, &block, &mut h);
                    //     // let new_stree = TreeCons::construct(fsm, &block, false);
                    //     og_splitting_tree.separating_sequence(&block).expect("Safe")
                    // }
                };
                // let st_node_idx = { og_splitting_tree.get_lca(&block).to_index() };
                // let (&sep, xfer_seq) = og_splitting_tree.arena[st_node_idx]
                //     .val
                //     .sep_seq
                //     .seq()
                //     .split_last()
                //     .expect("Separating sequence cannot be empty!");
                let (&sep, xfer_seq) = sep_seq.split_last().expect("Sep seq cannot be empty!");
                for &input in xfer_seq {
                    let r = self.mut_ref_at(curr_node_idx);
                    r.input = Some(input);
                    let output = r
                        .current
                        .iter()
                        .map(|&s| fsm.step_from(s, input).1)
                        .unique()
                        .exactly_one()
                        .expect("More than one output for xfering input");
                    let new_initial = r.initial.clone();
                    let new_current = r
                        .current
                        .iter()
                        .map(|&s| fsm.step_from(s, input).0)
                        .collect_vec();
                    let new_node = AdsNode::construct(new_initial, new_current);
                    let new_node_idx = self.tree.node(new_node);
                    let r = self.mut_ref_at(curr_node_idx);
                    r.chidren.insert(output, new_node_idx);
                    curr_node_idx = new_node_idx;
                }
                self.mut_ref_at(curr_node_idx).input = Some(sep);
                let r = self.mut_ref_at(curr_node_idx);
                let out_current_map: FxHashMap<_, Vec<_>> = r
                    .current
                    .iter()
                    .zip(r.initial.iter())
                    .map(|(&cur, &ini)| (fsm.step_from(cur, sep), ini))
                    .fold(HashMap::default(), |mut acc, ((new_curr, out), ini)| {
                        acc.entry(out).or_default().push((new_curr, ini));
                        acc
                    });
                let out_node_map: FxHashMap<_, _> = out_current_map
                    .into_iter()
                    .map(|(out, c_i_v)| {
                        let (cv, iv): (Vec<_>, Vec<_>) = c_i_v.into_iter().unzip();
                        (out, AdsNode::construct(iv, cv))
                    })
                    .map(|(out, node)| (out, self.tree.node(node)))
                    .collect();
                let r = self.mut_ref_at(curr_node_idx);
                r.chidren.extend(out_node_map.into_iter());
                let r = &self.tree.arena[curr_node_idx].val;
                for &c_idx in r.chidren.values() {
                    let c_node = &self.tree.arena[c_idx].val;
                    if c_node.current.iter().unique().count() > 1 {
                        unprocessed.push_back(c_idx);
                    } else if c_node.initial.iter().unique().count() > 1 {
                        undistinguished.push_back(c_node.initial.clone());
                    }
                }
            }
        }
    }

    fn mut_ref_at(&mut self, x: usize) -> &mut AdsNode {
        &mut self.tree.arena[x].val
    }

    pub(crate) fn randomise_root(&mut self, must_have: State, should_have: &[State]) {
        let initial_label_at = |idx: usize| &self.tree.ref_at(idx).initial;
        let should_have: FxHashSet<_> = should_have.iter().copied().collect();

        let node_label_intersects_with_should_have = |idx| {
            initial_label_at(idx)
                .iter()
                .any(|x| should_have.contains(x))
        };

        let roots: Vec<_> = self
            .roots
            .par_iter()
            .filter(|&&idx| initial_label_at(idx).contains(&must_have))
            .filter(|&&idx| node_label_intersects_with_should_have(idx))
            .copied()
            .collect();
        let mut rng = StdRng::seed_from_u64(self.seed);
        let new_root = *roots.choose(&mut rng).expect("Safe");
        self.initial_idx = new_root;
        self.curr_idx = new_root;
        self.seed = rng.gen();
    }
}

impl AdaptiveDistinguishingSequence for AdsTree {
    fn next_input(&mut self, prev_output: Option<OutputSymbol>) -> Result<InputSymbol, AdsStatus> {
        // Return AdsErr if the last_output was not found in the child map.
        self.curr_idx = prev_output.map_or_else(
            || Ok(self.curr_idx), // last_output was none.
            |output| {
                let cur_node = &self.tree.arena[self.curr_idx].val;
                cur_node
                    .chidren
                    .get(&output)
                    .copied()
                    .ok_or(AdsStatus::Unexpected)
            },
        )?;
        let cur_node = &self.tree.arena[self.curr_idx].val;
        cur_node.input.ok_or(AdsStatus::Done)
    }

    fn get_print_tree(&self) -> Box<[u8]> {
        unimplemented!("No printing available for the IADS");
    }

    fn identification_power(&self) -> f32 {
        unimplemented!("Not supported.");
    }

    fn reset_to_root(&mut self) {
        self.curr_idx = self.initial_idx;
    }
}

////////////////////////// Validation for the IADS /////////////////////////////

#[cfg(test)]
mod tests {
    use rayon::prelude::IntoParallelIterator;

    use super::*;
    use crate::util::parsers::machine::read_mealy_from_file;
    use std::path::Path;

    #[test]
    fn make_iads_example() {
        let file_name = Path::new("tests/src_models/iads_example.dot");
        let (fsm, _, _) = read_mealy_from_file(file_name.to_str().expect("Safe"));
        let all_states = fsm.states().into_iter().collect_vec();
        let mut tree = SplittingTree::new(&fsm, &all_states);
        let ads = AdsTree::construct(&fsm, &mut tree, &all_states, 0);
        assert_eq!(ads.roots.len(), 3);
    }

    impl AdsNode {
        fn is_leaf(&self) -> bool {
            self.chidren.is_empty()
        }
    }

    impl AdsTree {
        fn verify(&self) {
            let non_singleton_leaves = (0..self.tree.size())
                .into_par_iter()
                .filter(|&idx| self.tree.ref_at(idx).is_leaf())
                .filter(|&idx| self.tree.ref_at(idx).initial.len() != 1)
                .count();
            assert_eq!(self.roots.len() - 1, non_singleton_leaves);
        }

        fn validate(&self, fsm: &Mealy) {
            type LocalIdentType = Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)>;
            let mut work_list: VecDeque<_> = self.roots.iter().copied().collect();
            let mut separating_seqs: FxHashMap<State, LocalIdentType> = FxHashMap::default();
            while let Some(root) = work_list.pop_front() {
                let ads_sep_seq = self.validate_ads(root);
                for (state, ip_op_vec) in ads_sep_seq {
                    separating_seqs
                        .entry(state)
                        .or_default()
                        .extend(ip_op_vec.into_iter());
                }
            }

            for (state, ip_op_vecs) in separating_seqs {
                for (input_seq, expected_out_seq) in ip_op_vecs {
                    let hyp_out_seq = fsm.trace_from(state, &input_seq).1.to_vec();
                    assert_eq!(expected_out_seq, hyp_out_seq);
                }
            }
        }

        #[allow(clippy::type_complexity)]
        fn validate_ads(
            &self,
            root: usize,
        ) -> FxHashMap<State, Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)>> {
            let mut work_list = VecDeque::from([(vec![], vec![], root)]);
            let mut ret: FxHashMap<State, Vec<(Vec<InputSymbol>, Vec<OutputSymbol>)>> =
                FxHashMap::default();
            while let Some((unique_ip, unique_op, curr_node)) = work_list.pop_front() {
                if let Some(next_ip) = self.tree.ref_at(curr_node).input {
                    for (&output, &next_node) in &self.tree.ref_at(curr_node).chidren {
                        let mut new_ip_seq = unique_ip.clone();
                        new_ip_seq.push(next_ip);
                        let mut new_output_seq = unique_op.clone();
                        new_output_seq.push(output);
                        work_list.push_back((new_ip_seq, new_output_seq, next_node));
                    }
                } else {
                    // No next node, this is a root node, we must send back the paths.
                    for &state in &self.tree.ref_at(curr_node).initial {
                        ret.entry(state)
                            .or_default()
                            .push((unique_ip.clone(), unique_op.clone()));
                    }
                }
            }
            ret
        }
    }

    #[test]
    fn ads_construction() {
        let name = "tests/src_models/hypothesis_22.dot";
        let file_name = Path::new(name);
        let (fsm, _, _) = read_mealy_from_file(file_name.to_str().expect("Safe"));
        let all_states = fsm.states().into_iter().collect_vec();
        let mut tree = SplittingTree::new(&fsm, &all_states);
        let ads = AdsTree::construct(&fsm, &mut tree, &all_states, 0);
        ads.validate(&fsm);
        ads.verify();

        let non_singleton_leaves = (0..ads.tree.size())
            .into_par_iter()
            .filter(|&idx| ads.tree.ref_at(idx).is_leaf())
            .filter(|&idx| ads.tree.ref_at(idx).initial.len() != 1)
            .count();
        assert_eq!(ads.roots.len() - 1, non_singleton_leaves);
    }
}
