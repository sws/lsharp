use std::{fs::File, path::Path};

use proptest::{collection, proptest, strategy::Strategy};

use crate::{
    definitions::{
        mealy::{InputSymbol, Mealy, MealySerde, State},
        FiniteStateMachine,
    },
    oracles::equivalence::incomplete::splits::input_word_is_sep_inj,
    util::parsers::machine::read_mealy_from_file,
};

fn try_gen_inputs() -> impl Strategy<Value = Vec<InputSymbol>> {
    let num_inputs = 13;
    #[allow(clippy::cast_possible_truncation)]
    let input_symbol_strat = (0..num_inputs)
        .prop_map(|x| x as u16)
        .prop_map(InputSymbol::from);

    collection::vec(input_symbol_strat, 2..20)
}

fn load_basic_fsm(name: &str) -> Mealy {
    let file_name = Path::new(name);
    read_mealy_from_file(file_name.to_str().expect("Safe")).0
}

fn try_gen_states() -> impl Strategy<Value = Vec<State>> {
    let num_states = 66;
    #[allow(clippy::cast_possible_truncation)]
    let state_stat = (0..num_states).prop_map(|x| x as u32).prop_map(State::from);
    collection::vec(state_stat, 2..num_states)
}

fn gen_block_and_words() -> impl Strategy<Value = (Vec<InputSymbol>, Vec<State>)> {
    let input_strat = try_gen_inputs();
    let output_strat = try_gen_states();
    (input_strat, output_strat)
}

fn refac_works(fsm: &Mealy, iw: &[InputSymbol], block: &[State]) {
    let input_info = analyse_input_word(fsm, iw, block);
    let iw_is_sep_inj = input_word_is_sep_inj(fsm, iw, block);
    if iw_is_sep_inj != input_info.inj_sep_input() {
        let buffer =
            File::create("./src/oracles/equivalence/incomplete/splits/reg_1.json").expect("Safe");
        serde_json::to_writer_pretty(buffer, &MealySerde::from(fsm.clone())).expect("Safe");
        panic!("refac iword failed");
    }
}

#[test]
fn reg_1() {
    let file = File::open("./src/oracles/equivalence/incomplete/splits/reg_1.json").expect("Safe");
    let reader = std::io::BufReader::new(file);

    // Read the JSON contents of the file as an instance of `User`.
    let fsm_serde: MealySerde = serde_json::from_reader(reader).expect("Safe");
    let fsm: Mealy = fsm_serde.into();
    let iw = [InputSymbol::new(5), InputSymbol::new(5)];
    let block = [State::from(0), State::from(10)];
    let input_info = analyse_input_word(&fsm, &iw, &block);
    let iw_is_sep_inj = input_word_is_sep_inj(&fsm, &iw, &block);
    let old_is_sep_ink = input_info.inj_sep_input();
    assert_eq!(old_is_sep_ink, iw_is_sep_inj);
}

// #[test]
// fn reg_2() {
//     let file = File::open("./src/oracles/equivalence/incomplete/splits/reg_1.json").expect("Safe");
//     let reader = std::io::BufReader::new(file);

//     // Read the JSON contents of the file as an instance of `User`.
//     let fsm_serde: MealySerde = serde_json::from_reader(reader).expect("Safe");
//     let fsm: Mealy = fsm_serde.into();

//     let (iw, block) = (
//         ([InputSymbol::new(0), InputSymbol::new(0)]),
//         ([State::new(3), State::new(4), State::new(0)]),
//     );

//     let input_info = analyse_input_word(&fsm, &iw, &block);
//     let iw_is_sep_inj = input_word_is_sep_inj(&fsm, &iw, &block);
//     let old_is_sep_ink = input_info.inj_sep_input();
//     assert_eq!(old_is_sep_ink, iw_is_sep_inj);
// }

proptest! {

    // #[test]
    // fn test_iword_refact(trial in gen_block_and_words()) {
    //     // let fsm = load_basic_fsm("./src/oracles/equivalence/incomplete/splits/hypothesis_18.dot");
    //     let fsm = load_basic_fsm("tests/src_models/BitVise.dot");
    //     let (iw, block) = trial;
    //     refac_works(&fsm, &iw, &block);
    // }

}
