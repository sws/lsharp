use std::collections::HashMap;

use itertools::Itertools;

use rustc_hash::{FxHashMap, FxHashSet};

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub(super) enum Type {
    SepInj,
    SepNonInj,
    XferInj,
    XferNonInj,
    Useless,
}

#[derive(Debug)]
pub(super) struct PartitionInfo<T> {
    split_map: FxHashMap<T, FxHashMap<State, FxHashSet<State>>>,
}

pub(super) fn analyse_input_symb(
    fsm: &Mealy,
    i: InputSymbol,
    block: &[State],
) -> PartitionInfo<OutputSymbol> {
    let mut output_dest_map: FxHashMap<_, FxHashMap<_, FxHashSet<_>>> = HashMap::default();
    let make_entry_in_map = |(src, (dest, out))| {
        let out_map = output_dest_map.entry(out).or_default();
        out_map.entry(dest).or_default().insert(src);
    };
    block
        .iter()
        .map(|&s| (s, fsm.step_from(s, i)))
        .for_each(make_entry_in_map);
    PartitionInfo {
        split_map: output_dest_map,
    }
}

impl<T> PartitionInfo<T> {
    /// A partition is injective iff all source states which have the same output do not have the same destination state.
    fn is_injective(&self) -> bool {
        self.split_map
            .values() // For all (dest, [srcs]) values with the same output.
            .flat_map(HashMap::values) // Get all src blocks
            .all(|src_block| src_block.len() == 1)
    }

    /// A partition is separating iff the set of source states produce at least two distinct outputs.
    fn is_separating(&self) -> bool {
        // Simply check if the keys (which are the outputs) are more than one.
        self.split_map.len() > 1
    }

    fn is_useless(&self) -> bool {
        let o_d_ss_map = &self.split_map;
        // For each pair of (source,destination) state, we have the case that the source and destination states are the
        // same.
        let mut is_useless = true;
        for dests_srcs in o_d_ss_map.values() {
            let mut all_dest_src_pairs_same = true;
            for (dests, srcs) in dests_srcs {
                let dest_set: FxHashSet<_> = vec![*dests].into_iter().collect();
                if &dest_set != srcs {
                    all_dest_src_pairs_same = false;
                }
            }
            if !all_dest_src_pairs_same {
                is_useless = false;
            }
        }
        if o_d_ss_map.len() > 1 {
            is_useless = false;
        }
        is_useless
    }

    pub(super) fn i_type(&self) -> Type {
        let useless = self.is_useless();
        if useless {
            return Type::Useless;
        }
        let separating = self.is_separating();
        let injective = self.is_injective();
        if separating {
            if injective {
                return Type::SepInj;
            }
            return Type::SepNonInj;
        }
        // A partition *can be* transferring if it is not separating.
        if !separating {
            if injective {
                return Type::XferInj;
            } else if !self.merges_all_states() {
                return Type::XferNonInj;
            }
        }
        // Otherwise, the partition is useless: it only has one output and one destination state.
        Type::Useless
    }

    /// A partition merges all states if there is only one destination state for all source states.
    fn merges_all_states(&self) -> bool {
        let o_d_ss_map = &self.split_map;
        let d_iter = o_d_ss_map.values().flat_map(HashMap::keys).unique();
        d_iter.count() == 1
    }

    pub(super) fn non_inj_sep_input(&self) -> bool {
        self.i_type() == Type::SepNonInj
    }

    pub(super) fn all_dests(&self) -> impl Iterator<Item = State> + '_ {
        self.split_map
            .values()
            .flat_map(HashMap::keys)
            .unique()
            .copied()
    }
}

/// Is the input word separating and injective?
pub(super) fn input_word_is_sep_inj(fsm: &Mealy, input: &[InputSymbol], r_block: &[State]) -> bool {
    let mut old_outs_dest = FxHashSet::default();
    let mut inj;
    let mut num_outs = FxHashSet::default();
    for s in r_block {
        let (d, o) = fsm.trace_from(*s, input);
        inj = old_outs_dest.insert((o.clone(), d));
        if !inj {
            return false;
        }
        num_outs.insert(o);
    }
    num_outs.len() > 1
}
