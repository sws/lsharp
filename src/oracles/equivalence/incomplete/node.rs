use itertools::Itertools;
use rustc_hash::{FxHashMap, FxHashSet};

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;

use super::sep_seq::SepSeq;
use super::splits::{analyse_input_symb, Type};

#[derive(Debug, Default)]
pub struct Node {
    /// Set of states to split.
    pub label: Vec<State>,
    /// Successor nodes map.
    pub children: FxHashMap<OutputSymbol, usize>,
    /// Successors of the label states for input sequences.
    pub successors: FxHashMap<InputSymbol, Vec<State>>,
    /// Separating Sequence.
    pub(super) sep_seq: SepSeq<InputSymbol>,

    // Utilities
    /// Map input to split-type
    split_map: FxHashMap<InputSymbol, Type>,
}

impl PartialEq for Node {
    fn eq(&self, other: &Node) -> bool {
        if self.label != other.label {
            return false;
        }
        if self.sep_seq != other.sep_seq {
            return false;
        }
        let outs_eq = {
            let self_outs = self.children.keys().copied().collect::<FxHashSet<_>>();
            let other_outs = other.children.keys().copied().collect::<FxHashSet<_>>();
            self_outs == other_outs
        };
        if !outs_eq {
            return false;
        }
        true
    }
}

impl Node {
    /// Get inputs of specific type.
    pub(super) fn inputs_of_type(&self, s_type: Type) -> Vec<InputSymbol> {
        self.split_map
            .iter()
            .filter_map(|(i, t)| (*t == s_type).then_some(*i))
            .collect()
    }

    /// Node contains state `s` in the label.
    pub fn has_state(&self, s: State) -> bool {
        self.label.contains(&s)
    }

    /// Given a block of states, construct a new node,
    /// with the rest of the fields being default values.
    pub fn from_block(block: &[State]) -> Self {
        let label = block.iter().copied().unique().collect();
        Self {
            label,
            ..Default::default()
        }
    }

    /// A node is separated if it has children assigned to it.
    pub fn is_separated(&self) -> bool {
        !self.children.is_empty()
    }

    /// Number of states in the label.
    pub fn size(&self) -> usize {
        self.label.len()
    }

    /// Analyse a node of the tree: check partitions induced by each input and create successors.
    pub fn analyse(&mut self, fsm: &Mealy) {
        let r_block = &self.label;
        // Cannot unzip directly because the CLion linter is broken.
        // The optimiser should clear it away anyhow.
        (self.successors, self.split_map) = fsm
            .input_alphabet()
            .into_iter()
            .map(|i| (i, analyse_input_symb(fsm, i, r_block)))
            // We want to skip all inputs which induce useless splits.
            .filter(|(_, split)| split.i_type() != Type::Useless)
            .map(|(i, info)| {
                let succ = (i, info.all_dests().collect_vec());
                let split = (i, info.i_type());
                (succ, split)
            })
            .unzip();
        let sep_inj_input = self
            .split_map
            .iter()
            .find_map(|(i, t)| (Type::SepInj == *t).then_some(*i));
        if let Some(i) = sep_inj_input {
            self.sep_seq = SepSeq::inj(vec![i]);
        }
    }
}

#[cfg(test)]
mod tests {
    use itertools::Itertools;
    use rstest::rstest;

    use crate::definitions::FiniteStateMachine;
    use crate::util::parsers::machine::read_mealy_from_file;

    use super::super::splits::Type;
    use super::Node;

    #[rstest]
    #[case::soucha_example_input_analysis("tests/src_models/iads_example.dot")]
    /// Check if the block of nodes is analysed correctly.
    fn analyse_input(#[case] file_name: &str) {
        let (fsm, input_map, _) = read_mealy_from_file(file_name);
        let states = fsm.states().into_iter().collect_vec();
        let mut r_node = Node::from_block(&states);
        r_node.analyse(&fsm);
        let split_map = r_node.split_map;

        let exp_splits = [Type::SepNonInj, Type::SepNonInj, Type::XferNonInj];
        let exp_map: rustc_hash::FxHashMap<_, _> = ["a", "b", "c"]
            .into_iter()
            .filter_map(|x| input_map.get(x))
            .copied()
            .zip(exp_splits.into_iter())
            .collect();

        assert_eq!(exp_map, split_map, "Actual split map: {:?}", split_map);
    }
}
