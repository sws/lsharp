use crate::definitions::mealy::{InputSymbol, OutputSymbol};

use super::SystemUnderLearning;
/// A wrapper over an SUL for Sink-State Optimisation.
///
/// Any struct implementing [`SystemUnderLearning`] can be supplied to
/// this struct.
pub struct SinkWrapper<S> {
    sul: S,
    sink_output: OutputSymbol,
    in_error: bool,
}

impl<S: SystemUnderLearning> SinkWrapper<S> {
    pub fn new(sul: S, sink_output: OutputSymbol) -> Self {
        Self {
            sul,
            sink_output,
            in_error: false,
        }
    }
}

impl<S: SystemUnderLearning> SystemUnderLearning for SinkWrapper<S> {
    fn step(&mut self, input_seq: &[InputSymbol]) -> Box<[OutputSymbol]> {
        let mut ret = Vec::with_capacity(input_seq.len());
        for &i in input_seq {
            if self.in_error {
                ret.push(self.sink_output);
                continue;
            }
            let o = self.sul.single_step(i);
            ret.push(o);

            if o == self.sink_output {
                self.in_error = true;
            }
        }
        ret.into_boxed_slice()
    }

    fn reset(&mut self) {
        self.in_error = false;
        self.sul.reset();
    }

    fn get_counts(&mut self) -> (usize, usize) {
        self.sul.get_counts()
    }

    fn input_map(&self) -> Vec<(String, InputSymbol)> {
        self.sul.input_map()
    }

    fn output_map(&self) -> Vec<(String, OutputSymbol)> {
        self.sul.output_map()
    }

    fn single_step(&mut self, input: InputSymbol) -> OutputSymbol {
        if self.in_error {
            return self.sink_output;
        }
        let output = self.sul.single_step(input);
        if output == self.sink_output {
            self.in_error = true;
        }
        output
    }
}
