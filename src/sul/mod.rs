//! Module containing implementation(s) for a System Under Learning (SUL).
//!

use crate::definitions::mealy::{InputSymbol, OutputSymbol};

/// System Under Learning, as a trait.
/// An SUL needs to implement this trait. You can see [`Simulator`](self::Simulator)
/// or [`Stdio`](self::Stdio) for an example of the same.
pub trait SystemUnderLearning {
    /// Return the output of the provided input.
    #[must_use]
    fn single_step(&mut self, input: InputSymbol) -> OutputSymbol;

    /// Return the output sequence of the provided input sequence.
    #[must_use]
    fn step(&mut self, input_seq: &[InputSymbol]) -> Box<[OutputSymbol]>;

    /// Reset the SUL to its initial state.
    fn reset(&mut self);

    /// A [`reset`](SystemUnderLearning::reset) and a [`step`](SystemUnderLearning::step).
    #[must_use]
    fn trace(&mut self, input_seq: &[InputSymbol]) -> Box<[OutputSymbol]> {
        self.reset();
        self.step(input_seq)
    }

    /// Get the number of inputs and resets sent to the SUL thus far, **and reset the counters**.
    #[must_use = "Internal SUL counters are reset when this function is called."]
    fn get_counts(&mut self) -> (usize, usize);

    /// Get a ref to the input bimap of the SUL.
    #[must_use]
    fn input_map(&self) -> Vec<(String, InputSymbol)>;

    /// Get a ref to the input bimap of the SUL.
    #[must_use]
    fn output_map(&self) -> Vec<(String, OutputSymbol)>;
}

mod simulator;
mod sink_wrapper;
mod stdio;

pub use crate::sul::simulator::Simulator;
pub use crate::sul::sink_wrapper::SinkWrapper;
pub use crate::sul::stdio::Stdio;
