use std::collections::HashMap;
use std::hash::BuildHasher;
use std::io::{BufRead, BufReader, ErrorKind, Write};
use std::path::Path;
use std::process::{Child, ChildStderr, ChildStdin, ChildStdout, Command};

use bimap::BiHashMap;
use itertools::Itertools;

use crate::definitions::mealy::{InputSymbol, OutputSymbol};

use super::SystemUnderLearning;

/// An external SUL struct interacting with the actual SUL over stdio.
///
/// ### NOTE
/// We initialise the child process during the construction of the struct
/// itself.
pub struct Stdio<S> {
    cnt_inputs: usize,
    cnt_resets: usize,
    input_map: BiHashMap<String, InputSymbol, S, S>,
    output_map: BiHashMap<String, OutputSymbol, S, S>,
    #[allow(dead_code)]
    child: Child,
    sul_stdin: ChildStdin,
    sul_stdout: BufReader<ChildStdout>,
    reset_code: String,
    #[allow(dead_code)]
    sul_stderr: ChildStderr,
}

impl<S: BuildHasher + Default> Stdio<S> {
    /// Initialise the struct.
    ///
    /// ### Errors
    /// If the SUL could not be initialised: the path is incorrect; or the input, output, or error streams
    /// could not be obtained.
    pub fn new(
        loc: &dyn AsRef<Path>,
        input_map: &HashMap<String, InputSymbol, S>,
        reset_code: String,
    ) -> std::io::Result<Self> {
        let i_m = input_map.iter().map(|(s, i)| (s.clone(), *i)).collect();
        let canon_loc = std::fs::canonicalize(loc)?;
        let mut child = Command::new(canon_loc)
            .stdin(std::process::Stdio::piped())
            .stdout(std::process::Stdio::piped())
            .stderr(std::process::Stdio::piped())
            .spawn()?;
        let stderr_err =
            std::io::Error::new(ErrorKind::Other, "Could not get handle to SUL's stderr.");
        let stdout_err =
            std::io::Error::new(ErrorKind::Other, "Could not get handle to SUL's stdout.");
        let stdin_err =
            std::io::Error::new(ErrorKind::Other, "Could not get handle to SUL's stdin.");
        let sul_stdin = child.stdin.take().ok_or(stdin_err)?;
        let sul_stdout = BufReader::new(child.stdout.take().ok_or(stdout_err)?);
        let sul_stderr = child.stderr.take().ok_or(stderr_err)?;

        Ok(Self {
            cnt_inputs: 0,
            cnt_resets: 0,
            input_map: i_m,
            output_map: BiHashMap::default(),
            child,
            sul_stdin,
            sul_stdout,
            sul_stderr,
            reset_code,
        })
    }
}

impl<S: BuildHasher> SystemUnderLearning for Stdio<S> {
    fn step(&mut self, input_seq: &[InputSymbol]) -> Box<[OutputSymbol]> {
        let mut o_buf = String::new();
        let mut o_s = Vec::with_capacity(input_seq.len());
        let i_s = input_seq
            .iter()
            .map(|i| self.input_map.get_by_right(i).expect("Safe"))
            .collect_vec();
        for i in i_s {
            o_buf.clear();
            self.sul_stdin
                .write_all(i.as_bytes())
                .expect("Writing input to SUL failed.");
            self.sul_stdin
                .write_all("\n".as_bytes())
                .expect("Writing input newline to SUL failed.");
            self.sul_stdout
                .read_line(&mut o_buf)
                .expect("Could not read from SUL.");
            o_buf = o_buf.trim().to_string();
            let mut osymb = self.output_map.get_by_left(&o_buf).copied();
            if osymb.is_none() {
                #[allow(clippy::cast_possible_truncation)]
                self.output_map.insert(
                    o_buf.clone(),
                    OutputSymbol::new(self.output_map.len() as u16),
                );
                osymb = self.output_map.get_by_left(&o_buf).copied();
            }
            o_s.push(osymb.expect("Safe"));
        }
        self.cnt_inputs += input_seq.len();
        log::trace!(" {:?} / {:?}", input_seq, o_s);
        o_s.into_boxed_slice()
    }

    fn reset(&mut self) {
        self.cnt_resets += 1;
        self.sul_stdin
            .write_all(self.reset_code.as_bytes())
            .expect("Writing reset to SUL failed.");
        self.sul_stdin
            .write_all("\n".as_bytes())
            .expect("Writing reset newline to SUL failed.");
    }

    fn get_counts(&mut self) -> (usize, usize) {
        let ret = (self.cnt_inputs, self.cnt_resets);
        self.cnt_inputs = 0;
        self.cnt_resets = 0;
        ret
    }

    fn input_map(&self) -> Vec<(String, InputSymbol)> {
        self.input_map
            .iter()
            .map(|(s, i)| (s.clone(), *i))
            .collect()
    }

    fn output_map(&self) -> Vec<(String, OutputSymbol)> {
        self.output_map
            .iter()
            .map(|(s, o)| (s.clone(), *o))
            .collect()
    }

    fn single_step(&mut self, input: InputSymbol) -> OutputSymbol {
        let mut o_buf = String::new();
        let input_str = self.input_map.get_by_right(&input).expect("Safe");
        o_buf.clear();
        self.sul_stdin
            .write_all(input_str.as_bytes())
            .expect("Writing input to SUL failed.");
        self.sul_stdin
            .write_all("\n".as_bytes())
            .expect("Writing input newline to SUL failed.");
        self.sul_stdout
            .read_line(&mut o_buf)
            .expect("Could not read from SUL.");
        o_buf = o_buf.trim().to_string();
        let mut osymb = self.output_map.get_by_left(&o_buf).copied();
        if osymb.is_none() {
            #[allow(clippy::cast_possible_truncation)]
            self.output_map.insert(
                o_buf.clone(),
                OutputSymbol::new(self.output_map.len() as u16),
            );
            osymb = self.output_map.get_by_left(&o_buf).copied();
        }
        let output = osymb.expect("Safe");
        self.cnt_inputs += 1;
        log::trace!(" {:?} / {:?}", input, output);
        output
    }
}
