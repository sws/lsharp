use std::{collections::HashMap, hash::BuildHasher};

use bimap::BiHashMap;

use crate::definitions::mealy::{InputSymbol, Mealy, OutputSymbol, State};
use crate::definitions::FiniteStateMachine;

use super::SystemUnderLearning;

/// Simulate an SUL.
///
/// This struct uses an FSM to "simulate" the behaviour of a real SUL.
#[derive(Debug)]
pub struct Simulator<'a, S> {
    fsm: &'a Mealy,
    initial_state: State,
    curr_state: State,
    input_map: BiHashMap<String, InputSymbol, S, S>,
    output_map: BiHashMap<String, OutputSymbol, S, S>,
    cnt_inputs: usize,
    cnt_resets: usize,
}

impl<'a, S: BuildHasher + Default> Simulator<'a, S> {
    /// Constructor.
    #[must_use]
    pub fn new(
        fsm: &'a Mealy,
        input_map: &HashMap<String, InputSymbol, S>,
        output_map: &HashMap<String, OutputSymbol, S>,
    ) -> Self {
        let i_m = input_map.iter().map(|(s, i)| (s.clone(), *i)).collect();
        let o_m = output_map.iter().map(|(s, o)| (s.clone(), *o)).collect();
        Self {
            initial_state: fsm.initial_state(),
            curr_state: fsm.initial_state(),
            fsm,
            input_map: i_m,
            output_map: o_m,
            cnt_inputs: 0,
            cnt_resets: 0,
        }
    }
}

impl<'a, S: BuildHasher> SystemUnderLearning for Simulator<'a, S> {
    fn step(&mut self, input_seq: &[InputSymbol]) -> Box<[OutputSymbol]> {
        let (dest, out_seq) = self.fsm.trace_from(self.curr_state, input_seq);
        self.cnt_inputs += input_seq.len();
        self.curr_state = dest;
        log::trace!(" {:?} / {:?}", input_seq, out_seq);
        out_seq
    }

    fn reset(&mut self) {
        self.cnt_resets += 1;
        self.curr_state = self.initial_state;
        log::trace!("\tRESET");
    }

    fn get_counts(&mut self) -> (usize, usize) {
        let ret = (self.cnt_inputs, self.cnt_resets);
        self.cnt_inputs = 0;
        self.cnt_resets = 0;
        ret
    }

    fn input_map(&self) -> Vec<(String, InputSymbol)> {
        self.input_map
            .iter()
            .map(|(s, i)| (s.clone(), *i))
            .collect()
    }

    fn output_map(&self) -> Vec<(String, OutputSymbol)> {
        self.output_map
            .iter()
            .map(|(s, o)| (s.clone(), *o))
            .collect()
    }

    fn single_step(&mut self, input: InputSymbol) -> OutputSymbol {
        let (dest, output) = self.fsm.step_from(self.curr_state, input);
        self.cnt_inputs += 1;
        self.curr_state = dest;
        log::trace!(" {:?} / {:?}", input, output);
        output
    }
}
