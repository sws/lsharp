FROM ubuntu:21.04

# Update default packages
RUN apt-get update

# Get Ubuntu packages
RUN apt-get install -y \
    build-essential \
    curl \
    maven \
    unzip

# Update new packages
RUN apt-get update

# Get Rust
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y

ENV PATH="/home/bharat/.cargo/bin:${PATH}"

RUN useradd -m bharat && \
    echo bharat:bharat | chpasswd && \
    cp /etc/sudoers /etc/sudoers.bak && \
    echo 'bharat  ALL=(root) NOPASSWD: ALL' >> /etc/sudoers


USER bharat
WORKDIR /home/bharat

COPY lsharp.zip /home/bharat/lsharp.zip
CMD unzip lsharp.zip -d .
